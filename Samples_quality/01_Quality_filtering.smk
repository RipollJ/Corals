import os
import sys

configfile: "Config.yml"

SAMPLE, = glob_wildcards("out/workdir/{sample}_1.fastq")
ENVIRONMENT = "../envs/QCF.yml"
ENVIRONMENT2 = "../envs/Assembly.yml"

###############################################################################

rule all:
    """

    #####################
    # Quality filtering #
    #####################

    Author: Julie Ripoll
    Contact: julie.ripoll87@gmail.com
    created: 2018
    License: Licence CC-BY-NC 4.0

    Modified: 2018-10-02

    Aim: Trim of raw reads according to quality and adapters.

    This snakefile need the use of snakemake version > 3.5 for solving conda environment with the --use-conda option. 
    It needs also conda > 4.5

    Execute as this:
    >snakemake -s Samples_quality/01_Quality_filtering.smk --use-conda --conda-frontend mamba -p -r -j 1

    """
    input:
        final = "out/Sortmerna/Acropora/"

###############################################################################

rule Cutadapt:
    """
    Aim: Filtering with cutadapt.
    """
    message: 
        """--- CUT 10-1srt-LETTERS {wildcards.sample} ---"""
    conda:
        ENVIRONMENT
    log:
        "out/Cut/log/{sample}.log"
    input:
        Read1 = "out/workdir/{sample}_1.fastq",
        Read2 = "out/workdir/{sample}_2.fastq"
    output:
        CutR1 = "out/Cut/{sample}_1.fastq",
        CutR2 = "out/Cut/{sample}_2.fastq"
    shell:
        "cutadapt "
        "-u 10 -U 10 "
        "-o {output.CutR1} "
        "-p {output.CutR2} "
        "{input.Read1} {input.Read2} "
        "> {log}"


rule Sickle:
    """
    Aim: Filtering on quality with Sickle.
    """
    message: 
        """--- TRIMMED-ON-QUALITY-Q35 {wildcards.sample} ---"""
    conda:
        ENVIRONMENT 
    log:
        "out/Sickle/log/{sample}.log"
    input:
        CutR1 = "out/Cut/{sample}_1.fastq",
        CutR2 = "out/Cut/{sample}_2.fastq"
    output:
        TrimmedR1 = "out/Sickle/{sample}_1.fastq",
        TrimmedR2 = "out/Sickle/{sample}_2.fastq",
        Single = "out/Sickle/Single/{sample}.fastq"
    shell:
        "sickle "
        "pe " # mode paired-end, se for single-end
        "-f {input.CutR1} "
        "-r {input.CutR2} "
        "-t sanger "
        "-o {output.TrimmedR1} "
        "-p {output.TrimmedR2} "
        "-s {output.Single} "
        "-q 35 -l 50 " # q for quality Phred Score and l for length
        "> {log}"


rule Merge_files:
    """
    Aim: Merging of files read 1 and read 2 with SortMeRNA.
    """
    message: 
        """--- MERGE-TRIMMED-FILES-{wildcards.sample} ---"""
    conda:
        ENVIRONMENT
    log:
        "out/Sortmerna/log/{sample}.log"
    input:
        TrimmedR1 = "out/Sickle/{sample}_1.fastq",
        TrimmedR2 = "out/Sickle/{sample}_2.fastq"
    output:
        Merged = "out/Sortmerna/Merged_{sample}.fastq"
    shell:
        "chmod +x Samples_quality/scripts/merge-paired-reads.sh "
        "&& "
        "Samples_quality/scripts/merge-paired-reads.sh {input.TrimmedR1} {input.TrimmedR2} {output.Merged} "
        "> {log}"


rule ziprawfastq:
    """
    Aim: zip raw and intermediate FASTQ files to save space
    """
    message:
        """--- Zip raw FASTQ {wildcards.sample} ---"""
    conda:
        ENVIRONMENT2
    input:
        check = "out/Sortmerna/Merged_{sample}.fastq",
        raw1 = "out/workdir/{sample}_1.fastq",
        raw2 = "out/workdir/{sample}_2.fastq",
        cut1 = "out/Cut/{sample}_1.fastq",
        cut2 = "out/Cut/{sample}_2.fastq",
        TrimmedR1 = "out/Sickle/{sample}_1.fastq",
        TrimmedR2 = "out/Sickle/{sample}_2.fastq",
        Single = "out/Sickle/Single/{sample}.fastq"
    output:
        raw = "out/workdir/{sample}_1.fastq.gz",
        cut = "out/Cut/{sample}_1.fastq.gz",
        TrimmedR1 = "out/Sickle/{sample}_1.fastq.gz"
    script:
        "bzip2 -9 {input.raw1} "
        "&& "
        "bzip2 -9 {input.raw2} "
        "&& "
        "bzip2 -9 {input.cut1} "
        "&& "
        "bzip2 -9 {input.cut2} "
        "&& "
        "bzip2 -9 {input.TrimmedR1} "
        "&& "
        "bzip2 -9 {input.TrimmedR2} "
        "&& "
        "bzip2 -9 {input.Single} "


rule Sort_samples_by_species:
    """
    Aim: Separate data by species before assembling
    """
    message:
        """--- Separate data by species before assembling ---"""
    input:
        Reads = expand("out/Sickle/{sample}_1.fastq.gz", sample = SAMPLE)
    params:
        "out/Sortmerna/"
    output:
        out1 = directory("out/Sortmerna/Acropora/"),
        out2 = directory("out/Sortmerna/Pocillopora/")
    shell:
        "mkdir {output.out1} {output.out2} "
        "&& "
        "mv {params}/Merged_NA* {output.out1} "
        "&& "
        "mv {params}/Merged_NP* {output.out2}"
