import os
import sys

configfile: "Config.yml"

ENVIRONMENT = "../envs/QCF.yml"

###############################################################################

rule all:
    """

    ########################
    # Preparation of files #
    ########################

    Author: Julie Ripoll
    Contact: julie.ripoll87@gmail.com
    Created: 2018
    License: CC-BY-NC 4.0

    Modified: 2018-10-02

    Aim: Copy raw files in a working dir and check raw quality of samples.

    This snakefile need the use of snakemake version > 3.5 for solving conda environment with the --use-conda option. 
    It needs also conda > 4.5

    Execute as this:
    >snakemake -s Samples_quality/00_Preparing_files.smk --use-conda --conda-frontend mamba -p -r -j 1

    """
    input:
        final = expand('out/Quality/MQC1/')


###############################################################################

rule Copy:
    """
    Aim: Copy input files to a working dir, keep safe original samples.
    """
    message: 
        """--- Copy samples (require free space) ---"""
    input:
        Fastq = directory('inp/')
    output:
        Copy  = directory('out/workdir/')
    shell:
        "cp {input.Fastq}/*/*.fastq.gz {output.Copy} "
        "&& "
        "gunzip {output.Copy}/* --best"


rule FastQC:
    """
    Aim: Checks sequence quality before trim with FastQC.
    """
    message: 
        """--- FastQC ---"""
    conda:
        ENVIRONMENT
    input:
        'out/workdir/'
    params:
       config['threads']['Normal']
    output:
        directory('out/Quality/FQ1/')
    shell: 
        "mkdir out/Quality "
        "&& "
        "fastqc "
        "{input}*.fastq "
        "-t {params} "
        "-o {output}"


rule MultiQC:
    """
    Aim: Checks sequence quality before trim with MultiQC.
    """
    message: 
        """--- MultiQC on FastQC output ---"""
    conda:
        ENVIRONMENT
    input:
        'out/Quality/FQ1/'
    output:
        directory('out/Quality/MQC1/')
    shell: 
        "multiqc "
        "-d {input} "
        "-o {output} "
        "-n reportQ1.html "
        "-p"
