import os
import sys

configfile: "Config.yml"

ENVIRONMENT = "../envs/QCF.yml"
SPECIES = config["species"]
THREADS = config['threads']['Normal']

###############################################################################

rule all:
    """

    #################################################
    # Quality checking after filtering and trimming #
    #################################################

    Author: Julie Ripoll
    Contact: julie.ripoll87@gmail.com
    Created: 2018
    License: License CC-BY-NC 4.0

    Modified: 2018-10-02

    Aim: Check quality after trimming

    This snakefile need the use of snakemake version > 3.5 for solving conda environment with the --use-conda option. 
    It needs also conda > 4.5

    Execute as this:
    >snakemake -s Samples_quality/03_Quality_Check.smk --use-conda --conda-frontend mamba -p -r -j 1

    """
    input:
        expand('out/Quality/{species}/MQC2/MQC2.log', species = SPECIES)


###############################################################################

rule FastQC:
    """
    Aim:  Checks sequence quality after trim with FastQC.
    """
    message:
        """--- FastQC after filtering ---"""
    conda:
        ENVIRONMENT
    input:
        'out/Sortmerna/{species}/Other/'
    params:
        threads = THREADS,
        fold = 'out/Quality/{species}/FQ2/'
    output:
        "out/Quality/{species}/FQ2/FQC2.log"
    shell:
        "fastqc "
        "-t {params.threads} "
        "-o {params.fold} "
        "{input}/*.fastq "
        "2>{output}"


rule MultiQC:
    """
    Aim: Checks sequence quality after FastQC with MultiQC.
    """
    message:
        """--- MultiQC on FastQC after filtering ---"""
    conda:
        ENVIRONMENT
    input:
        'out/Quality/{species}/FQ2/'
    params:
        'out/Quality/{species}/MQC2/'
    output:
        'out/Quality/{species}/MQC2/MQC2.log'
    shell:
        "multiqc "
        "-d {input} "
        "-o {params} "
        "-n reportQ2.html "
        "-p "
        "2>{output}"
