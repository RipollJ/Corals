import os
import sys

configfile: "Config.yml"

ENVIRONMENT = "../envs/QCF.yml"
SAMPLE, = glob_wildcards("out/workdir/{sample}_1.fastq")
SPECIES = config["species"]
THREADS = config['threads']['Normal']

###############################################################################

rule all:
    """

    ###############
    # Filter rRNA #
    ###############

    Author: Julie Ripoll
    Contact: julie.ripoll87@gmail.com
    Created: 2018
    License: License CC-BY-NC 4.0

    Modified: 2018-10-02

    Aim: Separation of ribosomal RNA (rRNA) from other RNAs according to Silva rRNA database.
        Sequences of rRNA from this database was previously downloadead ;
        and divided in 6 fasta files according to kingdoms: archaea, bacteria and eukaryota,
        and ribosomal subunit size, the large subunit (LSU) and the small subunit (SSU).

    This snakefile need the use of snakemake version > 3.5 for solving conda environment with the --use-conda option. 
    It needs also conda > 4.5

    Execute as this:
    >snakemake -s Samples_quality/02_Filtering_rRNA.smk --use-conda --conda-frontend mamba -p -r -j 1

    """
    input:
        Read1 = expand("out/Assembling/{species}/{sample}_1.fastq",
                        sample = SAMPLE,
                        species = SPECIES),
        Read2 = expand("out/Assembling/{species}/{sample}_2.fastq",
                        sample = SAMPLE,
                        species = SPECIES)

###############################################################################

rule Index_db:
    """
    Aim: indexing rRNA Silva databases for Sortmerna
    """
    message:
        """--- INDEX Silva rRNA DATABASES ---"""
    conda:
        ENVIRONMENT
    input:
        LSU_archaea_notalign = "Databases/Silva/LSU_archaea_notalign.fasta",
        LSU_bacteria_notalign = "Databases/Silva/LSU_bacteria_notalign.fasta",
        LSU_eukaryota_notalign = "Databases/Silva/LSU_eukaryota_notalign.fasta",
        SSU_archaea_notalign = "Databases/Silva/SSU_archaea_notalign.fasta",
        SSU_bacteria_notalign = "Databases/Silva/SSU_bacteria_notalign.fasta",
        SSU_eukaryota_notalign = "Databases/Silva/SSU_eukaryota_notalign.fasta"
    output:
        "out/Sortmerna/log/index.done"
    shell:
        "mkdir out/Sortmerna/{wildcards.species}/rRNA_tot out/Sortmerna/{wildcards.species}/Other "
        "&& "
        "indexdb_rna "
        "--ref ./{input.LSU_archaea_notalign},./Databases/Silva/LSU_archaea_notalign.fasta.index:./{input.LSU_bacteria_notalign},./Databases/Silva/LSU_bacteria_notalign.fasta.index:./{input.LSU_eukaryota_notalign},./Databases/Silva/LSU_eukaryota_notalign.fasta.index:./{input.SSU_archaea_notalign},./Databases/Silva/SSU_archaea_notalign.fasta.index:./{input.SSU_bacteria_notalign},./Databases/Silva/SSU_bacteria_notalign.fasta.index:./{input.SSU_eukaryota_notalign},./Databases/Silva/SSU_eukaryota_notalign.fasta.index "
        "-m 4e+04 -v "
        "2>{output}"


rule Filter_rRNA:
    """
    Aim: Filtering rRNA with SortMeRNA.
    Requirements:
        - donwload and index databases for version 2 of SortMeRNA
        - merge of samples, see 01_Quality_filtering.smk
    """
    message:
        """--- SORTING-RNA-{wildcards.sample} ---"""
    conda:
        ENVIRONMENT
    log:
        "out/Sortmerna/{species}/log/filter_{sample}.done"
    input:
        check = "out/Sortmerna/log/index.done",
        Merged = "out/Sortmerna/{species}/Merged_{sample}.fastq"
    output:
        MergedrRNA = "out/Sortmerna/{species}/rRNA_tot/Merged_{sample}_rRNA.fastq",
        MergedOther = "out/Sortmerna/{species}/Other/Merged_{sample}_oRNA.fastq"
    params:
       threads = THREADS,
       merged_ribo_basename ='Merged_{sample}_rRNA',
       merged_tot_basename ='Merged_{sample}_oRNA'
    shell:
        "sortmerna "
        "--reads {input.Merged} "
        "--ref ./Databases/Silva/LSU_archaea_notalign.fasta,./Databases/Silva/LSU_archaea_notalign.fasta.index:./Databases/Silva/LSU_bacteria_notalign.fasta,./Databases/Silva/LSU_bacteria_notalign.fasta.index:./Databases/Silva/LSU_eukaryota_notalign.fasta,./Databases/Silva/LSU_eukaryota_notalign.fasta.index:./Databases/Silva/SSU_archaea_notalign.fasta,./Databases/Silva/SSU_archaea_notalign.fasta.index:./Databases/Silva/SSU_bacteria_notalign.fasta,./Databases/Silva/SSU_bacteria_notalign.fasta.index:./Databases/Silva/SSU_eukaryota_notalign.fasta,./Databases/Silva/SSU_eukaryota_notalign.fasta.index "
        "--fastx "
        "--paired_in "
        "--aligned out/Sortmerna/{wildcards.species}/rRNA_tot/{params.merged_ribo_basename} "
        "--other out/Sortmerna/{wildcards.species}/Other/{params.merged_tot_basename} "
        "--log "
        "-v "
        "-a {params.threads} "
        "> {log}"


rule Demerge_files:
    """
    Aim: Demerge reads files with Sortmerna for assembling
    """
    message:
        """--- DEMERGE-FILES-TOTAL-RNA-{wildcards.sample} ---"""
    conda:
        ENVIRONMENT
    log:
        "out/Sortmerna/{species}/log/demerge_{sample}.log"
    input:
        MergedOther = "out/Sortmerna/{species}/Other/Merged_{sample}_oRNA.fastq"
    params:
        "out/Sortmerna/{species}/Merged_{sample}.fastq"
    output:
        Read1 = "out/Assembling/{species}/{sample}_1.fastq",
        Read2 = "out/Assembling/{species}/{sample}_2.fastq"
    script:
        "chmod +x Samples_quality/scripts/unmerge-paired-reads.sh "
        "&& "
        "Samples_quality/scripts/unmerge-paired-reads.sh {input.MergedOther} {output.Read1} {output.Read2} "
        "2>{log}"
