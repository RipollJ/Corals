#!/bin/env python

#################
## Summary DGE ##
#################

# Author: RipollJ
# Created: 2023-12-05
# License: License CC-BY-NC 4.0
# Last update: 2023-12-05

#################################

import pandas as pd
from contextlib import redirect_stdout
from matplotlib import pyplot as plt
from matplotlib import cm
from IPython.display import SVG, display

# All snakemake variables
snakemake = snakemake  # suppress undeclared variable error

LOG_FILE = snakemake.log["log"]
INPUT_FILES = snakemake.input["summary"]
OUTPUT_HIST = snakemake.output['hist']
OUTPUT_HIST2 = snakemake.output['hist2']

#################################


with open(LOG_FILE, 'w') as f:
    with redirect_stdout(f):

        # import files and keep basename
        data = pd.read_csv(INPUT_FILES, sep="\t")
        print(data)

        # Histogram of DGE summary
        # keep only interesting comparisons
        filt_summary = data.loc[~data["Comparison"].isin(
            ["C.D0-CV.D110", "CC.D3-CC.D110", "CV.D3-CV.D110", "VC.D3-VC.D110",
             "VV.D3-VV.D110", "C.D0-VC.D3", "C.D0-VV.D3", "V.D0-CC.D3",
             "V.D0-CV.D3", "V.D0-VC.D110"])]

        ordered_x = ["C.D0-V.D0", "CC.D3-VV.D3", "CC.D110-VV.D110",
                     "C.D0-CC.D3", "V.D0-VV.D3",
                     "C.D0-CC.D110", "V.D0-VV.D110",
                     "C.D0-CV.D3", "V.D0-VC.D3",
                     "CC.D3-CV.D3", "CC.D3-VC.D3", "VV.D3-VC.D3", "VV.D3-CV.D3",
                     "CC.D110-CV.D110", "CC.D110-VC.D110", "VV.D110-VC.D110",
                     "VV.D110-CV.D110", "CV.D3-VC.D3", "CV.D110-VC.D110"]
        cat_type = pd.CategoricalDtype(categories=ordered_x, ordered=True)
        print(cat_type)

        filt_summary["Comparison"] = filt_summary["Comparison"].astype(
            cat_type)
        # plot a Bar Chart using pandas
        ax = filt_summary.sort_values("Comparison").plot.bar(x='Comparison',
                                                             stacked=False,
                                                             mark_right=False,
                                                             width=0.8,
                                                             tick_label=True,
                                                             figsize=(14, 8)
                                                             )

        for container in ax.containers:
            ax.bar_label(container, fontsize=10)

        # save plot
        # bbox_inches for no crop
        plt.savefig(OUTPUT_HIST, bbox_inches='tight')
        # bbox_inches for no crop
        plt.savefig(OUTPUT_HIST2, bbox_inches='tight')

        # Show plot
        # plt.show()
