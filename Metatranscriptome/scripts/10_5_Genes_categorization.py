#!/usr/bin/env python
# coding: utf-8

# Filter join table
# Author: Julie Ripoll
# Date: 2022/08/07

#################################

# import dependencies
import pandas as pd
import numpy as np
from contextlib import redirect_stdout

pd.__version__
np.__version__

# All snakemake variables
snakemake = snakemake  # suppress undeclared variable error

LOG_FILE = snakemake.log["log"]
INPUT_GENES = snakemake.input["GeneList"]
INPUT_BIOTYPE = snakemake.input["Biotype"]
OUTPUT = snakemake.output['genes']

#################################

# open log file
with open(LOG_FILE, 'w') as f:
    with redirect_stdout(f):
        #################################
        # import data

        data = pd.read_csv(
            INPUT_GENES, sep="\t", header=0)
        print(data.head())

        # import biotype
        biotype = pd.read_csv(
            INPUT_BIOTYPE, sep="\t", header=0)
        print(biotype.head())
        print(len(data["Contigs"].unique()))

        # filter on evalue for Conserved domain database (CDD) ID
        data_eval = data.groupby("Contigs", group_keys=False).apply(
            lambda g: g.nsmallest(1, "evalue_cdd"))
        print(data_eval)

        # filter duplicate
        data_nan = data[data["evalue_cdd"].isnull()].drop_duplicates()
        print(len(data_nan["Contigs"].unique()))

        # add contigs without CDD id
        data_to_merge = data_nan[data_nan["Contigs"].duplicated() == False]
        final = pd.concat([data_eval, data_to_merge])

        # add biotype infos
        final_bio = final.merge(biotype, on="Contigs")
        print(final_bio)

        #################################
        # Categorization

        def gene_name_contains(*args):
            return final_bio['Gene_Name'].str.contains("|".join(args))

        # add category
        final_bio["Category"] = "Others metabolisms"
        final_bio.loc[
            gene_name_contains("uncharacterized", "hypothetical_protein") &
            gene_name_contains("Acropora", "Pocillopora",
                               "Orbicella", "Stylophora"),
            "Category"] = "Unknown coral"

        final_bio.loc[gene_name_contains("Symbiodinium", "chloroplast"),
                      'Category'] = "Symbiodiniaceae"

        final_bio.loc[gene_name_contains("uncharacterized", "clone") &
                      gene_name_contains("Symbiodinium"),
                      "Category"] = "Unknown Symbiodiniaceae"
        # Other possibility of filtering:
        # biotype_str_contains = data["Biotype"].str.contains 
        # data.loc[
        # biotype_str_contains("Symbiodinium") & 
        # biotype_str_contains("unknown mRNA") | biotype_str_contains("library clone")), "Category"
        # ] =  "Unknown Symbiodiniaceae"

        final_bio.loc[gene_name_contains("mitochondrial"),
                      "Category"] = "Mitochondrial"

        final_bio.loc[gene_name_contains("calmodulin", "skeletal", "collagen_alpha", "cysteine", "histidine",
                                         "N66_matrix", "perlucin", "protein_4.1", "carbonic_anhydrase"),
                      "Category"] = "Calcification"

        final_bio.loc[gene_name_contains("GTPase-activating_protein", "solute_carrier_family", "G-protein-signaling", "ATP-binding", "vesicle-trafficking", "follistatin",
                                         "BTN1", "receptor_Ret", "protein_VAT-1", "ZINC_INDUCED_FACILITATOR", "riboflavin-binding", "forkhead_box", "moesin_family", "alpha-taxilin",
                                         "transporter_Rh", "D-ETS-3", "EGF-like", "glycosyltransferase", "golgin", "mitoferrin", "myb-like", "NAD_kinase", "Pax-3-B", "pyroglutamylated", "receptor", "SEC14", "TELO2", "titin", "transport_protein", "twist-related", "calcium_channel", "anion_channel"),
                      "Category"] = "Transport & signalization"

        final_bio.loc[gene_name_contains("fatty_acyl-CoA_reductase", "rhamnosyl_O", "acidic_phospholipase", "glucosidase", "lipoxygenase", "electron_transfer", "fucosyltransferase", "NADP",
                                         "long_chain_fatty", "exostosin", "fructose", "GDP", "protein_4.1", "lipase", "phosphatidylinositol", "prosaposin", "pyruvate", "succinate", "UDP"),
                      "Category"] = "Energetic metabolism"

        final_bio.loc[gene_name_contains("hephaestin-like", "metalloproteinase", "heme", "disintegrin_and_metalloproteinase_domain",
                                         "acid_phosphatase", "sodium-_and_chloride-dependent", "Iron-sulfur", "5-aminolevulinate"),
                      "Category"] = "Metal and ion binding"

        final_bio.loc[gene_name_contains("protein_sax-3", "synaptotagmin", "gamma-aminobutyric", "neurogenic", "neuronal", "netrin_receptor", "noelin", "neural"),
                      'Category'] = "Neural metabolism"

        final_bio.loc[gene_name_contains("bromodomain_adjacent_to_zinc_finger_domain", "exonuclease", "splicing",
                                         "transcription", "initiation_factor", "RNA-binding"),
                      'Category'] = "Splicing and transcription"

        final_bio.loc[gene_name_contains("calumenin", "choloylglycine_hydrolase", "F-box", "phospholipase_D", "RING_finger", "LRR-repeat",
                                         "CDC42_small", "ubiquitin", "polyubiquitin", "TNF_receptor", "Choloylglycine_hydrolase"),
                      'Category'] = "Protein metabolism"

        final_bio.loc[gene_name_contains("caspase-3", "periodic_tryptophan_protein", "coadhesin-like", "kappa_1_light_chain_immunglobulin",
                                         "migration_and_invasion-inhibitory", "fluorescent_protein", "cytochrome_c_oxidase", "ETS", "Fli-1",
                                         "ryncolin", "c-Fos", "peroxiredoxin", "myeloperoxidase", "C-type_lectin", "valiolone", "phospholipid_scramblase",
                                         "chorion_peroxidase", "acid_oxidase", "TNF_receptor", "cochlin", "lectin", "D-inositol", "flavin_reductase", "GFP-like",
                                         "amebocyte", "interferon_alpha", "lymphocyte_antigen", "NLRC5", "PIH1D3", "protein_tweety", "tyrosine-protein_phosphatase",
                                         "red_fluorescent", "G-protein_signaling", "saccharopine_dehydrogenase", "senecionine_N", "somatomedin", "stromal_interaction_molecule",
                                         "SWI/SNF", "tetratricopeptide", "thioredoxin_reductase"),
                      'Category'] = "Stress response/defense"

        print("Annotation complete with categorization according to function found in different databases")
        print(final_bio)

        #################################
        # export final annotation file

        final_bio.to_csv(
            OUTPUT, sep="\t", index=None)

#################################
