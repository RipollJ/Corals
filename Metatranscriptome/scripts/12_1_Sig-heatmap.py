#!/usr/bin/env python
# coding: utf-8

# Heatmaps
# - auteur: Julie Ripoll & stardisblue
# - date: 2022-07-21

#################################
# import des modules

import os
import os.path
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Patch
import seaborn as sns
from contextlib import redirect_stdout

# show dir
print(os.getcwd())
print(pd.__version__)
print(np.__version__)
print(sns.__version__)

#################################
# All snakemake variables

snakemake = snakemake  # suppress undeclared variable error

FILT_TAB = snakemake.input["filt_tab"]
OUT_PATH = snakemake.params["path"]
THRESHOLD = snakemake.params["threshold"]
LOG_FILE = snakemake.output["logfile"]


#################################

def replace_all(text, dic):
    for i, j in dic.items():
        text = text.replace(i, j)
    return text


def create_file_for_heatmap(data, threshold):
    # Rename time into day T -> D
    df_toplot = data.rename(columns=lambda x: replace_all(
        x, {"T0": "D0", "T3": "D3", "T110": "D110"}))
    # Rename Biotype
    df_toplot = df_toplot.rename(columns={"Biotype_x": "Biotype"})
    # log for the heatmap
    df_log2 = df_toplot.loc[:, df_toplot.columns.str.contains(
        "M_|Biotype|Category") == True]
    df_log = df_log2.loc[df_log2.Biotype.str.contains(
        "mRNA|ncRNA| cds|partial sequence|Symbiodinium") == True, ]
    # mean for other heatmap
    df_mean2 = df_toplot.loc[:, df_toplot.columns.str.contains(
        "_mean|Biotype|Category") == True]
    df_mean = df_mean2.loc[df_mean2.Biotype.str.contains(
        "mRNA|ncRNA| cds|partial sequence|Symbiodinium") == True, ]
    # pval for the heatmap
    df_pval = df_toplot.loc[:, df_toplot.columns.str.contains(
        "prob_") == True]
    # convert into presence - absence
    df_pval = df_pval.applymap(lambda x: 1 if x >= threshold else 0)
    return df_log, df_pval, df_mean, df_toplot


def biotype_reduction(df):
    """Reduction of biotype
    Args:
        df (_dataframe_): dataframe containing a column named Biotype
                          requires biotype_contains() function
    Returns:
        _dataframe_: dataframe with new names in Biotype column
    """
    new_df = df.copy()

    s_biotype = new_df["Biotype"].str
    # reduce biotype name
    new_df.loc[s_biotype.contains("partial sequence"), "Biotype"] = "other_RNA"
    new_df.loc[s_biotype.contains(
        "|".join(("Symbiodinium", "chloroplast"))), "Biotype"] = "mRNA"
    new_df.loc[s_biotype.contains(
        "|".join(("mRNA", "partial mRNA", "cds", "complete"))), "Biotype"] = "mRNA"
    new_df.loc[s_biotype.contains(
        "|".join(("ncRNA", "misc_RNA"))), "Biotype"] = "ncRNA"
    # misc_RNA is defined as any ncRNA that we can't categorise as anything else.
    # new_df.loc[(new_df['Biotype'] == "other_RNA") &
    #            (new_df["Category"] == "Symbiodiniaceae"), "Category"] = "Unknown Symbiodiniaceae"
    new_df['Category'] = np.where(((new_df['Biotype'] == "other_RNA") & (
        new_df["Category"] == "Symbiodiniaceae")), "Unknown Symbiodiniaceae", new_df['Category'])
    print(new_df["Biotype"].unique())
    return new_df


def heatmap_me_please(df_adj, df_val, name, threshold, zscoreparams, namescale):
    # check file
    print("Check files")
    if not df_val.empty:
        print(df_val.head())
    if not df_adj.empty:
        print(df_adj.head())
    # define row_colors
    row_colors = pd.concat([df_val["Biotype"].map(biotype_colors),
                            df_val["Category"].map(category_colors),
                            df_adj.applymap(lambda x: binary_colors[x])], axis=1)
    # select columns to plot
    df_logfc = df_val.loc[:, df_val.columns.str.contains("M_|_mean") == True]
    if (len(df_logfc.columns) <= 4):
        print("Few columns specific params")
        figsize = (4 + len(df_logfc.columns) * 0.9, 2 + len(df_logfc)/4)
        dendro = (.2, 0)
        bbox_bin = (0.35, 0)
        bbox_bio = (0.355, 0)
        bbox_cat = (0.55, 0)
    else:
        figsize = (2 + len(df_logfc.columns) * .9, 2 + len(df_logfc)/4)
        dendro = (.2, 0)
        bbox_bin = (0.3, 0)
        bbox_bio = (0.35, 0)
        bbox_cat = (0.5, 0)
    # clustermap
    g = sns.clustermap(df_logfc,
                       z_score=zscoreparams,
                       row_colors=row_colors,
                       # adapts on the number of row_colors
                       colors_ratio=0.4/len(row_colors.columns),
                       col_cluster=False,  # no clustering on columns
                       row_cluster=True,  # explicit clustering on rows
                       cmap="coolwarm",  # vlag, viridis, Spectral, icefire, Blues
                       dendrogram_ratio=dendro,
                       cbar_pos=(0.06, -0.005-5/(len(df_logfc) +
                                                 10), .03, 5/(len(df_logfc)+10)),
                       figsize=figsize,
                       linewidths=0.9)
    g.ax_cbar.set_title(namescale, loc="left")
    # 'left', 'center', 'right'
    # g.ax_heatmap.set_xlabel(name, loc="left")
    # Create biotype legend
    handles = [Patch(facecolor=biotype_colors[name])
               for name in biotype_colors]
    bio_legend = plt.legend(handles,
                            biotype_colors,
                            title='Biotype',
                            bbox_to_anchor=bbox_bio,
                            bbox_transform=plt.gcf().transFigure,
                            loc=("upper left"),
                            frameon=False)
    bio_legend._legend_box.align = "left"
    plt.gca().add_artist(bio_legend)
    # Create Category legend
    handles_cat = [Patch(facecolor=category_colors[name])
                   for name in category_colors]
    cat_legend = plt.legend(handles_cat,
                            category_colors,
                            title='Category',
                            bbox_to_anchor=bbox_cat,
                            bbox_transform=plt.gcf().transFigure,
                            loc=("upper left"),
                            frameon=False)
    cat_legend._legend_box.align = "left"
    plt.gca().add_artist(cat_legend)
    # Create binary legend
    handles_binary = [Patch(facecolor=binary_colors[name])
                      for name in binary_colors]
    sign_legend = plt.legend(handles_binary,
                             binary_colors,
                             title='Significance',
                             bbox_to_anchor=bbox_bin,
                             bbox_transform=plt.gcf().transFigure,
                             frameon=False)
    sign_legend._legend_box.align = "left"
    plt.gca().add_artist(sign_legend)
    # save figs
    plt.savefig(OUT_PATH + name + "_heatmap" + str(
        threshold) + ".pdf", bbox_inches='tight')  # bbox_inches for no crop
    plt.savefig(OUT_PATH + name + "_heatmap" +
                str(threshold) + ".svg", bbox_inches='tight')


#################################
# define colors

binary_colors = {1: '#000', 0: '#fff'}
print(binary_colors)

category_colors = {'Calcification': '#00BFFF',
                   'Transport & signalization': '#0000FF',
                   'Stress response/defense': '#FF00FF',
                   'Energetic metabolism': '#4B0082',
                   'Metal and ion binding': '#808080',
                   'Neural metabolism': '#FFFF00',
                   'Protein metabolism': '#FFA500',
                   'Others metabolisms': '#F08080',
                   'Splicing and transcription': '#FFC0CB',
                   'Mitochondrial': '#FF0000',
                   'Unknown coral': '#A52A2A',
                   'Symbiodiniaceae': '#98FB98',
                   'Unknown Symbiodiniaceae': '#2E8B57'}
print(category_colors)

biotype_colors = {'mRNA': '#48D1CC',
                  'ncRNA': '#4682B4',
                  'other_RNA': '#191970'}
print(biotype_colors)


#################################
# Execution

filt_tab = pd.read_csv(FILT_TAB, sep="\t", header=0)
print("Import of 'all dataframes in one' after filtering")
print(filt_tab)

# open log file
with open(LOG_FILE, 'w') as f:
    with redirect_stdout(f):
        # create heatmap files
        df_log, df_pval, df_mean, df_toplot = create_file_for_heatmap(
            filt_tab, THRESHOLD)
        # reduce biotype name
        df_log = biotype_reduction(df_log)
        print("Lenght of log file used for heatmap")
        print(len(df_log))
        df_mean = biotype_reduction(df_mean)
        print(len(df_mean))

        #################################
        # Heatmaps
        heatmap_me_please(df_pval, df_log,
                          "M_values_all_comparisons", THRESHOLD, None, 'Scale')

        heatmap_me_please(df_pval, df_mean, "Mean_count_zscore\nall_comparisons",
                          THRESHOLD, 0, 'Zscore')  # zscore params 0 rows or 1 columns

        #################################
        # partial plots for better visualization
        if (len(df_toplot.index) <= 2000):
            # split by time

            #################################
            # T3

            # drop unusefull columns
            df_logT = df_log.loc[:,
                                 df_log.columns.str.contains("D0|D110|Biotype_y") == False]
            df_pvalT = df_pval.loc[:, df_pval.columns.str.contains(
                "D0|D110") == False]
            df_meanT = df_mean.loc[:, df_mean.columns.str.contains(
                "D0|D110|Biotype_y") == False]

            # drop null rows
            df_pvalT = df_pvalT.loc[(df_pvalT != 0).any(1)]
            print("Values for day 3")
            print(len(df_pvalT.index))

            # filter others to keep only non null rows
            df_logT = df_logT.loc[df_logT.index.intersection(df_pvalT.index)]
            print(len(df_logT.index))
            df_meanT = df_meanT.loc[df_meanT.index.intersection(
                df_pvalT.index)]
            print(len(df_meanT.index))

            # plots
            heatmap_me_please(df_pvalT, df_logT, "M_values_T3",
                              THRESHOLD, None, 'Scale')
            # zscore params 0 rows or 1 columns
            heatmap_me_please(df_pvalT, df_meanT,
                              "zscore_mean_T3", THRESHOLD, 0, 'Zscore')

            #################################
            # T110

            # drop unusefull columns
            df_logT110 = df_log.loc[:,
                                    df_log.columns.str.contains("D0|D3|Biotype_y") == False]
            df_pvalT110 = df_pval.loc[:,
                                      df_pval.columns.str.contains("D0|D3") == False]
            df_meanT110 = df_mean.loc[:,
                                      df_mean.columns.str.contains("D0|D3|Biotype_y") == False]

            # drop null rows
            df_pvalT110 = df_pvalT110.loc[(df_pvalT110 != 0).any(1)]
            print("Values for day 110")
            print(len(df_pvalT110.index))

            # filter others to keep only non null rows
            df_logT110 = df_logT110.loc[df_logT110.index.intersection(
                df_pvalT110.index)]
            print(len(df_logT110.index))
            df_meanT110 = df_meanT110.loc[df_meanT110.index.intersection(
                df_pvalT110.index)]
            print(len(df_meanT110.index))

            # plots
            heatmap_me_please(df_pvalT110, df_logT110,
                              "M_values_T110", THRESHOLD, None, 'Scale')
            heatmap_me_please(df_pvalT110, df_meanT110,
                              "zscore_meanT110", THRESHOLD, 0, 'Zscore')

        else:
            print("Too much elements to plot. Please change your selection.")
