#!/bin/env python

###########################################
# check alignment on new reference genome #
###########################################

# Author: RipollJ
# Created: 2023/02/09
# License: License CC-BY-NC 4.0
# Last update: 2023/02/09

#################################

# import dependencies
import pandas as pd
from contextlib import redirect_stdout

print(pd.__version__)

# All snakemake variables
snakemake = snakemake  # suppress undeclared variable error

LOG_FILE = snakemake.log["log"]
INPUT_BLAST = snakemake.input["blast_file"]
INPUT_DIFF = snakemake.input["diff_res"]
OUTPUT = snakemake.output['ids_to_filt']
OUTPUT_DIFF = snakemake.output['ids_in_diff']

#################################

# open log file
with open(LOG_FILE, 'w') as f:
    with redirect_stdout(f):
        # import blast results
        data = pd.read_csv(INPUT_BLAST,
                           sep="\t",
                           comment="#",
                           names=["query-sequence id",
                                  "ref sequence id",
                                  "Percent identity of alignment",
                                  "Not used 1",
                                  "Not used 2",
                                  "Not used 3",
                                  "start on query",
                                  "stop on query",
                                  "start on ref",
                                  "stop on ref",
                                  "Not used 4",
                                  "Not used 5",
                                  "aln score",
                                  "query strand",
                                  "ref strand",
                                  "query read length",
                                  "BTOP",
                                  "Number of different alignments reported for query",
                                  "Not used 6",
                                  "alignments that belong to a single fragment",
                                  "Reverse complemented unaligned query",
                                  "Unaligned sequence at the end of the query, or ‘-‘",
                                  "Reference identifier where the mate is aligned",
                                  "Alignment start position on the reference sequence for the mate, or ‘-‘ if no alignment",
                                  "Composite alignment score for all exons that belong to the fragment"
                                  ])
        print("Blast results:")
        print(len(data))
        # keep interesting column
        data2 = data.drop(
            columns={"Not used 1",
                     "Not used 2",
                     "Not used 3",
                     "Not used 4",
                     "Not used 5",
                     "Not used 6"})

        #################################
        # Common between assembled contigs and reference genome

        filt_data_75 = data2[data2["Percent identity of alignment"] > 75]
        print("Matched contigs with 75%+")
        print(len(filt_data_75))

        filt_data = data2[data2["Percent identity of alignment"] > 97]
        print("Matched with 97%+")
        print(len(filt_data))

        filt_data_99 = data2[data2["Percent identity of alignment"] > 99]
        print("Matched with 99%+")
        print(len(filt_data_99))
        print(filt_data_99.head())

        #################################
        # Common with DGE results

        noiseq = pd.read_csv(INPUT_DIFF,
                             sep="\t")
        print(noiseq.head())

        # intersect between ids from DGE results and mapped on new ref genome
        common_set_tot = set(set(noiseq["Contigs"]) & set(
            data2["query-sequence id"]))
        print("with entire matched sequences :")
        print(len(common_set_tot))

        # intersect between ids from DGE results and mapped on new ref genome
        common_set_97 = set(set(noiseq["Contigs"]) &
                            set(filt_data["query-sequence id"]))
        print("for 97% and + matched sequences :")
        print(len(common_set_97))

        # intersect between ids from DGE results and mapped on new ref genome
        common_set_75 = set(set(noiseq["Contigs"]) & set(
            filt_data_75["query-sequence id"]))
        print("for 75% and + matched sequences :")
        print(len(common_set_75))

        # intersect between ids from DGE results and mapped on new ref genome
        common_set_99 = set(set(noiseq["Contigs"]) & set(
            filt_data_99["query-sequence id"]))
        print("for 99% and + matched sequences :")
        print(len(common_set_99))

        #################################
        # Check potential errors in Gene Name attributions

        error_tot = noiseq[~noiseq["Gene_Name"].str.contains(
            "Stylophora|Pocillopora|Acropora|Symbiodinium")]
        error_tot.to_csv(OUTPUT,
                         sep="\t",
                         index=False)
        print(error_tot)

        # explore 99% match results in DGE
        filt_dge = noiseq[noiseq["Contigs"].isin(common_set_99)]
        filt_dge.to_csv(OUTPUT_DIFF,
                        sep="\t",
                         index=False)
        print(filt_dge)

        error = filt_dge[~filt_dge["Gene_Name"].str.contains(
            "Stylophora|Pocillopora|Acropora|Symbiodinium")]
        print(error)

#################################