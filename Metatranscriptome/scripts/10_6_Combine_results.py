#!/usr/bin/env python
# coding: utf-8

# Combine results
# - auteur: Julie Ripoll
# - date: 2022-07-21


# import des modules
import os
import os.path
import pandas as pd
import numpy as np
import glob2 as glob  # for multiple files import
import seaborn as sns
from contextlib import redirect_stdout

pd.__version__
np.__version__
sns.__version__

def get_id(path):
    return os.path.basename(path).rsplit(".", 2)[0]


def get_file_df(path, maincol, indexcol, sepcol):
    name = get_id(path)
    test = pd.read_csv(path, sep=sepcol, index_col=indexcol)
    test[name] = test[maincol]
    return test[name]


def rn(df, suffix='-dup-'):
    appendents = (
        suffix + df.groupby(level=0).cumcount().astype(str).replace('0', '')).replace(suffix, '')
    return df.set_index(df.index + appendents)


def replace_all(text, dic):
    for i, j in dic.items():
        text = text.replace(i, j)
    return text


def combine_files(all_dfs, threshold, all_filenames, genes):
    """Combine and filter dataframes"""
    # drop by Name
    filt_df = all_dfs.drop(['ranking', 'Length', 'GC'], axis=1)
    # drop duplicated columns
    filt_df = filt_df.loc[:, ~filt_df.columns.duplicated()].copy()
    # order columns
    filt_df = filt_df.reindex(columns=sorted(filt_df.columns))
    # name index
    filt_df.index.name = 'Contigs'
    # merge with gene name
    filt_df2 = filt_df.merge(genes, on="Contigs")
    # merge df_log with all_info
    df_log_info = filt_df2
    print(df_log_info["Category"].unique())
    print("WARNINGS verify losses due to categorization")
    if (len(filt_df) != len(df_log_info)):
        print("losses with all_info:")
        print("Number of elements in df before merge")
        print(len(filt_df))
        print("Number of elements in df after merge")
        print(len(df_log_info))
    else:
        print("Merge is ok, no losses")

    # filter by threshold
    s_filter = "None"
    for filename in all_filenames:
        # extract comparison name
        basename = filename.split("/")[5]  # warnings depends on your path
        suffix = basename.split("_")[1]
        print(suffix)
        if not isinstance(s_filter, pd.Series):
            s_filter = (filt_df2[f"prob_{suffix}"] >= threshold)
        else:
            s_filter |= (filt_df2[f"prob_{suffix}"] >= threshold)

    df_toplot = filt_df2[s_filter]
    df_toplot = df_toplot.rename(columns=lambda x: replace_all(
        x, {"T0": "D0", "T3": "D3", "T110": "D110"}))
    df_toplot = df_toplot.rename(columns={"Biotype_x": "Biotype"})

    return df_toplot, filt_df2


###########
# open log file
with open(snakemake.log["log"], 'w') as f:
    with redirect_stdout(f):
        # using contextmanager to redirect print to output.log

        # Import dataframes
        genes = pd.read_csv(
            snakemake.input["genes"], sep="\t", low_memory=False, header=0,  index_col=[0])
        print("Import Gene names informations")
        genes = genes[["Gene_Name", "Biotype", "Category"]]
        print(genes.head())

        # import files
        all_filenames = glob.glob(snakemake.params["files"])
        print(all_filenames)

        # check duplicates
        for file in all_filenames:
            logf = pd.read_csv(file, sep="\t")
            basename = file  # os.path.basename(file)
            print(basename)
            print("Ok") if logf.index.is_unique else "Duplicates are presents"

        # Combine dataframes
        all_df = pd.DataFrame()

        for filename in all_filenames:
            # read files
            df = pd.read_csv(filename, sep="\t", index_col=[0], header=0)
            # extract comparison name
            basename = filename.split("/")[5]  # warnings depends on your path
            suffix = basename.split("_")[1]
            print(suffix)
            # add as a suffix
            df = df.rename(columns={"M": "M" + "_" + suffix,
                                    "D": "D" + "_" + suffix,
                                    "prob": "prob" + "_" + suffix})
            # append all files
            all_df = pd.concat([all_df, df], axis=1)

        # create combine files
        df_toplot, filt_df2 = combine_files(
            all_df, snakemake.params["threshold"], all_filenames, genes)

        # save df
        df_toplot.to_csv(snakemake.output["all_in_one_filtered"],
                         sep="\t", header=True, index=True)  # All_comparisons_in_one_df_filtered.tsv
        filt_df2.to_csv(snakemake.output["all_in_one"],
                        sep="\t", header=True, index=True)  # All_comparisons_in_one_df.tsv
