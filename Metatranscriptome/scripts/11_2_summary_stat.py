#!/bin/env python

#################
## Summary DGE ##
#################

# Author: RipollJ
# Created: 2022-08-18
# License: License CC-BY-NC 4.0
# Last update: 2022-08-18

#################################

import pandas as pd
from contextlib import redirect_stdout

# All snakemake variables
snakemake = snakemake  # suppress undeclared variable error

LOG_FILE = snakemake.log["log"]
INPUT_FILES = snakemake.input["files"]
INPUT_IDS = snakemake.input["ids_to_filt"]
OUTPUT_STATS = snakemake.output['stats']
OUTPUT_TAB = snakemake.output['filt_tab']


#################################


def retrieve_comparison_up_down_count(df_log: pd.DataFrame, df_pval: pd.DataFrame):
    for col in df_log.columns:
        # keep significative M value
        df_pval_cols = df_pval[df_pval[col] >= 0.99][col]
        df_log_cols = df_log[df_log.index.isin(df_pval_cols.index)][col]

        # up part
        up = df_log_cols[df_log_cols > 0]

        # down part
        dn = df_log_cols[df_log_cols < 0]

        yield {"Comparison": col, "DET_UP": len(up), "DET_DOWN": len(dn)}


with open(LOG_FILE, 'w') as f:
    with redirect_stdout(f):

        # import files and keep basename
        data = pd.read_csv(INPUT_FILES, sep="\t",
                           header=0, index_col="Contigs")
        basename = INPUT_FILES
        print(basename)
        print(data)

        # import of ids to filt according to new references published for Acropora and Pocillopora
        ids_to_filt = pd.read_csv(INPUT_IDS, sep="\t", header=0)
        basename2 = INPUT_IDS
        print(basename2)
        print(ids_to_filt)

        # filtering of ids defined as unvalidated
        df_filt2 = data[~data.index.isin(ids_to_filt["Contigs"])]

        # filtering of remaining rRNA
        df_filt2 = df_filt2[~df_filt2["Gene_Name"].str.contains("28S")]
        print(df_filt2.describe())

        # export filtered table
        df_filt2.to_csv(OUTPUT_TAB, sep="\t")

        # filter to keep log value (= M values)
        df_log = df_filt2.loc[:,  df_filt2.columns.str.contains("M_")]
        df_log.columns = df_log.columns.str.replace("M_", "")
        print(df_log)

        # filter to keep p-value (= prob values)
        df_pval = df_filt2.loc[:,  df_filt2.columns.str.contains("prob_")]
        df_pval.columns = df_pval.columns.str.replace("prob_", "")
        print(df_pval)

        summary = pd.DataFrame(
            retrieve_comparison_up_down_count(df_log, df_pval))
        print(summary)

        # export file
        summary.to_csv(OUTPUT_STATS, sep="\t")
