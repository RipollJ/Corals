#!/usr/bin/env python
# coding: utf-8

# Pie chart for day 0
# - auteur: Julie Ripoll
# - date: 2023-11-28

#################################
# import des modules

import os
import os.path
import pandas as pd
import numpy as np
import glob2 as glob  # for multiple files import
import matplotlib.pyplot as plt
from matplotlib.patches import Patch
import seaborn as sns
from contextlib import redirect_stdout

# modules versions
print(pd.__version__)
print(np.__version__)
print(sns.__version__)

# show dir
os.getcwd()

#################################
# All snakemake variables

snakemake = snakemake  # suppress undeclared variable error

LOG_FILE = snakemake.log["log"]
ACRO = snakemake.input["filt_tab_acro"]
POCILLO = snakemake.input["filt_tab_pocillo"]
PNG_FILE = snakemake.output["png_file"]
SVG_FILE = snakemake.output["svg_file"]
PDF_FILE = snakemake.output["pdf_file"]

#################################


def unique_by(arr, key):
    seen = set()
    acc = []
    for i in arr:
        id = key(i)
        if id not in seen:
            seen.add(id)
            acc.append(i)
    return acc


def multi_facet_pieplot(dataframe, category_colors):
    """
    Two circles one for up and one for down by species
    """
    fig, axes = plt.subplots(nrows=2, ncols=2, layout="tight")

    def align_labels(labels):
        for text in labels:
            x, y = text.get_position()
            h_align = 'left' if x > 0 else 'right'
            v_align = 'bottom' if y > 0 else 'top'
            text.set(ha=h_align, va=v_align)

    def autopct_format(values):
        def my_format(pct):
            total = sum(values)
            val = int(round(pct*total/100.0))
            return '{v:d}'.format(v=val)
        return my_format

    handles = []
    labels = []
    for ax, col in zip(axes.flat, dataframe.columns):
        values = dataframe[col].dropna()
        artists = ax.pie(values, autopct=autopct_format(
            values), pctdistance=1.05, colors=map(lambda k: category_colors[k], values.index))
        if ax.get_subplotspec().is_first_col():
            ax.set(ylabel=f"$\\it{{{col[0]}}}$")
        if ax.get_subplotspec().is_first_row():
            ax.set(title=col[1], aspect='equal')
        align_labels(artists[-1])

        handles.extend(artists[0])
        labels.extend(values.index)

    handles, labels = zip(*unique_by(zip(handles, labels),
                          lambda w: w[0].properties()["facecolor"]))
    fig.legend(handles, labels, bbox_to_anchor=(0.85, 0.5), loc='center left')
    return fig


#################################

# define colors
category_colors = {'Calcification': '#00BFFF',
                   'Transport & signalization': '#0000FF',
                   'Stress response/defense': '#FF00FF',
                   'Energetic metabolism': '#4B0082',
                   'Metal and ion binding': '#808080',
                   'Neural metabolism': '#FFFF00',
                   'Protein metabolism': '#FFA500',
                   'Others metabolisms': '#F08080',
                   'Splicing and transcription': '#FFC0CB',
                   'Mitochondrial': '#FF0000',
                   'Unknown coral': '#A52A2A',
                   'Symbiodiniaceae': '#98FB98',
                   'Unknown Symbiodiniaceae': '#2E8B57'}

#################################

# open log file
with open(LOG_FILE, 'w') as f:
    with redirect_stdout(f):
        # Import dataframes
        # Acropora
        acropora_df = pd.read_csv(ACRO, sep="\t")
        print(acropora_df.head())
        print(acropora_df["Category"].unique())
        # Pocillopora
        pocillo_df = pd.read_csv(POCILLO, sep="\t")
        print(pocillo_df.head())
        print(pocillo_df["Category"].unique())

        # Filtering
        # Acropora
        day0_acro = acropora_df.loc[:, acropora_df.columns.str.contains(
            "M_C.D0-V.D0|prob_C.D0-V.D0|Biotype|Contigs|Gene_Name|Category")]
        # keep significative
        day0_sig_acro = day0_acro[day0_acro["prob_C.D0-V.D0"] > 0.99]
        day0_sig_acro["Genes"] = np.where(
            day0_sig_acro["M_C.D0-V.D0"] > 0, "UP", "DOWN")
        print("Acropora:", len(day0_sig_acro))
        print(day0_sig_acro.head())
        # Pocillopora
        day0_pocillo = pocillo_df.loc[:, pocillo_df.columns.str.contains(
            "M_C.D0-V.D0|prob_C.D0-V.D0|Biotype|Contigs|Gene_Name|Category")]
        # keep significative
        day0_sig_pocillo = day0_pocillo[day0_pocillo["prob_C.D0-V.D0"] > 0.99]
        day0_sig_pocillo["Genes"] = np.where(
            day0_sig_pocillo["M_C.D0-V.D0"] > 0, "UP", "DOWN")
        print("Pocillopora:", len(day0_sig_pocillo))
        print(day0_sig_pocillo.head())

        # prepare dataframes for plot
        size_df_acro = day0_sig_acro.groupby(
            ["Category", "Genes"]).size().unstack(1)
        size_df_acro = size_df_acro[['UP', 'DOWN']]
        print(size_df_acro)
        size_df_pocillo = day0_sig_pocillo.groupby(
            ["Category", "Genes"]).size().unstack(1)
        size_df_pocillo = size_df_pocillo[['UP', 'DOWN']]
        print(size_df_pocillo)

        result = pd.concat([size_df_acro, size_df_pocillo], axis=1, join="outer", keys=[
                           "A. millepora", "P. acuta"])
        print(result)

        fig = multi_facet_pieplot(result, category_colors)
        fig.savefig(PNG_FILE, bbox_inches='tight')
        fig.savefig(PDF_FILE, bbox_inches='tight')
        fig.savefig(SVG_FILE, format="svg", bbox_inches='tight')
