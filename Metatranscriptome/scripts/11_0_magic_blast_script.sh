#!/bin/bash

# compare meagihit assembly with reference genome
## require: magicblast v.1.7.0, blast and samtools
## date: 2023-02-09

helpFunction() {
    echo ""
    echo "Usage: $0 -a path -b path_to_db_acro -c megahit_acro"
    echo -e "\t-a path"
    echo -e "\t-b path_to_db_acro"
    echo -e "\t-c megahit_acro"
    echo -e "\t-d path_to_db_pocillo"
    echo -e "\t-e megahit_pocillo"
    echo -e "\t-f number of threads"
    exit 1 # Exit script after printing help
}

while getopts "a:b:c:d:e:f:" opt; do
    case "$opt" in
    a) BASE_PATH="$OPTARG" ;;
    b) b="$OPTARG" ;;
    c) c="$OPTARG" ;;
    d) d="$OPTARG" ;;
    e) e="$OPTARG" ;;
    f) threads="$OPTARG" ;;
    ?) helpFunction ;; # Print helpFunction in case parameter is non-existent
    esac
done

# Print helpFunction in case parameters are empty
if [ -z "$BASE_PATH" ] || [ -z "$b" ] || [ -z "$threads" ]; then
    echo "Some or all of the parameters are empty"
    helpFunction
fi

# Begin script in case all parameters are correct
echo "$BASE_PATH"
echo "$b"
echo "$threads"

# Output path
mkdir $BASE_PATH/Acropora/new_ref_a_millepora
mkdir $BASE_PATH/Pocillopora/new_ref_p_acuta

###############################################################################
# MAGICBLAST

#  makeblastdb [-h] [-help] [-in input_file] [-input_type type]
#     -dbtype molecule_type [-title database_title] [-parse_seqids]
#     [-hash_index] [-mask_data mask_data_files] [-mask_id mask_algo_ids]
#     [-mask_desc mask_algo_descriptions] [-gi_mask]
#     [-gi_mask_name gi_based_mask_names] [-out database_name]
#     [-max_file_sz number_of_bytes] [-logfile File_Name] [-taxid TaxID]
#     [-taxid_map TaxIDMapFile] [-version]

# https://ncbi.github.io/magicblast/doc/output.html

#### Acropora
# make db with reference genome
makeblastdb -in $b -out $BASE_PATH/Acropora/new_ref_a_millepora -parse_seqids -dbtype nucl
# match with reference genome
magicblast -query $c -db $BASE_PATH/Acropora/new_ref_a_millepora -num_threads $threads >$BASE_PATH/Acropora/check_on_new_genome.sam
magicblast -query $c -db $BASE_PATH/Acropora/new_ref_a_millepora -num_threads $threads -outfmt tabular >$BASE_PATH/Acropora/check_on_new_genome.blast

## OPTIONAL
# # make db with reference transcripts
# makeblastdb -in ./Databases/Amil_v2.01/Amil.all.maker.transcripts.fasta  -parse_seqids -dbtype nucl -out $BASE_PATH/Acropora/transcripts/AM_transcripts
# # match with reference transcripts
# magicblast -query $c -db $BASE_PATH/Acropora/transcripts/AM_transcripts -num_threads $threads > $BASE_PATH/Acropora/transcripts/align_on_transcripts.sam
# magicblast -query $c -db $BASE_PATH/Acropora/transcripts/AM_transcripts -num_threads $threads -outfmt tabular > $BASE_PATH/Acropora/transcripts/align_on_transcripts.blast

#####
## Pocillopora
# make db with reference genome
makeblastdb -in $d -out $BASE_PATH/Pocillopora/new_ref_p_acuta -parse_seqids -dbtype nucl
# match with reference genome
magicblast -query $e -db $BASE_PATH/Pocillopora/new_ref_p_acuta -num_threads $threads >$BASE_PATH/Pocillopora/check_on_new_genome.sam
magicblast -query $e -db $BASE_PATH/Pocillopora/new_ref_p_acuta -num_threads $threads -outfmt tabular >$BASE_PATH/Pocillopora/check_on_new_genome.blast

#####
# Tabular output format

# The tabular output format shows one alignment per line with these tab delimited fields:

#     Query/read sequence identifier
#     Reference sequence identifier
#     Percent identity of the alignment
#     Not used
#     Not used
#     Not used
#     Alignment start position on the query sequence
#     Alignment stop position on the query sequence
#     Alignment start position on the reference sequence
#     Alignment stop position on the reference sequence
#     Not used
#     Not used
#     Alignment score
#     Query strand
#     Reference sequence strand
#     Query/read length
#     Alignment as extended BTOP string This is the same BTOP string as in BLAST tabular output with a few extensions:
#         a number represents this many matches,
#         two bases represent a mismatch and show query and reference base,
#         base and gap or gap and base, show a gap in query or reference,
#         ^<number>^ represents an intron of this number of bases,
#         _<number>_ represents an insertion (gap in reference) of this number of bases,
#         %<number>% represents a deletion (gap in read) of this number of bases,
#         (<number>) shows number of query bases that are shared between two parts of a spliced alignment; used when proper splice sites were not found
#     Number of different alignments reported for this query sequence
#     Not used
#     Compartment - a unique identifier for all alignments that belong to a single fragment. These can be two alignments for a pair of reads or alignments to exons that were not spliced.
#     Reverse complemented unaligned query sequence from the beginning of the query, or ‘-‘ if the query aligns to the left edge
#     Unaligned sequence at the end of the query, or ‘-‘
#     Reference sequence identifier where the mate is aligned, if different from the identifier in column 2, otherwise ‘-‘
#     Alignment start position on the reference sequence for the mate, or ‘-‘ if no alignment for the mate was found; a negative number denotes a divergent pair
#     Composite alignment score for all exons that belong to the fragment

###############################################################################
# Stats

# sort and export as bam
samtools sort $BASE_PATH/Acropora/check_on_new_genome.sam -O BAM -@ 24 >$BASE_PATH/Acropora/check_on_new_genome.bam

# Proportion of the Reads that Mapped to the Reference
samtools flagstat $BASE_PATH/Acropora/check_on_new_genome.bam >$BASE_PATH/Acropora/flagstat.txt

# Breadth of Coverage
samtools depth -a $BASE_PATH/Acropora/check_on_new_genome.bam | awk '{c++; if($3>0) total+=1}END{print (total/c)*100}'
### 17.3541

# all stats
samtools stats $BASE_PATH/Acropora/check_on_new_genome.bam >$BASE_PATH/Acropora/stats.txt

# GC the size of GC-depth bins
cat $BASE_PATH/Acropora/stats.txt | grep ^GCD | cut -f 2- >$BASE_PATH/Acropora/gc_stat.txt

# Coverage distribution. Use `grep ^COV | cut -f 2-` to extract this part.
cat $BASE_PATH/Acropora/stats.txt | grep ^COV | cut -f 2- >$BASE_PATH/Acropora/cov_stat.txt

# Read lengths - last fragments. Use `grep ^LRL | cut -f 2-` to extract this part. The columns are: read length, count
cat $BASE_PATH/Acropora/stats.txt | grep ^LRL | cut -f 2- >$BASE_PATH/Acropora/rl_stat.txt

# all sub results
grep 'grep' $BASE_PATH/Acropora/stats.txt
# Summary Numbers. Use `grep ^SN | cut -f 2-` to extract this part.
# First Fragment Qualities. Use `grep ^FFQ | cut -f 2-` to extract this part.
# Last Fragment Qualities. Use `grep ^LFQ | cut -f 2-` to extract this part.
# GC Content of first fragments. Use `grep ^GCF | cut -f 2-` to extract this part.
# GC Content of last fragments. Use `grep ^GCL | cut -f 2-` to extract this part.
# ACGT content per cycle. Use `grep ^GCC | cut -f 2-` to extract this part. The columns are: cycle; A,C,G,T base counts as a percentage of all A/C/G/T bases [%]; and N and O counts as a percentage of all A/C/G/T bases [%]
# ACGT content per cycle, read oriented. Use `grep ^GCT | cut -f 2-` to extract this part. The columns are: cycle; A,C,G,T base counts as a percentage of all A/C/G/T bases [%]
# ACGT content per cycle for first fragments. Use `grep ^FBC | cut -f 2-` to extract this part. The columns are: cycle; A,C,G,T base counts as a percentage of all A/C/G/T bases [%]; and N and O counts as a percentage of all A/C/G/T bases [%]
# ACGT raw counters for first fragments. Use `grep ^FTC | cut -f 2-` to extract this part. The columns are: A,C,G,T,N base counters
# ACGT content per cycle for last fragments. Use `grep ^LBC | cut -f 2-` to extract this part. The columns are: cycle; A,C,G,T base counts as a percentage of all A/C/G/T bases [%]; and N and O counts as a percentage of all A/C/G/T bases [%]
# ACGT raw counters for last fragments. Use `grep ^LTC | cut -f 2-` to extract this part. The columns are: A,C,G,T,N base counters
# Insert sizes. Use `grep ^IS | cut -f 2-` to extract this part. The columns are: insert size, pairs total, inward oriented pairs, outward oriented pairs, other pairs
# Read lengths. Use `grep ^RL | cut -f 2-` to extract this part. The columns are: read length, count
# Read lengths - first fragments. Use `grep ^FRL | cut -f 2-` to extract this part. The columns are: read length, count
# Read lengths - last fragments. Use `grep ^LRL | cut -f 2-` to extract this part. The columns are: read length, count
# Indel distribution. Use `grep ^ID | cut -f 2-` to extract this part. The columns are: length, number of insertions, number of deletions
# Indels per cycle. Use `grep ^IC | cut -f 2-` to extract this part. The columns are: cycle, number of insertions (fwd), .. (rev) , number of deletions (fwd), .. (rev)
# Coverage distribution. Use `grep ^COV | cut -f 2-` to extract this part.
# GC-depth. Use `grep ^GCD | cut -f 2-` to extract this part. The columns are: GC%, unique sequence percentiles, 10th, 25th, 50th, 75th and 90th depth percentile

# Summary Numbers.
cat $BASE_PATH/Acropora/stats.txt | grep ^SN | cut -f 2- >$BASE_PATH/Acropora/numbers.txt

###############################################################################
# Samtools

# sort and export as bam
samtools sort $BASE_PATH/Pocillopora/check_on_new_genome.sam -O BAM -@ 24 >$BASE_PATH/Pocillopora/check_on_new_genome.bam

# Proportion of the Reads that Mapped to the Reference
samtools flagstat $BASE_PATH/Pocillopora/check_on_new_genome.bam >$BASE_PATH/Pocillopora/flagstat.txt

# Breadth of Coverage
### The breadth of coverage is the percentage of target bases that have been sequenced for a given number of times. Hybrid sequencing approaches are being introduced to overcome problems in genome assembly and in placing highly repetitive sequence in a genome.
samtools depth -a $BASE_PATH/Pocillopora/check_on_new_genome.bam | awk '{c++; if($3>0) total+=1}END{print (total/c)*100}'
### 28.814

# GC the size of GC-depth bins
samtools stats $BASE_PATH/Pocillopora/check_on_new_genome.bam | grep ^GCD | cut -f 2- >$BASE_PATH/Pocillopora/gc_stat.txt

# Coverage distribution. Use `grep ^COV | cut -f 2-` to extract this part.
samtools stats $BASE_PATH/Pocillopora/check_on_new_genome.bam | grep ^COV | cut -f 2- >$BASE_PATH/Pocillopora/cov_stat.txt

# Read lengths - last fragments. Use `grep ^LRL | cut -f 2-` to extract this part. The columns are: read length, count
samtools stats $BASE_PATH/Pocillopora/check_on_new_genome.bam | grep ^LRL | cut -f 2- >$BASE_PATH/Pocillopora/rl_stat.txt

# Summary Numbers.
cat $BASE_PATH/Pocillopora/stats.txt | grep ^SN | cut -f 2- >$BASE_PATH/Pocillopora/numbers.txt

###############################################################################
