#!/bin/env Rscript

###################
## DGE.py Script ##
###################

# Author: Julie Ripoll
# Contact: julie.ripoll87@gmail.com
# Created: 2018-10
# Last update: 2022-07-11
# License: License CC-BY-NC 4.0


###############################################################################
# Import packages
###############################################################################

### Start
# log file with sink
sink(snakemake@log[["log"]], append = FALSE)
print("Start_time")
print(Sys.time())

# load previous session
shim2 <- snakemake
load(snakemake@input[["session2"]])

# reload snakemake S4 object for current input - output
snakemake <- shim2


###############################################################################
# annotations
###############################################################################

# input file from session2
if (length(deg) != 0) {
    # output GO
    fileToImport <- snakemake@input[["GOid"]]
    geneID2GO <- read.delim(fileToImport, sep = "\t", header = FALSE)
    colnames(geneID2GO) <- c("Contigs", "GO")
    print("GO ontologies")
    print(str(geneID2GO))
    # merge files
    TopGO <- merge(deg, geneID2GO, by = "Contigs", all = F)
    print("Annotation of DGE with GO terms")
    print(head(TopGO))
    # export tables
    write.table(TopGO,
        paste0(
            "out/DGE/",
            snakemake@wildcards$species,
            "/Comparisons/Comp_",
            snakemake@wildcards$conditions,
            "/TopGO.tsv"
        ),
        sep = "\t",
        quote = FALSE,
        row.names = FALSE
    )

    # ID_gene from blastn
    fileToImport2 <- snakemake@input[["GeneList"]]
    geneList <- read.delim(fileToImport2, sep = "\t")
    ## colnames: Contigs	ncbi_gene_ID	Species	Gene_Name	ncbi_prot_ID	Prot_ID	Isogroupsseqid_cdd	evalue_cdd	"Contigs"
    print("ID gene from blastn")
    print(str(geneList))
    # merge
    TopGene <- merge(deg, geneList, by = "Contigs", all = F)
    print("Annotation of Top Gene")
    print(head(TopGene))
    # export
    write.table(TopGene,
        paste0(
            "out/DGE/",
            snakemake@wildcards$species,
            "/Comparisons/Comp_",
            snakemake@wildcards$conditions,
            "/TopGene.tsv"
        ),
        sep = "\t",
        quote = FALSE,
        row.names = FALSE
    )
} else {
    print(paste0(
        "Comparison ",
        snakemake@wildcards$species,
        "and",
        snakemake@wildcards$conditions,
        "doesn't exist"
    ))
}

# # ID_gene from blastn
# geneList <- read.delim(snakemake@input[["GeneList"]], sep = "\t")
# print(str(geneList))
# # merge norm_count with gene info list
# TopCount <- merge(norm_count, geneList, by = "Contigs", all = F)
# print("Annotation of normalized counts")
# print(head(TopCount))
# # export
# write.table(TopCount,snakemake@output[["TopCount"]], sep = "\t", quote = FALSE, row.names = F)


###############################################################################
# end log file
sessionInfo()

# end log file
save.image(paste0(
    "out/DGE/",
    snakemake@wildcards$species,
    "/Comparisons/Comp_",
    snakemake@wildcards$conditions,
    "/Annotation.RData"
))

# End time
print("End_time")
print(Sys.time())

# End save info session
sink()

###############################################################################
# END