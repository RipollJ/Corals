import os
import sys

configfile: "Config.yml"

SAMPLE, = glob_wildcards('out/Assembling/"+SPECIES+"/{sample}_1.fastq')
SPECIES = config["species"]
THREADS = config['threads']['Normal']
ENVIRONMENT1 = "../envs/Assembly.yml"
ENVIRONMENT2 = "../envs/Align.yml"

###############################################################################

rule all:
    """

    ###################################################
    ### Alignment of reads on de novo transcriptome ###
    ###################################################

    Author: Julie Ripoll
    Contact: julie.ripoll87@gmail.com
    Created: 2018
    License: CC-BY-NC 4.0

    Modified: 2018-10-02

    Aim: Alignments of reads on the de novo transcriptome and quality checking

    This snakefile need the use of snakemake version > 3.5 for solving conda environment with the --use-conda option. 
    It needs also conda > 4.5

    Samtools version used here: 
    # wget https://github.com/samtools/samtools/releases/download/1.2/samtools-1.2.tar.bz2
    # tar xvjf samtools-1.2.tar.bz2
    # cd samtools-1.2 && make -j && cd .

    Execute as this:
    >snakemake -s Metatranscriptome/06_Align.smk --use-conda --conda-frontend mamba -p -r -j 1

    """
    input:
        mis = expand("out/Assembling/{species}/{species}.mispaired",
		            species = SPECIES)

###############################################################################

rule Align:
    """
    Aim: Aligning samples to the de novo assembled transcriptome with BBTools
    We keep the unfiltered contigs FASTA reference file after comparisons with 
    different filtered FASTA reference file performed in the 07 step
    """
    message:
        """--- ALIGN SAMPLES FOR {wildcards.species} ---"""
    conda:
        ENVIRONMENT1
    log:
        "out/Assembling/{species}/Align.log"
    input:
        Trans = "out/Assembling/{species}/megahit_out/final.contigs.fa",
        list1 = "out/Assembling/{species}/list_read_1",
        list2 = "out/Assembling/{species}/list_read_2"
    output:
        listAl = "out/Assembling/{species}/list_aln"
    shell:
        "paste {input.list1} | sed 's/_1.fastq/.sam/g' > {output.listAl} " # list of outputs
        "&& "
        "bbwrap.sh ref={input.Trans} in=$(paste {input.list1}) in2=$(paste {input.list2}) out=$(paste {output.listAl})"
        "&& "
        "exec >> {log}"


rule Merge:
    """
    Aim: Merge alignment files
    """
    message:
        """--- MERGE ALN SAMPLES FOR {wildcards.species} ---"""
    conda:
        ENVIRONMENT1
    params:
        threads = THREADS
    log:
        "out/Assembling/{species}/MergeAln.log"
    input:
        listAl = "out/Assembling/{species}/list_aln"
    output:
        Spebam = "out/Assembling/{species}/{species}.bam"
    shell:
        "samtools merge -nr1 $(paste {input.listAl}) -@ {params.threads} --output-fmt BAM -O {output.Spebam}"


rule Coverage:
    """
    Aim: Verify read coverage on the de novo assembled transcriptome
    """
    message:
        """--- CHECK COVERAGE FOR {wildcards.species} ---"""
    conda:
        ENVIRONMENT1
    log:
        "out/Assembling/{species}/Cov.log"
    input:
        Trans = "out/Assembling/{species}/megahit_out/final.contigs.fa",
        Spebam = "out/Assembling/{species}/{species}.bam"
    output:
        covspe = "out/Assembling/{species}/Aln/{species}.cov"
    shell:
        "pileup.sh ref={input.Trans} in={input.Spebam} out={output.covspe} hist=test "
        "&& "
        "exec >> {log}"
		# Output format cov:
        # ID:                Scaffold ID
        # Length:            Scaffold length
        # Ref_GC:            GC ratio of reference
        # Avg_fold:          Average fold coverage of this scaffold
        # Covered_percent:   Percent of scaffold with any coverage (only if arrays or bitsets are used)
        # Covered_bases:     Number of bases with any coverage (only if arrays or bitsets are used)
        # Plus_reads:        Number of reads mapped to plus strand
        # Minus_reads:       Number of reads mapped to minus strand
        # Read_GC:           Average GC ratio of reads mapped to this scaffold
        # Median_fold:       Median fold coverage of this scaffold (only if arrays are used)
        # Std_Dev:           Standard deviation of coverage (only if arrays are used)


rule Check_pair:
    """
    Aim: Check whether the readings are correctly matched or not, using the Samtools flag
    """
    message:
        """--- CHECK PAIRED READS FOR {wildcards.species} ---"""
    conda:
        ENVIRONMENT2
    params:
        threads = THREADS
    log:
        "out/Assembling/{species}/Pairs.log"
    input:
        logCov = "out/Assembling/{species}/Cov.log",
        Spebam = "out/Assembling/{species}/{species}.bam"
    output:
        Spebamsort = "out/Assembling/{species}/{species}.bam.sorted",
        mis = "out/Assembling/{species}/{species}.mispaired"
    shell:
        "samtools sort {input.Spebam} > {output.Spebamsort} -@ {params.threads} "
        "&& "
        "samtools view -F 0x2 {output.Spebamsort} > {output.mis} | wc -l "
        "&& "
        "samtools flagstat {input.Spebam} "
        "&& "
        "exec >> {log}"
