import os
import sys

configfile: "Config.yml"

# Conda environment
magicblast = '../envs/magicblast.yml'
Pandas = '../envs/pandas.yml'
tidyverse = '../envs/pca3d.yml'

# Config parameters
SPECIES = config["species"]
THREADS = config['threads']['Normal']
CORAL_PROJECT = config["redo"]

###############################################################################

rule all:
    """
    Conditional filtering
    =====================

    Author: Julie Ripoll
    Contact: julie.ripoll87@gmail.com
    Created: 2023
    License: CC-BY-NC 4.0
    Modified: 2023-11-28

    Aim:
    - Verify contigs with recent genomic/transcriptomic references
    - filter unknown contigs from DET results
    - summarize statistics on DET results
    - conditional filtering of DET results for article
    """
    input:
        expand('out/Metatranscriptome/DGE/{species}/Comparisons/d110.tsv', species=SPECIES) if CORAL_PROJECT == "redo" else expand('out/Metatranscriptome/DGE/{species}/Comparisons/All_comparisons_in_one_df_final.tsv', species=SPECIES)


###############################################################################

rule map_on_new_ref:
    """
    Aim: Mapping on new references of Acropora and Pocillopora according to litterature
    """
    message:
        """--- Search of annotation errors ---"""
    conda:
        magicblast
    log:
        "out/Metatranscriptome/Assembling/magicblast.log"
    input:
        ref_acro = "Databases/Amil_v2.01/Amil.v2.01.chrs.fasta",
        ref_pocillo = "Databases/P_acuta/Pocillopora_acuta_genome_v1.fasta",
        contigs_acro = "out/Metatranscriptome/Assembling/Acropora/megahit_out/final.contigs.fa",
        contigs_pocillo = "out/Metatranscriptome/Assembling/Pocillopora/megahit_out/final.contigs.fa"
    params:
        path = "out/Metatranscriptome/Assembling/",
        threads = THREADS
    output:
        sam_acro = 'out/Metatranscriptome/Assembling/Acropora/check_on_new_genome.sam',
        blast_acro = "out/Metatranscriptome/Assembling/Acropora/check_on_new_genome.blast",
        sam_pocillo = 'out/Metatranscriptome/Assembling/Pocillopora/check_on_new_genome.sam',
        blast_pocillo = "out/Metatranscriptome/Assembling/Pocillopora/check_on_new_genome.blast"
    shell:
        "bash ./Metatranscriptome/scripts/11_0_magic_blast_script.sh -a {params.path} -b {input.ref_acro} -c {input.contigs_acro} -d {input.ref_pocillo} -e {input.contigs_pocillo} -f {params.threads} "
        "> {log}"


rule filtered_data:
    """
    Aim: Filtered DET according to new references
    """
    message:
        """--- Search of annotation errors {wildcards.species} ---"""
    conda:
        Pandas
    log:
        log = "out/Metatranscriptome/Assembling/{species}/check_contig_new_ref.log"
    input:
        check = "out/Metatranscriptome/Assembling/Pocillopora/check_on_new_genome.blast",
        blast_file = 'out/Metatranscriptome/Assembling/{species}/check_on_new_genome.blast',
        diff_res = 'out/Metatranscriptome/DGE/{species}/Comparisons/All_comparisons_in_one_df_filtered_099.tsv'
    output:
        ids_to_filt = 'out/Metatranscriptome/DGE/{species}/Comparisons/All_comparisons_in_one_df_filtered_099_ID_A_SUPPRIMER.tsv',
        ids_in_diff = 'out/Metatranscriptome/DGE/{species}/Comparisons/Filtered_with_new_ref_genome_99.tsv'
    script:
        "scripts/11_1_check_contig_new_ref.py"


rule summary_stat:
    """
    Aim: Filtering of unvalid IDs according to litterature and Summary results
    """
    message:
        """--- Filtering of unvalid IDs according to litterature and Summary results of DEG transcripts {wildcards.species} ---"""
    conda:
        Pandas
    log:
        log = "out/Metatranscriptome/DGE/{species}/summary_stats.log"
    input:
        files = 'out/Metatranscriptome/DGE/{species}/Comparisons/All_comparisons_in_one_df_filtered_099.tsv',
        ids_to_filt = 'out/Metatranscriptome/DGE/{species}/Comparisons/All_comparisons_in_one_df_filtered_099_ID_A_SUPPRIMER.tsv'
    output:
        stats = 'out/Metatranscriptome/DGE/{species}/Comparisons/Summary_deg.tsv',
        filt_tab = 'out/Metatranscriptome/DGE/{species}/Comparisons/All_comparisons_in_one_df_final.tsv'
    script:
        "scripts/11_2_summary_stat.py"


rule filters_by_questions:
    """
    Aim: Filtering of differentially expressed transcripts to answer questions
    """
    message:
        """--- Filtering of unvalid IDs according to litterature and Summary results of DEG transcripts {wildcards.species} ---"""
    conda:
        tidyverse
    log:
        log = "out/Metatranscriptome/DGE/{species}/filters_by_questions.log"
    input:
        countfile = 'out/Metatranscriptome/DGE/{species}/Comparisons/All_comparisons_in_one_df_final.tsv'
    output:
        D0 = 'out/Metatranscriptome/DGE/{species}/Comparisons/d0.tsv',
        d0_bilan = 'out/Metatranscriptome/DGE/{species}/Comparisons/d0_bilan.tsv',
        CutC = 'out/Metatranscriptome/DGE/{species}/Comparisons/CutC.tsv',
        CutC_bilan = 'out/Metatranscriptome/DGE/{species}/Comparisons/CutC_bilan.tsv',
        CutV = 'out/Metatranscriptome/DGE/{species}/Comparisons/CutV.tsv',
        CutV_bilan = 'out/Metatranscriptome/DGE/{species}/Comparisons/CutV_bilan.tsv',
        OriC = 'out/Metatranscriptome/DGE/{species}/Comparisons/OriC.tsv',
        OriC_bilan = 'out/Metatranscriptome/DGE/{species}/Comparisons/OriC_bilan.tsv',
        OriV = 'out/Metatranscriptome/DGE/{species}/Comparisons/OriV.tsv',
        OriV_bilan = 'out/Metatranscriptome/DGE/{species}/Comparisons/OriV_bilan.tsv',
        D110 = 'out/Metatranscriptome/DGE/{species}/Comparisons/d110.tsv',
        d110_bilan = 'out/Metatranscriptome/DGE/{species}/Comparisons/d110_bilan.tsv'
    script:
        "scripts/11_3_filters_by_questions.py"
