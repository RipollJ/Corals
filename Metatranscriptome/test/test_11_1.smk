Pandas = '../../envs/pandas.yml'

rule all:
    input:
        "out/Metatranscriptome/test/data/check_contigs.difflog"


rule filtered_data:
    """
    Aim: Filtered DET according to new references
    """
    message:
        """--- Search of annotation errors ---"""
    conda:
        Pandas
    log:
        log = "out/Metatranscriptome/test/data/Acropora/check_contig_new_ref.log"
    input:
        blast_file = 'out/Metatranscriptome/Assembling/Acropora/check_on_new_genome.blast',
        diff_res = 'out/Metatranscriptome/DGE/Acropora/Comparisons/Heatmap/All_comparisons_in_one_df_filtered_099.tsv'
    output:
        ids_to_filt = 'out/Metatranscriptome/test/data/Acropora/Comparisons/All_comparisons_in_one_df_filtered_099_ID_A_SUPPRIMER.tsv',
        ids_in_diff = 'out/Metatranscriptome/test/data/Acropora/Comparisons/Filt_with_new_ref_genome_99.tsv'
    script:
        "../scripts/11_1_check_contig_new_ref.py"


rule test:
    input:
        genes = 'out/Metatranscriptome/test/data/Acropora/Comparisons/All_comparisons_in_one_df_filtered_099_ID_A_SUPPRIMER.tsv',
        reference = 'out/Metatranscriptome/DGE/Acropora/Comparisons/All_comparisons_in_one_df_filtered_099_ID_A_SUPPRIMER.tsv'
    output:
        "out/Metatranscriptome/test/data/check_contigs.difflog"
    shell:
        "diff -c {input.genes} {input.reference} > {output}"