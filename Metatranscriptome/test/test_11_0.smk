magicblast = '../../envs/magicblast.yml'

rule all:
    input:
        "out/Metatranscriptome/test/data/magicblast.difflog"


rule map_on_new_ref:
    """
    Aim: Mapping on new references of Acropora and Pocillopora according to litterature
    """
    message:
        """--- Search of annotation errors ---"""
    conda:
        magicblast
    log:
        "out/Metatranscriptome/Assembling/magicblast.log"
    input:
        ref_acro = "Databases/Amil_v2.01/Amil.v2.01.chrs.fasta",
        ref_pocillo = "Databases/P_acuta/Pocillopora_acuta_genome_v1.fasta",
        contigs_acro = "out/Metatranscriptome/Assembling/Acropora/megahit_out/final.contigs.fa",
        contigs_pocillo = "out/Metatranscriptome/Assembling/Pocillopora/megahit_out/final.contigs.fa"
    params:
        path = "out/Metatranscriptome/test/data/Assembling/",
        threads = 12
    output:
        sam_acro = 'out/Metatranscriptome/test/data/Assembling/Acropora/check_on_new_genome.sam',
        blast_acro = "out/Metatranscriptome/test/data/Assembling/Acropora/check_on_new_genome.blast",
        sam_pocillo = 'out/Metatranscriptome/test/data/Assembling/Pocillopora/check_on_new_genome.sam',
        blast_pocillo = "out/Metatranscriptome/test/data/Assembling/Pocillopora/check_on_new_genome.blast"
    shell:
        "bash ./Metatranscriptome/scripts/11_0_magic_blast_script.sh -a {params.path} -b {input.ref_acro} -c {input.contigs_acro} -d {input.ref_pocillo} -e {input.contigs_pocillo} -f {params.threads} "
        "> {log}"

rule test:
    input:
        genes = "out/Metatranscriptome/test/data/Assembling/Pocillopora/check_on_new_genome.blast",
        reference = "out/Metatranscriptome/Assembling/Pocillopora/check_on_new_genome_po.blast"
    output:
        "out/Metatranscriptome/test/data/magicblast.difflog"
    shell:
        "diff -c {input.genes} {input.reference} > {output}"