Pandas = '../../envs/pandas.yml'

rule all:
    input:
        "out/Metatranscriptome/test/data/combine_results.difflog"

rule combine_results:
    """
    Aim: Combine all results
    """
    conda:
        Pandas
    log:
        log = 'out/Metatranscriptome/test/data/combine_results.log'
    input:
        genes = 'out/Metatranscriptome/Acropora/Acropora_join_table_filt.txt'
    params:
        files = 'out/Metatranscriptome/DGE/Acropora/Comparisons/*/All_results.tsv',
        threshold = 0.99
    output:
        all_in_one = 'out/Metatranscriptome/test/data/Acropora/Comparisons/All_comparisons_in_one_df.tsv',
        all_in_one_filtered = 'out/Metatranscriptome/test/data/Acropora/Comparisons/All_comparisons_in_one_df_filtered_099.tsv'
    script:
        "../scripts/10_6_Combine_results.py"

rule test:
    input:
        genes = 'out/Metatranscriptome/test/data/Acropora/Comparisons/All_comparisons_in_one_df_filtered_099.tsv',
        reference = 'out/Metatranscriptome/DGE/Acropora/Comparisons/Heatmap/All_comparisons_in_one_df_filtered_099.tsv'
    output:
        "out/Metatranscriptome/test/data/combine_results.difflog"
    shell:
        "diff {input.genes} {input.reference} > {output}"
    