Pandas = '../../envs/pandas.yml'

rule all:
    input:
        "out/Metatranscriptome/test/data/Plots/day0.png"


rule day0plot:
    """
    Aim: Pie chart for comparison of day 0
    """
    message:
        """---Pie chart all in one---"""
    conda:
        Pandas
    log:
        log = 'out/Metatranscriptome/test/data/Plots/pie_chart_day0.log'
    input:
        filt_tab_acro = 'out/Metatranscriptome/test/data/Acropora/Comparisons/All_comparisons_in_one_df_final.tsv',
        filt_tab_pocillo = 'out/Metatranscriptome/test/data/Pocillopora/Comparisons/All_comparisons_in_one_df_final.tsv'
    output:
        png_file = "out/Metatranscriptome/test/data/Plots/day0.png",
        svg_file = "out/Metatranscriptome/test/data/Plots/day0.svg",
        pdf_file = "out/Metatranscriptome/test/data/Plots/day0.pdf",
    script:
        "../scripts/12_3_Plot_D0.py"

