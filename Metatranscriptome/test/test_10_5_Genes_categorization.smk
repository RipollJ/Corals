Pandas = '../../envs/pandas.yml'

rule all:
    input:
        "out/Metatranscriptome/test/data/genes_categorization.difflog"

rule categorization:
    """
    Aim: Categorizes genes
    """
    message:
        """---Categorizes genes of ---"""
    conda:
        Pandas
    log:
        log = 'out/Metatranscriptome/log/Acropora/Categorization.log'
    input:
        GeneList = "out/Metatranscriptome/Acropora/Acropora_join_table.txt",
        Biotype = 'out/Metatranscriptome/Acropora/Biotype_Acropora'
    output:
        genes = 'out/Metatranscriptome/test/data/Acropora/Acropora_join_table_filt.txt'
    script:
        "../scripts/10_5_Genes_categorization.py"

rule test:
    input:
        genes = 'out/Metatranscriptome/test/data/Acropora/Acropora_join_table_filt.txt',
        reference = 'out/Metatranscriptome/Acropora/Acropora_join_table_filt.txt'
    output:
        "out/Metatranscriptome/test/data/genes_categorization.difflog"
    shell:
        "diff {input.genes} {input.reference} > {output}"
    