Pandas = '../../envs/pandas.yml'

rule all:
    input:
        "out/Metatranscriptome/test/data/sum_stat.difflog"


rule summary_stat:
    """
    Aim: Summary results
    """
    message:
        """--- Summary results of DEG transcripts by up or down ---"""
    conda:
        Pandas
    log:
        log = "out/Metatranscriptome/test/data/Pocillopora/summary_stats.log"
    input:
        files = 'out/Metatranscriptome/DGE/Pocillopora/Comparisons/Heatmap/All_comparisons_in_one_df_filtered_099.tsv',
        ids_to_filt = 'out/Metatranscriptome/DGE/Pocillopora/Comparisons/All_comparisons_in_one_df_filtered_099_ID_A_SUPPRIMER.tsv'
    output:
        stats = 'out/Metatranscriptome/test/data/Pocillopora/Comparisons/Summary_deg_filtered.txt',
        filt_tab = 'out/Metatranscriptome/test/data/Pocillopora/Comparisons/All_comparisons_in_one_df_final.tsv'
    script:
        "../scripts/11_2_summary_stat.py"


rule test:
    input:
        genes = 'out/Metatranscriptome/test/data/Pocillopora/Comparisons/Summary_deg_filtered.txt',
        reference = 'out/Metatranscriptome/DGE/Pocillopora/Comparisons/Heatmap/Summary_deg_filtered.txt'
    output:
        "out/Metatranscriptome/test/data/sum_stat.difflog"
    shell:
        "diff -c {input.genes} {input.reference} > {output}"