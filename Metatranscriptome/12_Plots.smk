import os
import sys

configfile: "Config.yml"

# Conda environment
Pandas = '../envs/pandas.yml'
acp = '../envs/pca.yml'
acp3d = '../envs/pca3d.yml'

# Config parameters
SPECIES = config["species"]


###############################################################################

rule all:
    """
    Plots
    =====

    Author: Julie Ripoll
    Contact: julie.ripoll87@gmail.com
    Created: 2023
    License: CC-BY-NC 4.0
    Modified: 2023-11-28

    Aim: Perform heatmaps and PCA plots for the article
    """
    input:
        hist = expand('out/Metatranscriptome/DGE/{species}/Comparisons/Plots/Histogram_selected_comparisons.png', species = SPECIES),
        png_file = "out/Metatranscriptome/DGE/Plots/day0.png"


###############################################################################

rule classic_pca:
    """
    Aim: PCA on all differentially expressed results
    """
    message:
        """--- PCA on all differentially expressed results {wildcards.species} ---"""
    conda:
        acp
    log:
        log = 'out/Metatranscriptome/DGE/{species}/Comparisons/Plots/PCA.log'
    input:
        counts_deg = 'out/Metatranscriptome/DGE/{species}/Comparisons/All_comparisons_in_one_df_final.tsv'
    output:
        pca_var_12 = 'out/Metatranscriptome/DGE/{species}/Comparisons/Plots/Scaled_PCA_variables_axes12.png',
        pca_ind_12 = 'out/Metatranscriptome/DGE/{species}/Comparisons/Plots/Scaled_PCA_individuals_axes12.png',
        pca_var_13 = 'out/Metatranscriptome/DGE/{species}/Comparisons/Plots/Scaled_PCA_variables_axes13.png',
        pca_ind_13 = 'out/Metatranscriptome/DGE/{species}/Comparisons/Plots/Scaled_PCA_individuals_axes13.png',
        pca_var_23 = 'out/Metatranscriptome/DGE/{species}/Comparisons/Plots/Scaled_PCA_variables_axes23.png',
        pca_ind_23 = 'out/Metatranscriptome/DGE/{species}/Comparisons/Plots/Scaled_PCA_individuals_axes23.png',
        screeplot = 'out/Metatranscriptome/DGE/{species}/Comparisons/Plots/PCA_screeplot.png',
        vardisp = 'out/Metatranscriptome/DGE/{species}/Comparisons/Plots/PCA_dispersion_var.png',
        inddisp = 'out/Metatranscriptome/DGE/{species}/Comparisons/Plots/PCA_dispersion_ind.png',
        cluster = 'out/Metatranscriptome/DGE/{species}/Comparisons/Plots/PCA_cluster.png',
        cluster2 = 'out/Metatranscriptome/DGE/{species}/Comparisons/Plots/PCA_cluster2.png',
        Rdata = 'out/Metatranscriptome/DGE/{species}/Comparisons/Plots/PCA_scaled.Rdata'
    script:
        "scripts/12_0_ACP.R"


rule heatmap_filtered:
    """
    Aim: Create enriched heatmap with significative results
    """
    message:
        """--- Sig-Heatmap for {wildcards.species} ---"""
    conda:
        Pandas
    input:
        filt_tab = 'out/Metatranscriptome/DGE/{species}/Comparisons/All_comparisons_in_one_df_final.tsv',
        check_Rdata = 'out/Metatranscriptome/DGE/{species}/Comparisons/Plots/PCA_scaled.Rdata'
    params:
        path = 'out/Metatranscriptome/DGE/{species}/Comparisons/Plots/',
        threshold = 0.99
    output:
        logfile = 'out/Metatranscriptome/log/{species}/Plots/Heatmap_filtered.log'
    script:
        "scripts/12_1_Sig-heatmap.py"


rule histogram_diff_transcripts:
    """
    Aim: Histogram of the summary of DET
    """
    message:
        """--- Histograms of DET {wildcards.species} ---"""
    conda:
        Pandas
    log:
        log = 'out/Metatranscriptome/DGE/{species}/Comparisons/Plots/histograms_det.log'
    input:
        summary = 'out/Metatranscriptome/DGE/{species}/Comparisons/Summary_deg.tsv',
        logfile = 'out/Metatranscriptome/log/{species}/Plots/Heatmap_filtered.log'
    output:
        hist = 'out/Metatranscriptome/DGE/{species}/Comparisons/Plots/Histogram_selected_comparisons.svg',
        hist2 = 'out/Metatranscriptome/DGE/{species}/Comparisons/Plots/Histogram_selected_comparisons.png'
    script:
        "scripts/12_2_Histogram.py"


rule day0plot:
    """
    Aim: Pie chart for comparison of day 0
    """
    message:
        """---Pie chart all in one---"""
    conda:
        Pandas
    log:
        log = 'out/Metatranscriptome/DGE/Plots/pie_chart_day0.log'
    input:
        filt_tab_acro = 'out/Metatranscriptome/DGE/Acropora/Comparisons/All_comparisons_in_one_df_final.tsv',
        filt_tab_pocillo = 'out/Metatranscriptome/DGE/Pocillopora/Comparisons/All_comparisons_in_one_df_final.tsv'
    output:
        png_file = "out/Metatranscriptome/DGE/Plots/day0.png",
        svg_file = "out/Metatranscriptome/DGE/Plots/day0.svg",
        pdf_file = "out/Metatranscriptome/DGE/Plots/day0.pdf",
    script:
        "scripts/12_3_Plot_D0.py"
