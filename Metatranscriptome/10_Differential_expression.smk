import os
import sys
from queue import PriorityQueue
from snakemake.utils import R

configfile: "Config.yml"

R = '../envs/R.yml'
Pandas = '../envs/pandas.yml'
Heatmap_raw = '../envs/Rheatmap.yml'

SPECIES = config["species"]
CONDITIONS = config["conditions"]

###############################################################################

rule all:
    """

    ##########################################
    # Differential expression of transcripts #
    ##########################################

    Author: Julie Ripoll
    Contact: julie.ripoll87@gmail.com
    Created: 2018
    Last update: 2022
    License: CC-BY-NC 4.0

    Modified: 2018-02-08

    Aim: Perform statistics and plots on counts

    Note: each rule == one R session, please think to add all packages call and to log your terminal in a sink file.
    Need rpy2 from python 3 to run R.
    Run on R v3.4.4

    This snakefile need the use of snakemake version > 3.5 for solving conda environment with the --use-conda option. 
    It needs also conda > 4.5

    Execute as this:
    >snakemake -s Metatranscriptome/10_Differential_expression.smk --use-conda --conda-frontend mamba -p -r -j 1

    """
    input:
        comp = expand('out/Metatranscriptome/DGE/{species}/Comparisons/Comp_{conditions}/Deg_res_Symbiodinium.tsv',
                    species=SPECIES,
                    conditions=CONDITIONS),
        norm_count = expand('out/Metatranscriptome/DGE/{species}/Normalized_counts.tsv',
                    species = SPECIES),
        results = expand('out/Metatranscriptome/DGE/{species}/Comparisons/Heatmap/All_comparisons_in_one_df_filtered_099.tsv',
                    species=SPECIES)


###############################################################################

rule correlation_heatmap:
    """
    Aim: This rule creates a correlation heatmap of raw counts
        WARNING: This is specific to samples used in our project
        Do not use in other case
    """
    message:
        """--- Correlation heatmap ---"""
    conda:
        Heatmap_raw
    log:
        log = 'out/Metatranscriptome/log/Correlation_raw_counts.log'
    input:
        Acropora = 'out/Metatranscriptome/Acropora/Acropora_Total_counts.counts',
        Pocillopora = 'out/Metatranscriptome/Pocillopora/Pocillopora_Total_counts.counts',
        design_acro = 'out/Metatranscriptome/Acropora/Design_Acropora2.txt',
        design_pocillo = 'out/Metatranscriptome/Pocillopora/Design_Pocillopora.txt'
    output:
        session = 'out/Metatranscriptome/DGE/raw_data_plot.RData',
        CorHeatmapPNG = 'out/Metatranscriptome/DGE/Correlation_heatmap_raw_data.png',
        CorHeatmapPDF = 'out/Metatranscriptome/DGE/Correlation_heatmap_raw_data.pdf'
    script:
        "scripts/10_0_Heatmap_correlation.R"


rule correction:
    """
    Aim: This rule creates the output necessary to NOISeq packages with a batch correction determinate after PCA visualisation
        here a colony effect is also considered as a "batch" effect.
    """
    message:
        """--- NOISeq import count data and batch correction for {wildcards.species} ---"""
    conda:
        R
    log:
        log = 'out/Metatranscriptome/log/{species}/Correction.log'
    input:
        Count = 'out/Metatranscriptome/{species}/{species}_Total_counts.counts',
        Design = 'out/Metatranscriptome/{species}/Design_{species}.txt',
        Coverage = 'out/Metatranscriptome/{species}/cov_{species}.txt',
        Biotype = 'out/Metatranscriptome/{species}/Biotype_{species}'
    params:
        PcaRunEffect = 'out/Metatranscriptome/DGE/{species}/PCA_Run_Effect.png',
        PcaColRunEffect = 'out/Metatranscriptome/DGE/{species}/PCA_Col_Run_Effect.png'
    output:
        session = 'out/Metatranscriptome/DGE/{species}/Correction.RData',
        CorHeatmap = 'out/Metatranscriptome/DGE/{species}/Correlation_heatmap.png',
        PcaColEffect = 'out/Metatranscriptome/DGE/{species}/PCA_Colonie_Effect.png',
        PcaTimeEffect = 'out/Metatranscriptome/DGE/{species}/PCA_Time_Effect.png',
        PcaTreatmentEffect = 'out/Metatranscriptome/DGE/{species}/PCA_Transplantation_Effect.png',
        PcaCol = 'out/Metatranscriptome/DGE/{species}/PCA_Colonie_batch_correted.png',
        PcaSite = 'out/Metatranscriptome/DGE/{species}/PCA_Site_batch_correted.png',
        PcaTime = 'out/Metatranscriptome/DGE/{species}/PCA_Time_batch_correted.png',
        PcaSiteTime = 'out/Metatranscriptome/DGE/{species}/PCA_Site_Time_batch_correted.png',
        PcaComplete = 'out/Metatranscriptome/DGE/{species}/PCA_Complete_batch_correted.png',
        Norm = 'out/Metatranscriptome/DGE/{species}/Normalized_counts.tsv'
    script:
        "scripts/10_1_Rscript_DGE_correction.R"


rule comparisons:
    """
    Aim: Comparisons of conditions provided in config file, using NOISeq R package
    """
    message:
        """--- NOISeq comparisons for {wildcards.species} {wildcards.conditions} ---"""
    conda:
        R
    priority:
        100
    log:
        log = 'out/Metatranscriptome/log/{species}/Comparisons_{conditions}.log'
    input:
        session = 'out/Metatranscriptome/DGE/{species}/Correction.RData'
    params:
        threshold = 0.99,  # 0.99 for 1% of errors or 0.95 for 5% of errors on adjusted p-values
    output:
        session2 = 'out/Metatranscriptome/DGE/{species}/Comparisons/Comp_{conditions}/Comparison.RData',
        AllRes = 'out/Metatranscriptome/DGE/{species}/Comparisons/Comp_{conditions}/All_results.tsv',
        Norm = 'out/Metatranscriptome/DGE/{species}/Comparisons/Comp_{conditions}/Normalized_counts.tsv'
    script:
        "scripts/10_2_Rscript_DGE_comparisons.R"


rule annotations:
    """
    Aim: Annotations of results with GO and blastn
    """
    message:
        """--- Annotations of comparisons for {wildcards.species} {wildcards.conditions} ---"""
    conda:
        R
    log:
        log = 'out/Metatranscriptome/log/{species}/Annotations_{conditions}.log'
    input:
        session2 = 'out/Metatranscriptome/DGE/{species}/Comparisons/Comp_{conditions}/Comparison.RData',
        GOid = 'Databases/Results_from_anotations/{species}_GO', # results from GO annotation on our contigs
        GeneList = 'Databases/Results_from_anotations/{species}_join_table.txt' # left_join of all our annotations results on our contigs, see: https://gitlab.com/RipollJ/Database_SQL.git to redo it
    output:
        TopGO = 'out/Metatranscriptome/DGE/{species}/Comparisons/Comp_{conditions}/TopGO.tsv',
        TopGene = 'out/Metatranscriptome/DGE/{species}/Comparisons/Comp_{conditions}/TopGene.tsv',
        session = 'out/Metatranscriptome/DGE/{species}/Comparisons/Comp_{conditions}/Annotation.RData'
    script:
        "scripts/10_3_Rscript_DGE_annotations.R"


rule explore_res:
    """
    Aim: Explore results
    """
    message:
        """--- Explore results by biotype for comparisons of {wildcards.species} {wildcards.conditions} ---"""
    conda:
        R
    log:
        log = 'out/Metatranscriptome/log/{species}/Explore_{conditions}.log'
    input:
        session2 = 'out/Metatranscriptome/DGE/{species}/Comparisons/Comp_{conditions}/Annotation.RData'
    output:
        session = 'out/Metatranscriptome/DGE/{species}/Comparisons/Comp_{conditions}/Explore.RData',
        symb = 'out/Metatranscriptome/DGE/{species}/Comparisons/Comp_{conditions}/Deg_res_Symbiodinium.tsv'
    script:
        "scripts/10_4_Rscript_DGE_explore_results.R"


rule categorization:
    """
    Aim: Categorizes genes
    """
    message:
        """--- Categorizes genes of {wildcards.species} ---"""
    conda:
        Pandas
    log:
        log = 'out/Metatranscriptome/log/{species}/Categorization.log'
    input:
        GeneList = "Databases/Results_from_annotations/{species}_join_table.txt",
        Biotype = 'out/Metatranscriptome/{species}/Biotype_{species}',
        check = 'out/Metatranscriptome/DGE/{species}/Correction.RData'
    output:
        genes = 'out/Metatranscriptome/{species}/{species}_join_table_filt.txt'
    script:
        "scripts/10_5_Genes_categorization.py"


rule combine_results:
    """
    Aim: Combine all results
    """
    message:
        """--- combine results of all comparisons for {wildcards.species} ---"""
    conda:
        Pandas
    log:
        log = 'out/Metatranscriptome/log/{species}/combine_results.log'
    input:
        genes = 'out/Metatranscriptome/{species}/{species}_join_table_filt.txt'
    params:
        files = 'out/Metatranscriptome/DGE/{species}/Comparisons/*/All_results.tsv',
        threshold = 0.99 # change filt value according to threshold
    output:
        all_in_one = 'out/Metatranscriptome/DGE/{species}/Comparisons/All_comparisons_in_one_df.tsv',
        all_in_one_filtered = 'out/Metatranscriptome/DGE/{species}/Comparisons/All_comparisons_in_one_df_filtered_099.tsv'
    script:
        "scripts/10_6_Combine_results.py"
