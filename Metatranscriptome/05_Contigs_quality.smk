import os
import sys

configfile: "Config.yml"

ENVIRONMENT1 = "../envs/Assembly.yml"
ENVIRONMENT2 = "../envs/Align.yml"
SPECIES = config["species"]
THREADS = config['threads']['Normal']

###############################################################################

rule all:
    """

    #############################
    ### Check contigs quality ###
    #############################

    Author: Julie Ripoll
    Contact: julie.ripoll87@gmail.com
    Created: 2018
    License: CC-BY-NC 4.0

    Modified: 2018-10-02

    Aim: Check contigs quality by performing some statistics (duplicates, clusters, isogroups and properly reads alignment)

    This snakefile need the use of snakemake version > 3.5 for solving conda environment with the --use-conda option. 
    It needs also conda > 4.5

    Execute as this:
    >snakemake -s Metatranscriptome/05_Contigs_quality.smk --use-conda --conda-frontend mamba -p -r -j 1

    """
    input:
        iso = expand("out/Assembling/{species}/megahit_out/final_contigs_iso.clstr",
                     species = SPECIES)

###############################################################################

rule Contigs_Metrics:
    """
    Aim: check contig quality with BBMap
    """
    message:
        """--- CALCULATE-METRICS-ON-ASSEMBLED-CONTIGS FOR {wildcards.species} ---"""
    conda:
        ENVIRONMENT1
    log:
        "out/Assembling/{species}/Megahit.log"
    params:
        threads = THREADS
    input:
        inp = "out/Assembling/{species}/megahit_out/final.contigs.fa"
    output:
        metrics = "out/Assembling/{species}/megahit_out/Others_Metrics.table",
        nonredundant = "out/Assembling/{species}/megahit_out/final_contigs_nr.fasta",
        redundant = "out/Assembling/{species}/megahit_out/duplicate_contigs.fasta",
        stats = "out/Assembling/{species}/megahit_out/final_contig.stats",
        cat = "out/Assembling/{species}/megahit_out/Summary_fasta.table"
    shell:
        'grep -v "^>" {input.inp} |awk ''BEGIN{{c=0;}} {{c = c + length($0)}} END {{printf("length = %i\\n", c);}}'' = Metrics ' # nucletotide total length
        "&& "
        "awk 'END {{print NR}}' {input.inp} = Metrics2 " # total contigs
        "&& "
        "echo '$Metrics $Metrics2' > {output.metrics} "
        "&& "
        "dedupe.sh in={input.inp} out={output.nonredundant} outd={output.redundant} threads={params.threads} csf=cluster_info "
        "&& "
        "stats.sh in={input.inp} > {output.stats} " # read metrics
        "&& "
        'cat {input.inp} | awk ''$0 ~ ">" {{ print c; c=0; printf substr($0,2,100) "\\t"; }} $0 !~ ">" {{c+=length($0);}} END {{ print c; }}'' > {output.cat}'


rule Cluster:
    """
    Aim: Checking the grouping of contigs assembled with CD-HIT-EST
    """
    message:
        """--- CLUSTERS-INFO FOR {wildcards.species} ---"""
    conda:
        ENVIRONMENT1
    log:
        "out/Assembling/{species}/cd-hitAC.log"
    input:
        stats = "out/Assembling/{species}/megahit_out/final_contig.stats",
        inp = "out/Assembling/{species}/megahit_out/final.contigs.fa"
    output:
        clust = "out/Assembling/{species}/megahit_out/final_contigs_clust.fasta"
    shell:
        "cd-hit-est -i {input.inp} -o {output.clust} -c 0.99 -G 0 -aL 0.3 -aS 0.3 -T 12 > {log}"


rule Isogroup:
    """
    Aim: Checking isogroups for assembled contigs with the zOon github perl script
    """
    message:
        """--- ISOGROUPS-INFORMATIONS FOR {wildcards.species} ---"""
    conda:
        ENVIRONMENT1
    log:
        "out/Assembling/{species}/perl_iso.log"
    input:
        checklog = "out/Assembling/{species}/cd-hitAC.log", # log because clusters cannot be found in certain cases
        inp = "out/Assembling/{species}/megahit_out/final.contigs.fa"
    output:
        iso = "out/Assembling/{species}/megahit_out/final_contigs_iso.clstr"
    shell:
        "chmod +x scripts/isogroup_namer.pl "
        "&& "
        "perl scripts/isogroup_namer.pl {input.inp} {output.iso} > {log}"
