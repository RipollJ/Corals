import os
import sys
from snakemake.utils import R

configfile: "Config.yml"

ENVIRONMENT = "../envs/Count.yml"

SPECIES = config["species"]
SAMPLE, = glob_wildcards('out/Assembling/'+SPECIES+'/{sample}_1.fastq')

###############################################################################

rule all:
    """

    ###############
    # Count Table #
    ###############

    Author: Julie Ripoll
    Contact: julie.ripoll87@gmail.com
    Created: 2018
    License: CC-BY-NC 4.0

    Modified: 2018

    Aim: Count reads with samtools

    Note: it is possible to filter contigs by species assignations.
    But here, we choose to keep all contigs for count table and DGE analyses due to low undesirable contigs number.

    This snakefile need the use of snakemake version > 3.5 for solving conda environment with the --use-conda option.
    It needs also conda > 4.5

    Execute as this:
    >snakemake -s Metatranscriptome/09_Count.smk --use-conda --conda-frontend mamba -p -r -j 1

    """
    input:
         bamsort = expand("out/Assembling/{species}/bam/{sample}.bam.sorted",
                         species = SPECIES,
                         sample = SAMPLE),
         protlist = expand("out/Assembling/{species}/DGE/inp/{species}/ProtListForR_{species}",
                          species = SPECIES)

###############################################################################

rule Sort_BAM_FILES_FOR_COUNT:
    """
    Aim: Convert each sample sam file in bam file, sort and index output bam files
    """
    message:
        """--- Convert sample sam file in bam file for {wildcards.species} - {wildcards.sample} ---"""
    conda:
        ENVIRONMENT
    log:
        "out/Assembling/{species}/bam/log/Bam_{sample}.log"
    input:
        inp = "out/Assembling/{species}/megahit_out/final.contigs.fa",
        alignsam = "out/Assembling/{species}/{sample}.sam"
   params:
        threads = config['threads']['Normal'],
        Spebam = "out/Assembling/{species}/bam/{sample}.bam",
        tempf = temp("out/Assembling/{species}/bam/{sample}.tmp")
    output:
        bamsort =  "out/Assembling/{species}/bam/{sample}.bam.sorted"
    shell:
        "samtools faidx {input.inp} "
        "&& "
        "samtools view -b -T {input.inp} {input.alignsam} >{params.Spebam} "
        "&& "
        "samtools sort -O bam -@ {params.threads} -o {output.bamsort} -T {params.tempf} {params.Spebam} "
        "&& "
        "samtools index {output.bamsort} "
        "&& "
        "1>{log}"


rule Count_idxstats:
    """
    Aim: calculate BAM index stats using idxstats
    The output is TAB-delimited with each line consisting of reference sequence name, sequence length, mapped read-segments and unmapped read-segments.
    """
    message:
        """--- Calculate BAM index stats by sample using idxstats for {wildcards.species} ---"""
    conda:
        ENVIRONMENT
    log:
        "out/Assembling/{species}/Count/log/Count_{sample}.log"
    input:
        bamsort = "out/Assembling/{species}/bam/{sample}.bam.sorted"
    output:
        idx = "out/Assembling/{species}/Count/{sample}.idxstats"
    shell:
        "samtools idxstats {input.bamsort} > {output.idx} "
        "&& "
        "2>{log}"


rule Filter_idxstats:
    """
    Aim: Keep mapped read-segments counts from idxstats
    """
    message:
        """--- Keep count from idxstats for {wildcards.species} ---"""
    conda:
        ENVIRONMENT
    log:
        "out/Assembling/{species}/Count/log/Count_{sample}.log"
    input:
        idx = "out/Assembling/{species}/Count/{sample}.idxstats"
    output:
        counti = "out/Assembling/{species}/Count/{sample}.counts"
    shell:
        "cut -f1,3 {input.idx} > {output.counti} "
        "&& "
        "echo -e 'Contig\\t$filename' | cat - {output.counti} > {output.counti}.tmp; mv {output.counti}.tmp {output.counti} "
        "&& "
        "sed -i 's/\ /_/g' {output.counti} "


rule Format_idxstats:
    """
    Aim: concatenate all samples counts in one output by species
    """
    message:
        """--- Concatenate all samples counts for {wildcards.species} ---"""
    conda:
        ENVIRONMENT
    input:
        counti = "out/Assembling/{species}/Count/{sample}.counts"
    params:
        listC  = "out/Assembling/{species}/Count/listCount",
        tempf = temp("out/Assembling/{species}/Count/Total_counts.tmp")
    output:
        counts = "out/Assembling/{species}/Count/{species}_Total_counts.counts"
    shell:
        "ls {input.counti} > {params.listC} "
        "&& "
        "cat $(paste {params.listC}) > {params.tempf} "
        "&& "
        "awk '{{for (i=3;i<=NF;i+=2) $i=""}} 1' {params.tempf} > {output.counts} "
        "&& "
        "sed -i 's/\  /\t/g; s/\ /\t/g' {output.counts}"


rule Prepare_file_for_DGE:
    """
    Aim: prepare files necessary for DGE analyses
    """
    message: 
        """--- Prepare files for DGE analyses for {wildcards.species} ---"""
    conda:
        ENVIRONMENT
    log:
        "out/Assembling/{species}/DGE/log/Prepare.log"
    input:
        check = "out/Assembling/{species}/Count/{species}_Total_counts.counts",
        blast = "out/Assembling/{species}/Annotations/Nuc_Blast_Contigs_OneSeq",
        diamond_blast_nr = "out/Assembling/{species}/Annotations/Diamond_blastx_nr.txt"
    output:
        genlist = "out/Assembling/{species}/DGE/inp/{species}/GeneListForR_{species}",
        biotype = "out/Assembling/{species}/DGE/inp/{species}/Biotype_{species}",
        protlist = "out/Assembling/{species}/DGE/inp/{species}/ProtListForR_{species}"
    shell:
        "awk 'BEGIN{{FS=OFS=""\t""}} {{print $1,$3,$6,$7}}' {input.blast} > tmp.tmp "
        "sed '1i Contigs\\tTaxid\\tSpecies\\tGene_Name ; s/\\ /_/g' tmp.tmp > {output.genlist} "
        "&& "
        "sed 's/\\t/,/g' {input.blast} | awk 'BEGIN{{FS=""\,""}} {{print $1 ""_\t"" $NF}}' | sed '1i Contigs\\tBiotype' | uniq > {output.biotype} "
        "&& "
        "sed '1i qseqid\\tqlen\\tqstart\\tqend\\tevalue\\tscore\\tppos\\tgaps\\tstaxids\\tqtitle\\tstitle' {input.diamond_blast_nr} > out/Assembling/{species}/Annotations/protNR.tmp "
        "&& "
        "awk 'BEGIN{{FS=OFS=""\t""}} {{print $9,$10,$11}}' out/Assembling/{species}/Annotations/protNR.tmp | sed 's/\\ /_/g' | uniq > {output.protlist} "
        "&& "
        "1>{log}"
