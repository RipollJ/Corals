import os
import sys

configfile: "Config.yml"

ENVIRONMENT = "../envs/Assembly.yml"
SPECIES = config["species"]
THREADS = config['threads']['Normal']

###############################################################################

rule all:
    """

    #############################################
    ### De Novo Assembly of metatranscriptome ###
    #############################################

    Author: Julie Ripoll
    Contact: julie.ripoll87@gmail.com
    Created: 2018
    License: CC-BY-NC 4.0

    Modified: 2018-10-02

    Aim: De novo assembling of filtered RNA reads from metatranscriptome
        (contigs of corals and associated micro-organisms)

    This snakefile need the use of snakemake version > 3.5 for solving conda environment with the --use-conda option. 
    It needs also conda > 4.5

    Execute as this:
    >snakemake -s Metatranscriptome/04_DeNovoAssembly.smk --use-conda --conda-frontend mamba -p -r -j 1

    """
    input:
        out = expand('out/Assembling/{species}/megahit_out',
                     species = SPECIES)

###############################################################################

rule Prepare_Assembly:
    """
    Aim: Prepare assembly list files
    """
    message:
        """--- PREPARE LIST OF READS 1 AND 2 FOR {wildcards.species} ---"""
    conda:
        ENVIRONMENT 
    params:
        threads = THREADS
    input:
        ReadD = 'out/Sortmerna/{species}/Other/'
    output:
        list1 = 'out/Assembling/{species}/list_read_1',
        list2 = 'out/Assembling/{species}/list_read_2'
    shell:
        "ls {input.ReadD}/*_1.fastq > {output.list1} "
        "&& "
        "sed -i ':a;N;$!ba;s/\\n/,/g' {output.list1} "
        "&& "
        "ls {input.ReadD}/*_2.fastq > {output.list2} "
        "&& "
        "sed -i ':a;N;$!ba;s/\\n/,/g' {output.list2} "


rule Assembly:
    """
    Aim: De Novo assembling with Megahit.
    """
    message:
        """--- ASSEMBLY OF READS FOR {wildcards.species} ---"""
    conda:
        ENVIRONMENT
    log:
        'out/Assembling/{species}/error.log'
    params:
        threads = THREADS
    input:
        list1 = 'out/Assembling/{species}/list_read_1',
        list2 = 'out/Assembling/{species}/list_read_2'
    output:
        out = directory("out/Assembling/{species}/megahit_out")
    shell:
        "megahit "
        "-1 $(paste {input.list1}) "
        "-2 $(paste {input.list2}) "
        "-o {output.out} "
        "-m 0.7 "
        "-t {params.threads} "
        "2>{log}"
