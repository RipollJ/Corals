import os
import sys

configfile: "Config.yml"

SPECIES = config["species"]
THREADS = config['threads']['Normal']
ENVIRONMENT = "../envs/Annot.yml"

###############################################################################

rule all:
    """

    #########################
    # Annotation of contigs #
    #########################

    Author: Julie Ripoll
    Contact: julie.ripoll87@gmail.com
    Created: 2018
    License: License CC-BY-NC 4.0

    Modified: 2018-10-02

    Aim: Annotation of contigs for filtering of non corals/Symbiodinium contigs

    This snakefile need the use of snakemake version > 3.5 for solving conda environment with the --use-conda option. 
    It needs also conda > 4.5

    Execute as this:
    >snakemake -s Metatranscriptome/06_Contigs_annotation.smk --use-conda --conda-frontend mamba -p -r -j 1

    """
    input:
          out_sprot2 = expand("out/Assembling/{species}/Annotations/Diamond_transdecoder_sprot.outfmt6",
		      species = SPECIES)

###############################################################################

rule Megablast_nt:
    """
    Aim: Megablast with nt database.
        Pre-install nt database with taxid following NCBI.sh script.
    """
    message:
        """--- Blast on nucleotide databases nt from NCBI, can take a lot of time for {wildcards.species} ---"""
    conda:
        ENVIRONMENT
    log:
        "out/Assembling/{species}/Annotations/Blast_nt.log"
    params:
        threads = THREADS,
        format = '6 qseqid cigar qcov qstrand evalue sgi staxids sscinames scomnames stitle'
    input:
        Megahit = "out/Assembling/{species}/megahit_out/final.contigs.fa"
    output:
        blast = "out/Assembling/{species}/Annotations/Nuc_Blast_Contigs_OneSeq"
    shell:
        "makeblastdb -in Databases/nt -parse_seqids -dbtype nucl "
        "&& "
        "blastn "
        "-num_threads {params.threads} "
        "-db Databases/nt "
        "-outfmt {params.format} "
        "-query {input.Megahit} "
        "-out {output.blast} "
        "-perc_identity 80 "
        "-max_target_seqs 1 > {log}"


rule Transdecoder_LongORF:
    """
    Aim: Calculate the longest coding region
    """
    message:
        """--- CALCULATE THE LONGEST CODING REGION FOR {wildcards.species} ---"""
    conda:
        ENVIRONMENT
    input:
        clust = "out/Assembling/{species}/megahit_out/final_contigs_clust.fasta"
    output:
        outlog = "out/Assembling/{species}/Annotations/LongOrfs.log",
        transdir = directory("out/Assembling/{species}/Annotations/final_contigs_clust.fasta.transdecoder_dir")
    shell:
        "TransDecoder.LongOrfs "
        "-t {input.clust} " # extract the long ORF
        "> {log}"


rule Transdecoder_Predict:
    """
    Aim: run TransDecoder.Predict for your final coding region predictions
    """
    message:
        """--- TRANSDECODER.PREDICT FOR {wildcards.species} ---"""
    conda:
        ENVIRONMENT
    params:
        threads = THREADS
    input:
        outlog = "out/Assembling/{species}/Annotations/LongOrfs.log",
        clust = "out/Assembling/{species}/megahit_out/final_contigs_clust.fasta"
    output:
        outpfam = "out/Assembling/{species}/Annotations/final_contigs_clust.fasta.transdecoder_dir/pfam.domtblout",
        outblastp = "out/Assembling/{species}/Annotations/final_contigs_clust.fasta.transdecoder_dir/blastp.outfmt6",
        outlog = "out/Assembling/{species}/Annotations/final_contigs_clust.fasta.transdecoder_dir/predict.log"
    shell:
        "TransDecoder.Predict "
        "-t {input.clust} "
        "--cpu {params.threads} "
        "--retain_pfam_hits {output.outpfam} "
        "--retain_blastp_hits {output.outblastp} "
        "--single_best_orf > {output.outlog}" # predict likely coding regions


rule Diamond_nr:
    """
    Aim: Megablast with several database using Diamond. Pre-install databases with taxid file and index it.
    """
    message:
        """--- Diamond blastx on RefSeq non-redundant NCBI database for {wildcards.species} ---"""
    conda:
        ENVIRONMENT
    log:
        "out/Assembling/{species}/Annotations/Diamond_nr.log"
    params:
        threads = THREADS,
        format = '6 qseqid qlen qstart qend evalue score ppos gaps staxids qtitle stitle',
        numb = '3' #number of output sequence results by contigs
    input:
        outlog = "out/Assembling/{species}/Annotations/final_contigs_clust.fasta.transdecoder_dir/predict.log",
        clust = "out/Assembling/{species}/megahit_out/final_contigs_clust.fasta"
    output:
        diamond_blast_nr = 'out/Assembling/{species}/Annotations/Diamond_blastx_nr.txt'
    shell:
        "diamond makedb --in Databases/nr.fasta -d nr " # index db
        "&& "
        "diamond blastx "
        "-d nr "
        "-q {input.clust} "
        "-o {output.diamond_blast_nr} "
        "--outfmt {params.format} "
        "--taxonmap Databases/prot.accession2taxid.gz "
        "-k {params.numb} "
        "--more-sensitive "
        "-p {params.threads} > {log}"


rule Diamond_sprot:
    """
    Aim: Megablast with several database using Diamond. Pre-install databases with taxid file and index it.
    """
    message:
        """--- Diamond blastx on swiss-prot NCBI database for {wildcards.species} ---"""
    conda:
        ENVIRONMENT
    log:
        "out/Assembling/{species}/Annotations/Diamond_sprot.log"
    params:
        threads = THREADS,
        format = '100', # default output format of diamond
        numb = '1' #number of output sequence results by contigs
    input:
        diamond_blast_nr = 'out/Assembling/{species}/Annotations/Diamond_blastx_nr.txt',
        clust = "out/Assembling/{species}/megahit_out/final_contigs_clust.fasta"
    output:
        diamond_blast_sprot = 'out/Assembling/{species}/Annotations/Diamond_blastx_sprot.daa'
    shell:
        "diamond makedb --in Databases/uniprot_sprot.fasta -d sprot " # index db
        "&& "
        "diamond blastx "
        "-d sprot "
        "-q {input.clust} "
        "-o {output.diamond_blast_sprot} "
        "--outfmt {params.format} "
        "-k {params.numb} "
        "--more-sensitive "
        "-p {params.threads} > {log}"


rule Diamond_sprot_transdecoder:
    """
    Aim: Perform diamond blast on transdecoder output
    """
    message:
        """--- Diamond blast on transdecoder longest ORFs peptides with swiss-prot NCBI database for {wildcards.species} ---"""
    conda:
        ENVIRONMENT
    log:
        "out/Assembling/{species}/Annotations/Diamond_sprot_transdecoder.log"
    params:
        threads = THREADS,
        format = '100', # default output format of diamond
        numb = '1' #number of output sequence results by contigs
    input:
        transdir = directory("out/Assembling/{species}/Annotations/final_contigs_clust.fasta.transdecoder_dir"),
        logi = "out/Assembling/{species}/Annotations/Diamond_sprot.log"
    output:
        diamond_blast_sprot2 = "out/Assembling/{species}/Annotations/Diamond_transdecoder_sprot.daa"
    shell:
        "diamond blastx "
        "-d sprot "
        "-q {input.transdir}/longest_orfs.pep "
        "-o {output.diamond_blast_sprot2} "
        "--outfmt {params.format} "
        "-k {params.numb} "
        "--more-sensitive "
        "-p {params.threads} > {log}"


rule Diamond_rfam:
    """
    Aim: Megablast with several database using Diamond. Pre-install databases with taxid file and index it.
    """
    message:
        """--- Diamond blastx on Rfam database for {wildcards.species} ---"""
    conda:
        ENVIRONMENT
    log:
        "out/Assembling/{species}/Annotations/Diamond_rfam.log"
    params:
        threads = THREADS,
        format = '100',
        numb = '3' #number of output sequence results by contigs
    input:
        logit = "out/Assembling/{species}/Annotations/Diamond_sprot_transdecoder.log",
        clust = "out/Assembling/{species}/megahit_out/final_contigs_clust.fasta"
    output:
        diamond_blast_rfam = 'out/Assembling/{species}/Annotations/Diamond_blastx_rfam.daa'
    shell:
        "zcat Databases/Rfam/*.fa.gz | diamond makedb -d Rfam_db " # index db
        "&& "
        "diamond blastx "
        "-d Rfam_db "
        "-q {input.clust} "
        "-o {output.diamond_blast_rfam} "
        "--outfmt {params.format} "
        "-k {params.numb} "
        "--more-sensitive "
        "-p {params.threads} > {log}"


rule Hmmscan:
    """
    Aim: Hmmscan from Transdecoder. Pre-install database with taxid file and index it.
    """
    message:
        """--- HMMSCAN for {wildcards.species} ---"""
    conda:
        ENVIRONMENT
    log:
        "out/Assembling/{species}/Assembling/HMMScan.log"
    params:
        threads = THREADS
    input:
        transdir = directory("out/Assembling/{species}/Annotations/final_contigs_clust.fasta.transdecoder_dir"),
        logii = "out/Assembling/{species}/Annotations/Diamond_rfam.log"
    output:
        blast_hmm = 'out/Assembling/{species}/Annotations/pfam.domtblout '
    shell:
        "hmmscan "
        "--cpu {params.threads} "
        "--domtblout {output.blast_hmm} "
        "Databases/Pfam-A.hmm "
        "{input.transdir}/longest_orfs.pep > {log}"


rule RPSBlast:
    """
    Aim: rpsblast. Pre-install CDD databases with taxid file and index it.
    """
    message:
        """--- RPSBLAST CDD and KOG databases for {wildcards.species} ---"""
    conda:
        ENVIRONMENT
    log:
        logC = "out/Assembling/{species}/Assembling/cdd.log",
        logK = "out/Assembling/{species}/Assembling/kog.log"
    params:
        threads = THREADS,
        eval = "0.0001",
        format = '8', # tabular; 9 for tabular with comments line
        numb = '1' #number of output sequence results by contigs
    input:
        transdir = directory("out/Assembling/{species}/Annotations/final_contigs_clust.fasta.transdecoder_dir"),
        logiii = "out/Assembling/{species}/Assembling/HMMScan.log"
    output:
        cdd = "out/Assembling/{species}/Annotations/Rpsblast_CDD.outfmt6",
        kog = "out/Assembling/{species}/Annotations/Rpsblast_KOG.outfmt6"
    shell:
        "makeprofiledb -title CDD.v.3.12 -in Databases/cdd/Cdd.pn -out CDD -threshold 9.82 -scale 100.0 -dbtype rps -index true "
        "&& "
        "rpsblast -a {params.threads} -e {params.eval} -p -i {input.transdir}/longest_orfs.pep -d Databases/cdd/CDD -o {output.cdd} -P {params.numb} -m {params.format} > {log.logC} "
        "&& "
        "makeprofiledb -title CDD.v.3.12 -in Databases/cdd/Kog.pn -out KOG -threshold 9.82 -scale 100.0 -dbtype rps -index true "
        "&& "
        "rpsblast {params.threads} -e {params.eval} -p -i {input.transdir}/longest_orfs.pep -d Databases/cdd/KOG -o {output.kog} -P {params.numb} -m {params.format} > {log.logK}"


rule Convert:
    """
    Aim: Convert daa output in outfmt6
    """
    message:
        """--- Convert diamond output for {wildcards.species} ---"""
    conda:
        ENVIRONMENT
    input:
        logK = "out/Assembling/{species}/Assembling/kog.log",
        daa_sprot = "out/Assembling/{species}/Annotations/Diamond_blastx_sprot.daa",
        daa_rfam = "out/Assembling/{species}/Annotations/Diamond_blastx_rfam.daa",
        daa_sprot2 = "out/Assembling/{species}/Annotations/Diamond_transdecoder_sprot.daa"
    output:
        out_sprot = "out/Assembling/{species}/Annotations/Diamond_blastx_sprot.outfmt6",
        out_rfam = "out/Assembling/{species}/Annotations/Diamond_blastx_rfam.outfmt6",
        out_sprot2 = "out/Assembling/{species}/Annotations/Diamond_transdecoder_sprot.outfmt6"
    shell:
        "diamond view --daa {input.daa_sprot} -o {out_sprot} --outfmt 6 "
        "&& "
        "diamond view --daa {input.daa_rfam} -o {out_rfam} --outfmt 6 "
        "&& "
        "diamond view --daa {input.daa_sprot2} -o {out_sprot2} --outfmt 6"
