import os
import sys

configfile: "Config.yml"

ENVIRONMENT = "../envs/blast+.yml"
SPECIES = config["species"]
THREADS = config['threads']['Normal']

# format for output blast
FORMAT = '6 qseqid cigar qcov qstrand evalue sgi staxids sscinames scomnames stitle'

###############################################################################

rule all:
    """

    ###################################################################
    # Filter contigs to sparate Cnidarian and Symbiodiniaceae contigs #
    ###################################################################

    Author: Julie Ripoll
    Contact: julie.ripoll87@gmail.com
    Created: 2018
    License: CC-BY-NC 4.0

    Modified: 2018-10-02

    Aim: Filtering of contigs on species identifications.

    This part was used for the creation of a SQL database used later to identify counted contigs

    This snakefile need the use of snakemake version > 3.5 for solving conda environment with the --use-conda option. 
    It needs also conda > 4.5

    Execute as this:
    >snakemake -s Metatranscriptome/07_Filter_contigs.smk --use-conda --conda-frontend mamba -p -r -j 1

    """
    input:
        symb = expand("out/Assembling/{species}/Annotations/Symb_prot.fasta",
                     species = SPECIES),
        cnidarian = expand("out/Assembling/{species}/Annotations/Cnidaria_prot.fasta",
                          species = SPECIES),
        noblastcontigs = expand("out/Assembling/{species}/Annotations/NoBlast_contigs.fasta",
                               species = SPECIES)

###############################################################################

rule Filter_contigs:
    """
    Aim: Filter contig on blast output
    """
    message:
        """--- Filter assembled fasta file by blast output ---"""
    conda:
        ENVIRONMENT
    log:
        "out/Assembling/{species}/Splitblast.log"
    params:
        threads = THREADS
    input:
        inp = "out/Assembling/{species}/megahit_out/final.contigs.fa",
        blast = "out/Assembling/{species}/Annotations/Nuc_Blast_Contigs_OneSeq"
    output:
        ids = "out/Assembling/{species}/Annotations/ids.txt",
        blastcontigs = "out/Assembling/{species}/Annotations/Blast_contigs.fasta",
        noblastcontigs = "out/Assembling/{species}/Annotations/NoBlast_contigs.fasta"
    shell:
        "awk '{print $1}' {input.blast} | uniq > {output.ids} "
        "&& "
        "filterbyname.sh in={input.inp} out={output.blastcontigs} names={output.ids} include=t " #  multi-line fasta, matching contigs
        "&& "
        "filterbyname.sh in={input.inp} out={output.noblastcontigs} names={output.ids} include=f " # multi-line fasta, no matching contigs
        "1>{log}"


rule Split_cnidarian:
    """
    Aim: Split blastx output by taxid 6073 for Cnidaria
    """
    message:
        """--- Filter assembled fasta file by Cnidaria gitaxid ---"""
    conda:
        ENVIRONMENT
    log:
        "out/Assembling/{species}/SplitCnidaria.log"
    params:
        threads = THREADS,
        formatp = FORMAT,
        taxID = "6073" # Cnidarian
    input:
        inp = "out/Assembling/{species}/megahit_out/final.contigs.fa"
    output:
        cnidarian = "out/Assembling/{species}/Annotations/Cnidaria_prot.fasta"
    shell:
        "makeblastdb -in Databases/nr.fasta -parse_seqids -dbtype prot "
        "&& "
        "tblastx -db Databases/nr.fasta -query {input.inp} -entrez_query {params.taxID} -remote -outfmt {params.formatp} -out {output.cnidarian} "
        "1> {log}"


rule Split_symb:
    """
    Aim: Split blastx output by taxid 252141 for Symbiodiniaceae
    """
    message:
        """--- Filter assembled fasta file by Symbiodiniaceae gitaxid ---"""
    conda:
        ENVIRONMENT
    log:
        "out/Assembling/{species}/SplitSymb.log"
    params:
        threads = THREADS,
        formatp = FORMAT,
        taxID = "252141" # Symbiodiniaceae
    input:
        inp = "out/Assembling/{species}/megahit_out/final.contigs.fa"
    output:
        symb = "out/Assembling/{species}/Annotations/Symb_prot.fasta"
    shell:
        "makeblastdb -in Databases/nr.fasta -parse_seqids -dbtype prot "
        "&& "
        "tblastx -db Databases/nr.fasta -query {input.inp} -entrez_query {params.taxID} -remote -outfmt {params.formatp} -out {output.symb} "
        "1>{log}"
