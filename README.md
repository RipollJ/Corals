# CORALS PROJECT

Contents:
- [Introduction](#introduction)
- [Pipeline description](#pipeline-description)
- [Licence and Contributing](#licence-and-contributing)
- [Citation](#citation)
- [Acknowledgments](#acknowledgments)
- [Getting started](#getting-started)

## Introduction

The Corals project takes place in the [ANR Carioca](http://www.agence-nationale-recherche.fr/Projet-ANR-15-CE02-0006). This ANR focuses on the comparison of coral species before and after transplantation between two sites, a control and a site mimicking the effects of ocean acidification, near CO<sub>2</sub> seeps in Papua New Guinea.

To analyze the RNA-seq data from this project, we develop a bioinformatics pipeline that can be used to re-analyse our RNA samples ([DOI]*#data-coming-soon*). It can be used on new RNA samples with some modification in the configuration [Config.yml](https://gitlab.com/RipollJ/Corals/blob/master/Config.yml) file and scripts.

WARNINGS:
- This project was developed with [Snakemake](https://snakemake.readthedocs.io/en/stable/tutorial/tutorial.html) and [Conda](https://docs.conda.io/en/latest/) environments.
- It requires a Conda version > 4.5 and a Snakemake version > 3.5 (due to the use of the option *--use-conda* and *conda activate* command)
- Download steps take time and disk space, min. 150 G of free space just for the databases.


## Pipeline description

Detailed documentation is available in the [wiki](https://gitlab.com/RipollJ/Corals/wikis/home) page.

This pipeline takes a configuration file and FASTQ files as input and produces a gene expression matrix and many visualizations.

Steps:
- [loading multiple databases and formatting them](https://gitlab.com/RipollJ/Corals/wikis/home/1-Installations-and-download)
- [samples quality control and trimming](https://gitlab.com/RipollJ/Corals/wikis/home/2-Samples-quality)
- 3 different analyses:
  - [corals and *Symbiodiniaceae* genetic identification](https://gitlab.com/RipollJ/Corals/wikis/home/3-Identifications)
  - [de novo metatranscriptome analysis](https://gitlab.com/RipollJ/Corals/wikis/home/4-Metatranscriptome-de-novo)
  - [microbiome presence-absence identification](https://gitlab.com/RipollJ/Corals/wikis/home/5-Microbiome)


Each of the 3 analyses can be used separately.

This pipeline is made up of several snakefiles, all numbered according to their order of execution. 

We don't provide a *Master snakefile* to perform all these steps at once, as we consider it important for the user to have a preview of the results before performing the following steps.

To this end, all the command lines given in the wiki contain the *'-n'* option called dry-run mode. This option gives you the different steps before execution, remove it to execute the snakefile.


## Licence and Contributing

All scripts are provided “as is” without warranty of any kind.

All scripts and snakefiles are licensed under the Creative Commons Attribution-NonCommercial 4.0 International CC-BY-NC 4.0 rigths [LICENSE](https://gitlab.com/RipollJ/Corals/blob/master/LICENSE.md).

Any suggestions are welcome to improve this project, which is maintained on a volunteer basis.


## Citation

Several packages and software are used in these scripts, please quote them if you use one of these pipelines, check the [References](home/6-References) page, or quote this Gitlab project and/or our paper:

    Ripoll J., Berteaux-Lecellier V., Rodolpho-Metalpa R., Lecellier G. (2018). The Corals project pipeline: a Snakemake reproducible pipeline for de-novo metatranscriptome and genetic identification analysis. Gitlab, https://gitlab.com/RipollJ/Corals.

    Ripoll J., et al (unpub). future article (DOI)


## Acknowledgments

This work was funded by the French National Research Agency (ANR; project [CARIOCA](https://anr.fr//Projet-ANR-15-CE02-0006) grant no. ANR15CE02-0006-01, 2015).


## Getting started

1. **Databases preparation**

    After the installation of Conda and Snakemake, this pipeline requires to execute these shell scripts [NCBI.sh](https://gitlab.com/RipollJ/Corals/blob/master/NCBI.sh) and [Databases.sh](https://gitlab.com/RipollJ/Corals/blob/master/Databases.sh). Please read these shell scripts before using them.

    The **NCBI.sh** script downloads several NCBI databases like the 'nt' or 'nr' databases (see the [wiki](https://gitlab.com/RipollJ/Corals/wikis/home) for details). This requires a lot of disk space.

    The **Databases.sh** script downloads the [Silva](https://www.arb-silva.de/) SSU and LSU databases and some specific databases (see the [wiki](https://gitlab.com/RipollJ/Corals/wikis/home) for details).

    Usage
    ```sh
    $ ./NCBI.sh

    $ ./Databases.sh
    ```

    After the Databases.sh execution, where is a manual step which requires the [ARB](http://www.arb-home.de/) software, to filter by mark the downloaded Silva databases and export them as fasta by category: Eukaryota, Archaea and Bacteria.

    ```sh
    # filter by 'mark' in ARB software, see arb notice
    $ arb Databases/Silva/SSURef_NR99_128_SILVA_07_09_16_opt.arb
    $ arb Databases/Silva/LSURef_NR99_128_SILVA_07_09_16_opt.arb
    ```

    Use these names for saving the filtered mark files:
    - "LSU_archaea.fasta"
    - "LSU_bacteria.fasta"
    - "LSU_eukaryota.fasta"
    - "SSU_archaea.fasta"
    - "SSU_bacteria.fasta"
    - "SSU_eukaryota.fasta"

    After this step, do these command lines:

    ```sh
    # activate the conda environment for mothur
    $ conda activate DBD

    # Seed file creation
    $ for file in Databases/Silva/*.fasta; do
        dirF=Databases/Silva
        filename=$(basename $file | cut -d '.' -f 1)
        grep ">" $file | cut -f 1,2 | grep "\s100" | cut -f 1 | cut -c 2- > $dirF/$filename'_unique.accnos';
    done

    $ mothur "#get.seqs(fasta=Databases/Silva/SSU_eukaryota.fasta, accnos=Databases/Silva/SSU_eukaryota_unique.accnos)"
    $ mothur "#get.seqs(fasta=Databases/Silva/SSU_bacteria.fasta, accnos=Databases/Silva/SSU_bacteria_unique.accnos)"
    $ mothur "#get.seqs(fasta=Databases/Silva/SSU_archaea.fasta, accnos=Databases/Silva/SSU_archaea_unique.accnos)"
    $ mothur "#get.seqs(fasta=Databases/Silva/LSU_archaea.fasta, accnos=Databases/Silva/LSU_archaea_unique.accnos)"
    $ mothur "#get.seqs(fasta=Databases/Silva/LSU_bacteria.fasta, accnos=Databases/Silva/LSU_bacteria_unique.accnos)"
    $ mothur "#get.seqs(fasta=Databases/Silva/LSU_eukaryota.fasta, accnos=Databases/Silva/LSU_eukaryota_unique.accnos)"

    # remove points and dashes from align fasta for use in Sortmerna
    $ for file in Databases/Silva/*.pick.fasta; do
        dirF=Databases/Silva/
        filename=$(basename $file | cut -d '.' -f 1)
        sed 's/[\.-]//g' $file > $dirF/$filename'_notalign.fasta';
    done
    ```

2. **Samples download and Configuration file**

    Coral RNA samples can be downloaded here: [DOI] (*#coming-soon*). This requires XX G of free space.

    Samples folder should be specified in the configuration file [Config.yml](https://gitlab.com/RipollJ/Corals/blob/master/Config.yml) after the **'dataset:'** keyword.

    Download annotation files :
    - Download these files: [DOI](https://drive.google.com/drive/folders/1bM9cNEa3DT2fIgupZ9oHKp2FcsUpYKU6?usp=sharing)
    - Add them to the "Databases/Results_from_annotations" folder.
    These files contain the results of our contig annotations.

    Adapt the number of threads defined in the Config.yml according to your computer or cluster. By default, all Snakemake rules are parametrized with the **'Normal'** threads number which uses 8 threads, cf. config['threads']['Normal'] at the beginning of each snakefile.


3. **Snakefile execution**

    Snakefiles are numbered according to their order of execution.

    See the [wiki](https://gitlab.com/RipollJ/Corals/wikis/home) pages for more details on rules parameters.

    All command lines given here are in dry-run mode. If no errors occur in dry-run mode, remove the *'-n'* from the command line to run the snakefile.


    3.1. **Samples quality:**
    - Quality checking
    - Filtering of transcriptomic data

        Usage
        ```sh
        $ snakemake -s Samples_quality/00_Preparing_files.smk --use-conda --conda-frontend conda -j 1 -n

        $ snakemake -s Samples_quality/01_Quality_filtering.smk --use-conda --conda-frontend conda -j 1 -n

        $ snakemake -s Samples_quality/02_Filtering_rRNA.smk --use-conda --conda-frontend conda -j 1 -n

        $ snakemake -s Samples_quality/03_Quality_Check.smk --use-conda --conda-frontend conda -j 1 -n
        ```

    3.2. **Identification pipeline:**
    - Extraction and identification of genetic markers
    - iTOL files for plotting phylogenetic trees

        Usage
        ```sh
        $ ./Genetic_diversity/04_Identification.sh

        $ ./Genetic_diversity/05_Tree.sh
        ```

    3.3. **De novo Metatranscriptome pipeline:**
    - De novo assembling of metatranscriptomic data and quality checking
    - Annotation of transcriptome with several databases
    - Creation of count table and quality checking
    - Differential expression analyses of transcripts

        Usage
        ```sh
        $ snakemake -s Metatranscriptome/04_DeNovoAssembly.smk --use-conda --conda-frontend conda -j 1 -n

        $ snakemake -s Metatranscriptome/05_Contigs_quality.smk --use-conda --conda-frontend conda -j 1 -n

        $ snakemake -s Metatranscriptome/06_Contigs_annotation.smk --use-conda --conda-frontend conda -j 1 -n

        $ snakemake -s Metatranscriptome/07_Filter_contigs.smk --use-conda --conda-frontend conda -j 1 -n

        $ snakemake -s Metatranscriptome/08_Alignment.smk --use-conda --conda-frontend conda -j 1 -n

        $ snakemake -s Metatranscriptome/09_Count.smk --use-conda --conda-frontend conda -j 1 -n

        $ snakemake -s Metatranscriptome/10_DGE.smk --use-conda --conda-frontend conda -j 1 -n

        $ snakemake -s Metatranscriptome/11_New_references.smk --use-conda --conda-frontend conda -j 1 -n

        $ snakemake -s Metatranscriptome/12_Plots.smk --use-conda --conda-frontend conda -j 1 -n
        ```

    3.4. **Microbiome pipeline:**
    - Extraction of LSU/SSU markers using a modified metabarcoding approach
    - Summaries and graphical outputs

        Usage
        ```sh
        $ snakemake -s Microbiome/04_Microbiota_otu_quant.smk --use-conda --conda-frontend conda -j 1 -n

        $ snakemake -s Microbiome/05_Microbiota.smk --use-conda --conda-frontend conda -j 1 -n

        $ snakemake -s Microbiome/06_Summary_tax.smk --use-conda --conda-frontend conda -j 1 -n

        $ snakemake -s Microbiome/07_Summary_tax_all_species.smk --use-conda --conda-frontend conda -j 1 -n
        ```


[<small>[top↑]</small>](#)
