#!/bin/bash

################################################
## NCBI Databases download for CORALS Project ##
################################################


###############
# Author: Julie Ripoll
# Contact: julie.ripoll87@gmail.com
# Created: 2018-10
# Last update: 2022-07-11
# License: License CC-BY-NC 4.0


###############
# Note: download time is dependent on your internet connection. Not executable as is due to md5sum check, follow each step.
# Copy all files of the github and follow the process


###############
# Manage folders
mkdir Databases/nt Databases/cdd Databases/Rfam


###############
# Nucleotide 'nt'

# DL databases from NCBI
wget -o Log/lognt -t 2 -nc --show-progress -P Databases/nt 'ftp://ftp.ncbi.nlm.nih.gov/blast/db/nt.*.tar.gz'
wget -o Log/logntmd -t 2 -nc --show-progress -P Databases/nt 'ftp://ftp.ncbi.nlm.nih.gov/blast/db/nt.*.tar.gz.md5'

# Check at md5sum file for all DL before continue
md5sum -c <<<"f8277b626b3db0015638699429eea2cc Databases/nt/nt.*.tar.gz.md5" #compare key from md5 files and yours

# dezip + decompress
for file in ./Databases/nt/*.tar.gz; do
    tar -xzvf $file;
done 

# DL taxo files
wget -o Log/loggitaxid -t 2 -nc --show-progress -P Databases 'ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/gi_taxid_*.*'
wget -o Log/loggitaxid2 -t 2 -nc --show-progress -P Databases 'ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/gi_taxid_nucl.dmp.gz'
wget -o Log/logtaxdump -t 2 -nc --show-progress -P Databases 'ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/taxdump.tar.gz'
wget -o Log/logtaxdb-t 2 -nc --show-progress -P Databases 'ftp://ftp.ncbi.nlm.nih.gov/blast/db/taxdb.tar.gz'

# dezip files
tar -xzvf Databases/taxdump.tar.gz
tar -xzvf Databases/taxdb.tar.gz
gunzip Databases/gi_taxid_*.*


###############
# Prot 'nr' fasta

# DL databases from NCBI
wget -o Log/lognr -t 2 -nc --show-progress -P Databases 'ftp://ftp.ncbi.nlm.nih.gov/blast/db/FASTA/nr.gz'
wget -o Log/lognr -t 2 -nc --show-progress -P Databases 'ftp://ftp.ncbi.nlm.nih.gov/blast/db/FASTA/nr.gz.md5'

# Check at md5sum file for all DL before continue
md5sum -c <<<"5c7601cb64efbafc8f694d8d21fda800 Databases/nr.gz" #compare key from DL md5_files (copy and replace the key of nr.gz.md5 you DL) and yours DL fasta

# DL taxo files
wget -o Log/lognrtaxid -t 2 -nc --show-progress -P Databases 'ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/accession2taxid/prot.accession2taxid.gz' #map with taxonomy, option diamond --taxonmap
wget -o Log/lognrtaxdmp -t 2 -nc --show-progress -P Databases 'ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/taxdmp.zip' # map classification, option diamond --taxonnodes, output format 102

# dezip files
tar -xf Databases/nr.gz
tar -xf Databases/prot.accession2taxid.gz
gunzip Databases/taxdmp.zip


###############
# Prot 'SwissProt' fasta

# DL databases from NCBI
wget -o Log/logsprot -t 2 -nc -P Databases 'ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/complete/uniprot_sprot.fasta.gz' # --show-progress

# DL taxo file
wget -o Log/logidmap -t 2 -nc -P Databases 'ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/idmapping/idmapping_selected.tab.gz' # --show-progress

# dezip files if md5sum ok
gunzip Databases/uniprot_sprot.fasta.gz
gunzip Databases/idmapping_selected.tab.gz


###############
# Conserved domains 'cdd'

# DL databases from NCBI
wget -o Log/logCDD -t 2 -nc -P Databases/cdd 'ftp://ftp.ncbi.nih.gov/pub/mmdb/cdd/cdd*' # --show-progress

# decompress files  if md5sum ok
tar -xzvf Databases/cdd/cdd.tar.gz


###############
# Pfam and Rfam databases

# DL databases from EBI /Trinotate
wget -o Log/logRfam -t 2 -nc -P Databases/Rfam 'ftp://ftp.ebi.ac.uk/pub/databases/Rfam/CURRENT/fasta_files' # only fasta sequences
wget -o Log/logPfam -t 2 -nc -P Databases 'https://data.broadinstitute.org/Trinity/Trinotate_v3_RESOURCES/Pfam-A.hmm.gz'

# dezip  if md5sum ok
gunzip Pfam-A.hmm.gz

# index in HMMScan
hmmpress Pfam-A.hmm


###############
## Filtering blast output files by taxonomy
## provided as example if you need to go further after your blast, enjoy
## decomment for use

## Create a tree file from taxdump
#taxtree.sh names.dmp nodes.dmp tree.taxtree.gz

## Ordination from gitaxid
#gitable.sh gi_taxid_nucl.dmp.gz gitable.int1d.gz

## Rename blastID in taxid number
#gi2taxid.sh in=Blast_contigs_names.fasta out=renamed.fasta gi=gitable.int1d.gz tree=tree.taxtree.gz 

## Filter by taxa
#filterbytaxa.sh in=Blast_contigs_names.fasta out=filtered_contigs.fasta tree=tree.taxtree.gz table=gitable.int1d.gz names=Acropora level=phylum include=t

## Split by taxo, create one file by phylum
#splitbytaxa.sh in=Blast_contigs_names.fasta out=%.fasta tree=tree.taxtree.gz table=gitable.int1d.gz level=family

## Print the complete taxonomy
#taxonomy.sh tree=tree.taxtree.gz table=gitable.int1d.gz Acropora
