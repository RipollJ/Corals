import os
import sys

configfile: "Config.yml"

ENVIRONMENT = "../envs/ClusMus.yml"
SPECIES = config["species"][1] # caution you have to declare species one by one
SAMPLE, = glob_wildcards("out/Sickle/{sample}_1.fastq")
THREADS = config['threads']['Normal']

###############################################################################

rule all:
    """

    ###############
    # OTU picking #
    ###############

    Author: Julie Ripoll
    Contact: julie.ripoll87@gmail.com
    Created: 2018
    License: License CC-BY-NC 4.0

    Warnings:
        these suite of scripts allow to quantify rRNA from microbiome using OTUs
        is was applied to see the main rRNA remaining after ribodepletion
        they can remain due to structure or very high abundance
        results can't be taken as real microbiome
        for this, you should realize a metabarcoding experiment

    Modified: 2018-10-30

    This snakefile need the use of snakemake version > 3.5 for solving conda environment with the --use-conda option.
    It needs also conda > 4.5

    Execute as this:
    >snakemake -s Microbiome/04_Microbiota_out_quant.smk --use-conda --conda-frontend mamba -p -r -j 1

    """
    input:
        rRNA3 = expand("out/rRNA_Eukaryota_otus/{sample}.sam",
                       sample=SAMPLE)

###############################################################################

rule Index_db:
    """
    Aim: Index LSU and SSU Silva databases
    """
    message:
        """--- Prepare Index for Sortmerna v2 ---"""
    params:
        threads = THREADS
    conda:
        ENVIRONMENT
    input:
        LSU_archaea_notalign = "Databases/Silva/LSU_archaea_notalign.fasta",
        LSU_bacteria_notalign = "Databases/Silva/LSU_bacteria_notalign.fasta",
        LSU_eukaryota_notalign = "Databases/Silva/LSU_eukaryota_notalign.fasta",
        SSU_archaea_notalign = "Databases/Silva/SSU_archaea_notalign.fasta",
        SSU_bacteria_notalign = "Databases/Silva/SSU_bacteria_notalign.fasta",
        SSU_eukaryota_notalign = "Databases/Silva/SSU_eukaryota_notalign.fasta"
    output:
        touch("out/done/Index.done")
    shell:
        "indexdb_rna --ref ./{input.LSU_archaea_notalign},./Databases/Silva/LSU_archaea_notalign.fasta.index:./{input.SSU_archaea_notalign},./Databases/Silva/SSU_archaea_notalign.fasta.index -m 4e+04 -v "
        "&& "
        "indexdb_rna --ref ./{input.LSU_bacteria_notalign},./Databases/Silva/LSU_bacteria_notalign.fasta.index:./{input.SSU_bacteria_notalign},./Databases/Silva/SSU_bacteria_notalign.fasta.index -m 4e+04 -v "
        "&& "
        "indexdb_rna --ref ./{input.LSU_eukaryota_notalign},./Databases/Silva/LSU_eukaryota_notalign.fasta.index:./{input.SSU_eukaryota_notalign},./Databases/Silva/SSU_eukaryota_notalign.fasta.index -m 4e+04 -v"


rule Merge_files:
    """
    Aim: Merging of files read 1 and read 2 with SortMeRNA.
    Note: Previously merged in Samples_quality steps if None this step will be executed
    """
    message:
        """--- MERGE-TRIMMED-FILES-{wildcards.sample} ---"""
    log:
        "out/Sortmerna_microbiome/log/{sample}.log"
    input:
        TrimmedR1 = "out/Sickle/{sample}_1.fastq",
        TrimmedR2 = "out/Sickle/{sample}_2.fastq"
    output:
        Merged = "out/Sortmerna_microbiome/Merged_{sample}.fastq"
    shell:
        "chmod +x Metatranscriptome/merge-paired-reads.sh "
        "&& "
        "Metatranscriptome/merge-paired-reads.sh {input.TrimmedR1} {input.TrimmedR2} {output.Merged} "
        "> {log}"


rule SortRNA_SSU_LSU_Archaea:
    """
    Aim: Filtering rRNA
    """
    message:
        """--- Sortmerna v2 - Archaea part - for {wildcards.sample} ---"""
    conda:
        ENVIRONMENT
    input:
        checkIndex = "out/done/Index.done",
        Merged = "out/Sortmerna_microbiome/Merged_{sample}.fastq"
    params:
        threads = THREADS,
        rRNA1 = "out/rRNA_Archaea_otus/{sample}"
    output:
        "out/rRNA_Archaea_otus/{sample}.sam"
    shell:
        "sortmerna "
        "--ref ./Databases/Silva/LSU_archaea_notalign.fasta,./Databases/Silva/LSU_archaea_notalign.fasta.index:./Databases/Silva/SSU_archaea_notalign.fasta,./Databases/Silva/SSU_archaea_notalign.fasta.index "
        "--reads {input.Merged} "
        "--aligned {params.rRNA1} "
        "--blast '1 cigar qcov qstrand' "
        "--sam "
        "--id 0.90 "
        "--coverage 0.95 "
        "--de_novo_otu "
        "--otu_map "
        "--log "
        "-v "
        "-a {params.threads}"


rule SortRNA_SSU_LSU_Bacteria:
    """
    Aim: Filtering rRNA
    """
    message:
        """--- Sortmerna v2 - Bacteria part - for {wildcards.sample} ---"""
    conda:
        ENVIRONMENT
    input:
        rRNA1 = "out/rRNA_Archaea_otus/{sample}.sam",
        Merged = "out/Sortmerna_microbiome/Merged_{sample}.fastq"
    params:
        threads = THREADS,
        rRNA2 = "out/rRNA_Bacteria_otus/{sample}"
    output:
        "out/rRNA_Bacteria_otus/{sample}.sam"
    shell:
        "sortmerna "
        "--ref ./Databases/Silva/LSU_bacteria_notalign.fasta,./Databases/Silva/LSU_bacteria_notalign.fasta.index:./Databases/Silva/SSU_bacteria_notalign.fasta,./Databases/Silva/SSU_bacteria_notalign.fasta.index "
        "--reads {input.Merged} "
        "--aligned {params.rRNA2} "
        "--blast '1 cigar qcov qstrand' "
        "--sam "
        "--id 0.90 "
        "--coverage 0.95 "
        "--de_novo_otu "
        "--otu_map "
        "--log "
        "-v "
        "-a {params.threads}"


rule SortRNA_SSU_LSU_Eukaryota:
    """
    Aim: Filtering rRNA
    """
    message:
        """--- Sortmerna v2 - Eukaryota part - for {wildcards.sample} ---"""
    conda:
        ENVIRONMENT
    input:
        rRNA2 = "out/rRNA_Bacteria_otus/{sample}.sam",
        Merged = "out/Sortmerna_microbiome/Merged_{sample}.fastq"
    params:
        threads = THREADS,
        rRNA3 = "out/rRNA_Eukaryota_otus/{sample}"
    output:
        "out/rRNA_Eukaryota_otus/{sample}.sam"
    shell:
        "sortmerna "
        "--ref ./Databases/Silva/LSU_eukaryota_notalign.fasta,./Databases/Silva/LSU_eukaryota_notalign.fasta.index:./Databases/Silva/SSU_eukaryota_notalign.fasta,./Databases/Silva/SSU_eukaryota_notalign.fasta.index "
        "--reads {input.Merged} "
        "--aligned {params.rRNA3} "
        "--blast '1 cigar qcov qstrand' "
        "--sam "
        "--id 0.90 "
        "--coverage 0.95 "
        "--de_novo_otu "
        "--otu_map "
        "--log "
        "-v "
        "-a {params.threads}"

# %id similarity threshold default 0.97
# %query coverage threshold default 0.97
