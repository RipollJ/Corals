import os
import sys

configfile: "Config.yml"

ENVIRONMENT = "../envs/ClusMus.yml"
ENVIRONMENT2 = "../envs/Assembly.yml"
SPECIES = config["species"][1] # caution you have to declare species one by one
SAMPLE, = glob_wildcards("out/Sortmerna/"+SPECIES+"/Merged_{sample}.fastq")
THREADS = config['threads']['Normal']

###############################################################################

rule all:
    """

    ##############
    # Microbiota #
    ##############

    Author: Julie Ripoll
    Contact: julie.ripoll87@gmail.com
    Created: 2018
    License: License CC-BY-NC 4.0

    Warnings:
        these suite of scripts allow to identify rRNA from microbiome as presence-absence
        is was applied to see the main rRNA remaining after ribodepletion
        they can remain due to structure or very high abundance
        results can't be taken as real microbiome
        for this, you should realize a metabarcoding experiment

    Modified: 2018-10-30

    This snakefile need the use of snakemake version > 3.5 for solving conda environment with the --use-conda option.
    It needs also conda > 4.5

    Execute as this:
    >snakemake -s Microbiome/05_Microbiota.smk --use-conda --conda-frontend mamba -p -r -j 1

    """
    input:
        rRNA3 = expand("out/rRNA_Eukaryota/{species}/{sample}.sam",
                       sample=SAMPLE,
                       species = SPECIES)

###############################################################################

rule Index_db:
    """
    Aim: Index LSU and SSU Silva databases
    """
    message:
        """--- Prepare Index for Sortmerna v2 ---"""
    params:
        threads = THREADS
    conda:
        ENVIRONMENT
    input:
        LSU_archaea_notalign = "Databases/Silva/LSU_archaea_notalign.fasta",
        LSU_bacteria_notalign = "Databases/Silva/LSU_bacteria_notalign.fasta",
        LSU_eukaryota_notalign = "Databases/Silva/LSU_eukaryota_notalign.fasta",
        SSU_archaea_notalign = "Databases/Silva/SSU_archaea_notalign.fasta",
        SSU_bacteria_notalign = "Databases/Silva/SSU_bacteria_notalign.fasta",
        SSU_eukaryota_notalign = "Databases/Silva/SSU_eukaryota_notalign.fasta"
    output:
        touch("out/done/Index.done")
    shell:
        "indexdb_rna --ref ./{input.LSU_archaea_notalign},./Databases/Silva/LSU_archaea_notalign.fasta.index:./{input.SSU_archaea_notalign},./Databases/Silva/SSU_archaea_notalign.fasta.index -m 4e+04 -v "
        "&& "
        "indexdb_rna --ref ./{input.LSU_bacteria_notalign},./Databases/Silva/LSU_bacteria_notalign.fasta.index:./{input.SSU_bacteria_notalign},./Databases/Silva/SSU_bacteria_notalign.fasta.index -m 4e+04 -v "
        "&& "
        "indexdb_rna --ref ./{input.LSU_eukaryota_notalign},./Databases/Silva/LSU_eukaryota_notalign.fasta.index:./{input.SSU_eukaryota_notalign},./Databases/Silva/SSU_eukaryota_notalign.fasta.index -m 4e+04 -v"


rule SortRNA_SSU_LSU_Archaea:
    """
    Aim: Filtering rRNA
    """
    message:
        """--- Sortmerna v2 - Archaea part - for {wildcards.sample} ---"""
    conda:
        ENVIRONMENT
    input:
        checkIndex = "out/done/Index.done",
        Merged = "out/Sortmerna/{species}/Merged_{sample}.fastq" # previously merged in 02_Filtering_rRNA.smk
    params:
        threads = THREADS,
        rRNA1 = "out/rRNA_Archaea/{species}/{sample}"
    output:
        "out/rRNA_Archaea/{species}/{sample}.sam"
    shell:
        "sortmerna "
        "--reads {input.Merged} "
        "--ref ./Databases/Silva/LSU_archaea_notalign.fasta,./Databases/Silva/LSU_archaea_notalign.fasta.index:./Databases/Silva/SSU_archaea_notalign.fasta,./Databases/Silva/SSU_archaea_notalign.fasta.index "
        "--blast 1 cigar qcov qstrand "
        "--sam "
        "--aligned {params.rRNA1} "
        "--log "
        "-v "
        "-a {params.threads}"


rule SortRNA_SSU_LSU_Bacteria:
    """
    Aim: Filtering rRNA
    """
    message:
        """--- Sortmerna v2 - Bacteria part - for {wildcards.sample} ---"""
    conda:
        ENVIRONMENT
    input:
        rRNA1 = "out/rRNA_Archaea/{species}/{sample}",
        Merged = "out/Sortmerna/{species}/Merged_{sample}.fastq"
    params:
        threads = THREADS,
        rRNA2 = "out/rRNA_Bacteria/{species}/{sample}"
    output:
        "out/rRNA_Bacteria/{species}/{sample}.sam"
    shell:
        "sortmerna "
        "--reads {input.Merged} "
        "--ref ./Databases/Silva/LSU_bacteria_notalign.fasta,./Databases/Silva/LSU_bacteria_notalign.fasta.index:./Databases/Silva/SSU_bacteria_notalign.fasta,./Databases/Silva/SSU_bacteria_notalign.fasta.index "
        "--blast 1 cigar qcov qstrand "
        "--sam "
        "--aligned {params.rRNA2} "
        "--log "
        "-v "
        "-a {params.threads}"


rule SortRNA_SSU_LSU_Eukaryota:
    """
    Aim: Filtering rRNA
    """
    message:
        """--- Sortmerna v2 - Eukaryota part - for {wildcards.sample} ---"""
    conda:
        ENVIRONMENT
    input:
        rRNA2 = "out/rRNA_Bacteria/{species}/{sample}",
        Merged = "out/Sortmerna/{species}/Merged_{sample}.fastq"
    params:
        threads = THREADS,
        rRNA3 = "out/rRNA_Eukaryota/{species}/{sample}"
    output:
        "out/rRNA_Eukaryota/{species}/{sample}.sam"
    shell:
        "sortmerna "
        "--reads {input.Merged} "
        "--ref ./Databases/Silva/LSU_eukaryota_notalign.fasta,./Databases/Silva/LSU_eukaryota_notalign.fasta.index:./Databases/Silva/SSU_eukaryota_notalign.fasta,./Databases/Silva/SSU_eukaryota_notalign.fasta.index "
        "--blast 1 cigar qcov qstrand "
        "--sam "
        "--aligned {params.rRNA3} "
        "--log "
        "-v "
        "-a {params.threads}"


rule zipMergedFiles:
    """
    Aim: zip merged files to save space
    """
    message:
        """--- Zip merged files to save space {wildcards.sample} ---"""
    conda:
        ENVIRONMENT2
    input:
        Merged = "out/Sortmerna/{species}/Merged_{sample}.fastq"
    output:
        Merged = "out/Sortmerna/{species}/Merged_{sample}.fastq.gz"
    shell:
        "bzip2 -9 {input.Merged}"
