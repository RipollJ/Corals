#!/bin/bash
cd $1

set -x

# run 1 and 2 together

mothur "#merge.taxsummary(input=NAC3C3.tax.summary-NAC3.tax.summary, output=output1.tax.summary)"

mothur "#merge.taxsummary(input=output1.tax.summary-NAC3V3.tax.summary, output=output2.tax.summary)"

mothur "#merge.taxsummary(input=output2.tax.summary-NAC4C3.tax.summary, output=output3.tax.summary)"

mothur "#merge.taxsummary(input=output3.tax.summary-NAC4.tax.summary, output=output4.tax.summary)"

mothur "#merge.taxsummary(input=output4.tax.summary-NAC4V3.tax.summary, output=output5.tax.summary)"

mothur "#merge.taxsummary(input=output5.tax.summary-NAC5C3.tax.summary, output=output6.tax.summary)"

mothur "#merge.taxsummary(input=output6.tax.summary-NAC5.tax.summary, output=output7.tax.summary)"

mothur "#merge.taxsummary(input=output7.tax.summary-NAC5V3.tax.summary, output=output8.tax.summary)"

mothur "#merge.taxsummary(input=output8.tax.summary-NAV1C3.tax.summary, output=output9.tax.summary)"

mothur "#merge.taxsummary(input=output9.tax.summary-NAV1.tax.summary, output=output10.tax.summary)"

mothur "#merge.taxsummary(input=output10.tax.summary-NAV1V3.tax.summary, output=output11.tax.summary)"

mothur "#merge.taxsummary(input=output11.tax.summary-NAV1V5M.tax.summary, output=output12.tax.summary)"

mothur "#merge.taxsummary(input=output12.tax.summary-NAV3C3.tax.summary, output=output13.tax.summary)"

mothur "#merge.taxsummary(input=output13.tax.summary-NAV3C5M.tax.summary, output=output14.tax.summary)"

mothur "#merge.taxsummary(input=output14.tax.summary-NAV3.tax.summary, output=output15.tax.summary)"

mothur "#merge.taxsummary(input=output15.tax.summary-NAV3V3.tax.summary, output=output16.tax.summary)"

mothur "#merge.taxsummary(input=output16.tax.summary-NAV3V5M.tax.summary, output=output17.tax.summary)"

mothur "#merge.taxsummary(input=output17.tax.summary-NAV4C3.tax.summary, output=output18.tax.summary)"

mothur "#merge.taxsummary(input=output18.tax.summary-NAV4C5M.tax.summary, output=output19.tax.summary)"

mothur "#merge.taxsummary(input=output19.tax.summary-NAV4.tax.summary, output=output20.tax.summary)"

mothur "#merge.taxsummary(input=output20.tax.summary-NAV4V3.tax.summary, output=output21.tax.summary)"

mothur "#merge.taxsummary(input=output21.tax.summary-NAV4V5M.tax.summary, output=output22.tax.summary)"

mothur "#merge.taxsummary(input=output22.tax.summary-NAV5C3.tax.summary, output=output23.tax.summary)"

mothur "#merge.taxsummary(input=output23.tax.summary-NAV5C5M.tax.summary, output=output24.tax.summary)"

mothur "#merge.taxsummary(input=output24.tax.summary-NAV5.tax.summary, output=output25.tax.summary)"

mothur "#merge.taxsummary(input=output25.tax.summary-NAV5V3.tax.summary, output=output26.tax.summary)"

mothur "#merge.taxsummary(input=output26.tax.summary-NAV5V5M.tax.summary, output=output27.tax.summary)"

mothur "#merge.taxsummary(input=output27.tax.summary-NPC1C3.tax.summary, output=output28.tax.summary)"

mothur "#merge.taxsummary(input=output28.tax.summary-NPC1C5M.tax.summary, output=output29.tax.summary)"

mothur "#merge.taxsummary(input=output29.tax.summary-NPC1.tax.summary, output=output30.tax.summary)"

mothur "#merge.taxsummary(input=output30.tax.summary-NPC1V3.tax.summary, output=output31.tax.summary)"

mothur "#merge.taxsummary(input=output31.tax.summary-NPC1V5M.tax.summary, output=output32.tax.summary)"

mothur "#merge.taxsummary(input=output32.tax.summary-NPC2C3.tax.summary, output=output33.tax.summary)"

mothur "#merge.taxsummary(input=output33.tax.summary-NPC2C5M.tax.summary, output=output34.tax.summary)"

mothur "#merge.taxsummary(input=output34.tax.summary-NPC2.tax.summary, output=output35.tax.summary)"

mothur "#merge.taxsummary(input=output35.tax.summary-NPC2V3.tax.summary, output=output36.tax.summary)"

mothur "#merge.taxsummary(input=output36.tax.summary-NPC2V5M.tax.summary, output=output37.tax.summary)"

mothur "#merge.taxsummary(input=output37.tax.summary-NPC3C3.tax.summary, output=output38.tax.summary)"

mothur "#merge.taxsummary(input=output38.tax.summary-NPC3C5M.tax.summary, output=output39.tax.summary)"

mothur "#merge.taxsummary(input=output39.tax.summary-NPC3.tax.summary, output=output40.tax.summary)"

mothur "#merge.taxsummary(input=output40.tax.summary-NPC3V3.tax.summary, output=output41.tax.summary)"

mothur "#merge.taxsummary(input=output41.tax.summary-NPC4C3.tax.summary, output=output42.tax.summary)"

mothur "#merge.taxsummary(input=output42.tax.summary-NPC4C5M.tax.summary, output=output43.tax.summary)"

mothur "#merge.taxsummary(input=output43.tax.summary-NPC4.tax.summary, output=output44.tax.summary)"

mothur "#merge.taxsummary(input=output44.tax.summary-NPC4V3.tax.summary, output=output45.tax.summary)"

mothur "#merge.taxsummary(input=output45.tax.summary-NPC4V5M.tax.summary, output=output46.tax.summary)"

mothur "#merge.taxsummary(input=output46.tax.summary-NPC5C3.tax.summary, output=output47.tax.summary)"

mothur "#merge.taxsummary(input=output47.tax.summary-NPC5C5M.tax.summary, output=output48.tax.summary)"

mothur "#merge.taxsummary(input=output48.tax.summary-NPC5.tax.summary, output=output49.tax.summary)"

mothur "#merge.taxsummary(input=output49.tax.summary-NPC5V3.tax.summary, output=output50.tax.summary)"

mothur "#merge.taxsummary(input=output50.tax.summary-NPC5V5M.tax.summary, output=output51.tax.summary)"

mothur "#merge.taxsummary(input=output51.tax.summary-NPV1C3.tax.summary, output=output52.tax.summary)"

mothur "#merge.taxsummary(input=output52.tax.summary-NPV1C5M.tax.summary, output=output53.tax.summary)"

mothur "#merge.taxsummary(input=output53.tax.summary-NPV1.tax.summary, output=output54.tax.summary)"

mothur "#merge.taxsummary(input=output54.tax.summary-NPV1V3.tax.summary, output=output55.tax.summary)"

mothur "#merge.taxsummary(input=output55.tax.summary-NPV1V5M.tax.summary, output=output56.tax.summary)"

mothur "#merge.taxsummary(input=output56.tax.summary-NPV2C3.tax.summary, output=output57.tax.summary)"

mothur "#merge.taxsummary(input=output57.tax.summary-NPV2C5M.tax.summary, output=output58.tax.summary)"

mothur "#merge.taxsummary(input=output58.tax.summary-NPV2.tax.summary, output=output59.tax.summary)"

mothur "#merge.taxsummary(input=output59.tax.summary-NPV2V3.tax.summary, output=output60.tax.summary)"

mothur "#merge.taxsummary(input=output60.tax.summary-NPV2V5M.tax.summary, output=output61.tax.summary)"

mothur "#merge.taxsummary(input=output61.tax.summary-NPV3C3.tax.summary, output=output62.tax.summary)"

mothur "#merge.taxsummary(input=output62.tax.summary-NPV3C5M.tax.summary, output=output63.tax.summary)"

mothur "#merge.taxsummary(input=output63.tax.summary-NPV3.tax.summary, output=output64.tax.summary)"

mothur "#merge.taxsummary(input=output64.tax.summary-NPV3V3.tax.summary, output=output65.tax.summary)"

mothur "#merge.taxsummary(input=output65.tax.summary-NPV3V5M.tax.summary, output=output66.tax.summary)"

mothur "#merge.taxsummary(input=output66.tax.summary-NPV4C3.tax.summary, output=output67.tax.summary)"

mothur "#merge.taxsummary(input=output67.tax.summary-NPV4C5M.tax.summary, output=output68.tax.summary)"

mothur "#merge.taxsummary(input=output68.tax.summary-NPV4.tax.summary, output=output69.tax.summary)"

mothur "#merge.taxsummary(input=output69.tax.summary-NPV4V3.tax.summary, output=output70.tax.summary)"

mothur "#merge.taxsummary(input=output70.tax.summary-NPV5C3.tax.summary, output=output71.tax.summary)"

mothur "#merge.taxsummary(input=output71.tax.summary-NPV5C5M.tax.summary, output=output72.tax.summary)"

mothur "#merge.taxsummary(input=output72.tax.summary-NPV5.tax.summary, output=output73.tax.summary)"

mothur "#merge.taxsummary(input=output73.tax.summary-NPV5V3.tax.summary, output=output74.tax.summary)"

mothur "#merge.taxsummary(input=output74.tax.summary-NPV5V5M.tax.summary, output=output75.tax.summary)"

mothur "#merge.taxsummary(input=output75.tax.summary-NAC3C5Mb.tax.summary, output=output76.tax.summary)"

mothur "#merge.taxsummary(input=output76.tax.summary-NAC3V5Mb.tax.summary, output=output77.tax.summary)"

mothur "#merge.taxsummary(input=output77.tax.summary-NAC4C5Mb.tax.summary, output=output78.tax.summary)"

mothur "#merge.taxsummary(input=output78.tax.summary-NAC4V5Mb.tax.summary, output=output79.tax.summary)"

mothur "#merge.taxsummary(input=output79.tax.summary-NAC5C5Mb.tax.summary, output=output80.tax.summary)"

mothur "#merge.taxsummary(input=output80.tax.summary-NAC5V5Mb.tax.summary, output=output81.tax.summary)"

mothur "#merge.taxsummary(input=output81.tax.summary-NAV4C5Mb.tax.summary, output=All_LSU_SSU.tax.summary)"

# run 2 séparement

mothur "#merge.taxsummary(input=NAC3C5Mb.tax.summary-NAC3V5Mb.tax.summary, output=output96.tax.summary)"

mothur "#merge.taxsummary(input=output96.tax.summary-NAC4C5Mb.tax.summary, output=output97.tax.summary)"

mothur "#merge.taxsummary(input=output97.tax.summary-NAC4V5Mb.tax.summary, output=output98.tax.summary)"

mothur "#merge.taxsummary(input=output98.tax.summary-NAC5C5Mb.tax.summary, output=output99.tax.summary)"

mothur "#merge.taxsummary(input=output99.tax.summary-NAC5V5Mb.tax.summary, output=output100.tax.summary)"

mothur "#merge.taxsummary(input=output100.tax.summary-NAV4C5Mb.tax.summary, output=Run2.tax.summary)"

rm output*.tax.summary
