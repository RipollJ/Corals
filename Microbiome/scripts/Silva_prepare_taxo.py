######################################
## Prepare Taxonomy file from SILVA ##
######################################

# Author: Fati Chen & Julie Ripoll
# Contact: chen.fati@gmail.com, julie.ripoll87@gmail.com
# Last update: 2022-08-09
# License: License CC-BY-NC 4.0

#################################
import pandas as pd

snakemake = snakemake # to avoid python missing variable error

# https://mothur.org/blog/2021/SILVA-v138_1-reference-files/
# levels we have
hierarchy = ["root", "domain", "major_clade", "superkingdom", "kingdom",
        "subkingdom", "infrakingdom", "superphylum", "phylum", "subphylum",
        "infraphylum", "superclass", "class", "subclass", "infraclass",
        "superorder", "order", "suborder", "superfamily", "family",
        "subfamily", "genus"]
# levels we want
outhierarchy = ["domain", "kingdom", "phylum", "class", "order", "family", "genus"]

# Rules to polyfill 
# levels we have -> levels we want
hierarchy_equiv = {'domain': "domain",
    # 'major_clade': "", can't be domain
    'superkingdom': "kingdom",
    'kingdom': "kingdom",
    'subkingdom': "kingdom",
    'infrakingdom': "kingdom",
    'superphylum': "phylum",
    'phylum': "phylum",
    'subphylum': "phylum",
    'infraphylum': "phylum",
    'superclass': "class",
    'class': "class",
    'subclass': "class",
    'infraclass': "class",
    'superorder': "order",
    'order': "order",
    'suborder': "order",
    'superfamily': "family",
    'family': "family",
    'subfamily': "family",
    'genus': "genus"}

df_ssu_taxo = pd.read_csv(snakemake.input["ssu"], names=["taxon", "dunno", "taxonlev", "dunno1", "dunno2"], sep="\t", header= None)
df_lsu_taxo = pd.read_csv(snakemake.input["lsu"],  names=["taxon", "dunno", "taxonlev", "dunno1", "dunno2"], sep="\t", header= None)
df_taxo = df_ssu_taxo.append(df_lsu_taxo)
df_taxo = df_taxo[["taxon", "taxonlev"]]
df_taxo.drop_duplicates(subset=["taxon", "taxonlev"], inplace=True)
df_taxo["taxonlev"] = pd.Categorical(df_taxo["taxonlev"], categories=hierarchy, ordered=True)

def get_full_taxons(taxon):
    """retrieve all known levels of taxon from df_taxo"""
    taxons = taxon.split(";")
    acc = []
     # search for the level with increasing detail of taxon
    for idx, taxon in enumerate(taxons, 1):
        subpart = ";".join(taxons[:idx])
        candidates = df_taxo.loc[df_taxo["taxon"].str.contains(subpart), "taxonlev"].sort_values()
        if len(candidates) > 0:
            acc.append((taxon, candidates.iloc[0]))
    return acc

df_notalign = pd.read_csv(snakemake.input["notalign"], names=["dunno", "taxon"], sep="\t", header= None)
df_notalign["levels"] = df_notalign["taxon"].map(get_full_taxons)
df_notalign[outhierarchy] = ""

def hierarchy_projection(row):
    """Get wanted levels and try to accomodate when equivalent levels"""
    for (name, level) in row.levels:
        if level in row: # is a valid hierarchy
            row[level] = name
        elif level in hierarchy_equiv and row[hierarchy_equiv[level]] == "":
            # is not a valid hierarchy trying to polyfill
            row[hierarchy_equiv[level]] = name+"__"+level

def fill_missing_hierarchy(row):
    """fill in between missing values"""
    hasvalue = False
    for level in reversed(outhierarchy):
        if row[level] != "":
            hasvalue = True
        elif row[level] == "" and hasvalue == True:
            row[level] = 'unknown'

df_notalign.apply(hierarchy_projection, axis=1)
df_notalign.apply(fill_missing_hierarchy, axis=1)

# collapse level columns into a single identifier
df_notalign["outlevel"] = df_notalign[outhierarchy].agg(lambda xs: ';'.join([x for x in xs if x]), axis=1)

df_notalign[["dunno", "outlevel"]].to_csv(snakemake.output["notalign"], header=False, index=False, sep="\t")