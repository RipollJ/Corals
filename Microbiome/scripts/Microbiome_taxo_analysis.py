#!/usr/bin/env python
# coding: utf-8

# Microbiome Taxonomy analysis
#
# - auteur: Julie Ripoll & Fati Chen
# - date: 2022-08-05

# load packages
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from math import pi
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from contextlib import redirect_stdout
from sklearn.cross_decomposition import PLSRegression


snakemake = snakemake # avoid undeclared variable error everywhere

## functions

def lenTaxo(df, tax):
    """ size of corresponding taxon in df

    ! deprecated use `len(df[df["taxlevel"] == tax])` instead
    """
    df2 = df[df["taxlevel"] == tax]
    print(len(df2))


def plot_taxo(df, tax):
    """ df: pd.DataFrame, tax: str """
    sns.displot(data=df[["Acropora_run1_sum", "Acropora_run2_sum"]], palette=['red', 'blue'], kind="hist", kde=True)

    plt.savefig(snakemake.params["barplot"] + tax + ".svg", bbox_inches='tight')
    plt.close()

def plot_run_density(df, tax):
    """ df: pd.DataFrame, tax: str """
    sns.displot(data=df[["NAV4C5M", "NAV4C5Mb"]], palette=['red', 'blue'], kind="hist", kde=True)

    plt.savefig(snakemake.params["diffrun"] + tax + ".svg", bbox_inches='tight')
    plt.close()

def testplot(labels, x, y, xtitle, ytitle, filename):
    _ , ax = plt.subplots()
    ax.fill_between(labels, x, y, alpha=.5, linewidth=0)
    ax.plot(labels, x, color='blue')
    ax.plot(labels, y, color='red')

    plt.xticks(rotation=90)

    ax.set_xlabel(xtitle, fontsize = 15)
    ax.set_ylabel(ytitle, fontsize = 10)

    plt.savefig(filename, bbox_inches='tight')
    plt.close()

def plot_test(df, tax, clip=False):
    """ Prepares the data for the test plot
    
    Will take in account if the data needs to be clipped.
    """
    x = df["Acropora_run1_sum"] 
    y = df["Pocillopora_sum"]  
    param = "test" 

    if clip:
        x = x.clip(lower=0, upper=1)
        y = y.clip(lower=0, upper=1)
        param += "_clipped"

    testplot(df["taxon"],
        x=x,
        y=y,
        xtitle=tax,
        ytitle=f"Differences between Acropora and Pocillopora \nin presence - absence of {tax} identification",
        filename=snakemake.params[param] + tax + ".svg"
    ) 


def diffplot(x, y, xlabel, ylabel, filename):
    g = sns.lineplot(x=x, y=y)
    plt.axhline(y=0, color='black', linestyle='--')

    plt.xticks(rotation=90)

    g.set_xlabel(xlabel, fontsize = 15)
    g.set_ylabel(ylabel, fontsize = 10)

    plt.savefig(filename, bbox_inches='tight')
    plt.close()

def plot_diff(df, tax, clip=False):
    """ difference between two sum columns """
    y = df["Acropora_sum"] - df["Pocillopora_sum"]
    param = "diffplot"

    if clip:
        y = y.clip(lower=0, upper=1)
        param += "_clipped"

    diffplot(df["taxon"], y,
        xlabel=tax, 
        ylabel=f"Differences between all Acropora and Pocillopora \nin presence - absence of {tax} identification",
        filename=snakemake.params[param] + tax + ".svg")

def plot_diff_run1(df, tax):
    diffplot(df["taxon"], df["Acropora_run1_sum"] - df["Pocillopora_sum"],
        xlabel=tax, 
        ylabel=f"Differences between all Acropora and Pocillopora \nin presence - absence of {tax} identification",
        filename=snakemake.params["diffplot2"] + tax + ".svg")


def spiderplot(x, y, label, color, title):
    # ------- PART 1: Create background
    # number of variable
    categories=list(x)
    N = len(categories)
    # What will be the angle of each axis in the plot? (we divide the plot / number of variable)
    angles = [n / float(N) * 2 * pi for n in range(N)]
    angles += angles[:1]
    # Initialise the spider plot
    ax = plt.subplot(111, polar=True)
    # If you want the first axis to be on top:
    # ax.set_theta_offset(pi / 2)
    # ax.set_theta_direction(-1)
    # Draw one axe per variable + add labels
    plt.xticks(angles[:-1], categories, size=8)
    # Draw ylabels
    ax.set_rlabel_position(45)
    # plt.yticks([10,20,30], ["10","20","30"], color="grey", size=7)
    # plt.ylim(0,40)
     # ------- PART 2: Add plots
    # Plot each individual = each line of the data
    # I don't make a loop, because plotting more than 3 groups makes the chart unreadable
    values= y.values.flatten().tolist()
    values += values[:1]
    ax.plot(angles, values, linewidth=1, linestyle='solid', label=label, color=color)
    ax.fill(angles, values, color, alpha=0.1)
    # Add legend
    plt.legend(loc='upper right', bbox_to_anchor=(1.9, 0.1))
    # Add title
    plt.title(title, pad = 5)

    angles = np.linspace(0,2*np.pi,len(ax.get_xticklabels())+1)
    angles[np.cos(angles) < 0] = angles[np.cos(angles) < 0] + np.pi
    angles = np.rad2deg(angles)
    labels = []
    for label, angle in zip(ax.get_xticklabels(), angles[:-1]):
        x, y = label.get_position()
        label.set_rotation(0)
        lab = ax.text(x,y-.5, label.get_text(), transform=label.get_transform(),
                    ha=label.get_ha(), va=label.get_va())
        lab.set_rotation(angle)
        labels.append(lab)
    ax.set_xticklabels([])
    # Show the graph
    # plt.show()
    # plt.savefig(filename, bbox_inches='tight')
    # plt.close()

def plot_spider(df, tax, column, label, colorline):
    spiderplot(df["taxon"], df[column], color=colorline, label=label, title=tax)

    plt.savefig(snakemake.params["spiderplot"] + tax + "_" + label +".svg", bbox_inches='tight')
    plt.close()

def plot_multi_spider(df, tax, data, clip=False):
    """ df: pd.DataFrame, tax: str, data: list[tuple[str, str, str]] """
    clip_str = "_clipped" if clip else ""

    for label, column, color in data:
        y = df[column].clip(lower=0, upper=1) if clip else df[column]

        spiderplot(df["taxon"], y, color=color, label=label, title=tax)

    plt.savefig(snakemake.params["spiderplot"] + tax + clip_str +  ".svg", bbox_inches='tight')
    plt.close()



def plot_PCA(df, tax):
    # PCA
    df3 = df.loc[:,df.columns.str.contains("N")] 

    #X = df2[[col1, col2]]
    # Standardizing the features
    normalized = StandardScaler().fit_transform(df3)

    pca = PCA(n_components=2)
    components = pca.fit_transform(normalized.transpose())

    df_pca = pd.DataFrame(components, columns=["PCA1", "PCA2"])
    df_pca["Origin"] = df3.columns.map(lambda x: x[:3]) # get first 3 letters

    sns.scatterplot(data=df_pca, x="PCA1", y="PCA2", hue="Origin")

    plt.savefig(snakemake.params["pca"] + tax + ".svg", bbox_inches='tight')
    plt.close()

def plot_PLSDA(df, tax):
    df_categories = df.loc[:,  df.columns.str.contains('NP') + df.columns.str.contains('NA')]
     
    categories = np.array(df_categories.columns.str.startswith('NP'), dtype=int)
    values = df_categories.T.clip(lower=0, upper=1).values

    plsr = PLSRegression(n_components=2, scale=False)
    plsr.fit(values, categories)

    df_scores = pd.DataFrame(plsr.x_scores_, columns=["PLS-DA 1", "PLS-DA 2"])
    df_scores.index = df_categories.columns
    df_scores["Species"] = df_categories.columns.map(lambda x: x[:2])
    df_scores["Species"].replace({"NA": "Acropora", "NP": "Pocillopora"}, inplace=True)
    df_scores["Days"] = df_categories.columns.map(lambda x: x[4:])
    df_scores["Days"].replace({"": "D0", "C3": "D3", "C5M": "D110", "V5M": "D110", "C5Mb": "D110", "V3": "D3", "V5Mb": "D110"}, inplace=True)
    df_scores["Site"] = df_categories.columns.map(lambda x: x[2:])
    df_scores["Site"] = df_scores["Site"].replace(r"[\dMb]", "", regex=True)
    
    sns.scatterplot(data=df_scores, x="PLS-DA 1", y="PLS-DA 2", hue="Days")
    plt.savefig(snakemake.params["plsda_days"] + tax + ".svg", bbox_inches='tight')
    plt.close()

    sns.scatterplot(data=df_scores, x="PLS-DA 1", y="PLS-DA 2", hue="Species", style="Species")
    plt.savefig(snakemake.params["plsda_species"] + tax + ".svg", bbox_inches='tight')
    plt.close()

    sns.scatterplot(data=df_scores, x="PLS-DA 1", y="PLS-DA 2", hue="Site", palette = sns.color_palette("tab10", 6))
    plt.savefig(snakemake.params["plsda_site"] + tax + ".svg", bbox_inches='tight')
    plt.close()


# def tax_stats(df):
#     # clip all columns
#     clipped_acropora = df.loc[:,df.columns.str.startswith("NA")].clip(0, 1)
#     clipped_pocillopora = df.loc[:,df.columns.str.startswith("NP")].clip(0, 1)
#     # reduce columns
#     acropora_count = clipped_acropora.max(axis=1)
#     pocillopora_count = clipped_pocillopora.max(axis=1)

#     # retrieve statistics
#     df_presenceprob = pd.DataFrame({
#         "Acropora_count" : acropora_count, # soit 1 soit 0
#         "Pocillopora_count" : pocillopora_count, # soit 1 soit 0
#         "difference": acropora_count != pocillopora_count, # difference between both of them
#         "common": acropora_count == pocillopora_count, # common between both of them
#         "Acropora_mean" : clipped_acropora.mean(axis=1),
#         "Pocillopora_mean" : clipped_pocillopora.mean(axis=1)})
    
#     # get statistics by taxlevel
#     df_stats = df_presenceprob.pivot_table(index=df["taxlevel"], aggfunc=
#         {"Acropora_count": np.sum,
#         "Pocillopora_count": np.sum, 
#         "difference": np.sum,
#         "common": np.sum,
#         "Acropora_mean": np.mean,
#         "Pocillopora_mean": np.mean}, sort=False)
    
#     df_stats.to_csv(snakemake.params["stats"] + ".tsv",sep="\t", header=True, index=True)


with open(snakemake.output["log"], 'w') as f:
    with redirect_stdout(f):
        # using contextmanager to redirect print to output.log
        print("data") # import data
        data = pd.read_csv(snakemake.input["microbiome"], sep="\t", header= 0)
        print(data.head())

        # rename taxlevel
        rep = {0: "Root", 
            1: "Domain",
            2: "Kingdom",
            3: "Phylum",
            4: "Class",
            5: "Order",
            6: "Family",
            7: "Genus"} # define desired replacements here

        data["taxlevel"].replace(to_replace=rep, inplace=True)
        print(data.head())


        # Perform sum
        data["Acropora_sum"] = data.loc[:,data.columns.str.contains("NA")].sum(axis=1)
        data["Pocillopora_sum"] = data.loc[:,data.columns.str.contains("NP")].sum(axis=1)
        data["Acropora_run2_sum"] = data.loc[:,data.columns.str.contains("b")].sum(axis=1)
        data["Acropora_run1_sum"] = data["Acropora_sum"]  - data["Acropora_run2_sum"]
        print(data.head())


        # comptages

        # tax_stats(data)

        # build plots

        plot_PLSDA(data, "all") # empty group => all

        for group in ["Kingdom", "Phylum", "Class", "Order", "Family"]:
            df_group = data[data["taxlevel"] == group]
            df_group.to_csv(snakemake.params["dat_not_clipped"] + group + ".tsv",sep="\t", header=True, index=True)
            # lenTaxo(data, group)
            print(f"{group}: {len(df_group)}")

            plot_taxo(df_group, group)
            plot_run_density(df_group, group)
            plot_test(df_group, group)
            plot_test(df_group, group, clip=True)
            plot_diff(df_group, group)
            plot_diff(df_group, group, clip=True)
            plot_diff_run1(df_group, group)
            plot_spider(df_group, group, "Acropora_sum", "Acropora", "blue")
            plot_spider(df_group, group, "Acropora_run1_sum", "Acropora_run1", "darkblue")
            plot_spider(df_group, group, "Pocillopora_sum", "Pocillopora", "red")
            plot_multi_spider(df_group, group, 
                [("Acropora", "Acropora_sum", "blue"), ("Pocillopora", 'Pocillopora_sum', "red")], clip=True)
            plot_PCA(df_group, group)
            plot_PLSDA(df_group, group)
