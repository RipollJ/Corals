import os
import sys

configfile: "Config.yml"

ENVIRONMENT1 = "../envs/DBD.yml"
ENVIRONMENT2 = '../envs/R.yml'
ENVIRONMENT3 = '../envs/pandas.yml'
ENVIRONMENT = '../envs/Rstat.yml'

MICROBIOME = config["microbiome"]
SPECIES = config["species"]
THREADS = config['threads']['Normal']

###############################################################################

rule all:
    """

    ####################
    # Summary Taxonomy #
    ####################

    Author: Julie Ripoll
    Contact: julie.ripoll87@gmail.com
    Created: 2018
    License: License CC-BY-NC 4.0

    Modified: 2018-10-30

    Note: Add taxonomy to results of microbiome

    This snakefile need the use of snakemake version > 3.5 for solving conda environment with the --use-conda option. 
    It needs also conda > 4.5

    Execute as this:
    >snakemake -s Microbiome/07_Summary_tax_all_species.smk --use-conda --conda-frontend mamba -p -r -j 1

    """
    input:
        sumtax = expand("out/rRNA_{microbiome}/Taxonomy/Plots/summary.log",
                        microbiome=MICROBIOME),
        sumstats = expand("out/rRNA_{microbiome}/Taxonomy/Plots/stats_summary.log",
                        microbiome=MICROBIOME)

###############################################################################

rule Prepare_rule:
    """
    Aim: Collects documents for merge
    """
    message:
        """--- Collects documents for merge by {wildcards.microbiome} ---"""
    input:
        "out/rRNA_{microbiome}/{species}/Taxonomy/"
    params:
        "out/rRNA_{microbiome}/Taxonomy/"
    output:
        "out/rRNA_{microbiome}/Taxonomy/{species}_log_scp.log"
    shell:
        """
        if [ ! -d {params} ]; then
            mkdir {params};
        fi

        scp {input}/*.tax.summary {params} 

        echo 'done' > {output}
        """


rule Summary_tax_Final:
    """
    Aim: Creates a final taxonomy file by Microbiota category
    """
    message:
        """--- Combine all taxonomy in one file for {wildcards.microbiome} ---"""
    conda:
        ENVIRONMENT1
    input:
        expand("out/rRNA_{microbiome}/Taxonomy/{species}_log_scp.log", species = SPECIES)
    params:
        "out/rRNA_{microbiome}/Taxonomy/"
    output:
        "out/rRNA_{microbiome}/Taxonomy/All_LSU_SSU.tax.summary"
    shell:
        "chmod +x scripts/Merge_SumTax.sh "
        "&& "
        'scripts/Merge_SumTax.sh {params} '


rule Plots:
    """
    Aim: Plots
    """
    message:
        """--- Export plots {wildcards.microbiome} ---"""
    conda:
        ENVIRONMENT
    input:
        microbiome = "out/rRNA_{microbiome}/Taxonomy/All_LSU_SSU.tax.summary"
    params:
        barplot = "out/rRNA_{microbiome}/Taxonomy/Plots/Barpot_{microbiome}_",
        diffrun = "out/rRNA_{microbiome}/Taxonomy/Plots/Diff_run_{microbiome}_",
        test = "out/rRNA_{microbiome}/Taxonomy/Plots/Test_{microbiome}_",
        test_clipped = "out/rRNA_{microbiome}/Taxonomy/Plots/Test_{microbiome}_",
        diffplot = "out/rRNA_{microbiome}/Taxonomy/Plots/Diff_plot_all_{microbiome}_",
        diffplot_clipped = "out/rRNA_{microbiome}/Taxonomy/Plots/Diff_plot_all_clip_{microbiome}_",
        diffplot2 = "out/rRNA_{microbiome}/Taxonomy/Plots/Diff_plot_run1_{microbiome}_",
        spiderplot = "out/rRNA_{microbiome}/Taxonomy/Plots/Spiderplot_{microbiome}_",
        pca = "out/rRNA_{microbiome}/Taxonomy/Plots/PCA_{microbiome}_",
        dat_not_clipped = "out/rRNA_{microbiome}/Taxonomy/Plots/Data_{microbiome}_",
        dat_clipped = "out/rRNA_{microbiome}/Taxonomy/Plots/Bin_Data_{microbiome}_",
        plsda_species = "out/rRNA_{microbiome}/Taxonomy/Plots/plsda_species_{microbiome}_",
        plsda_days = "out/rRNA_{microbiome}/Taxonomy/Plots/plsda_days_{microbiome}_",
        plsda_site = "out/rRNA_{microbiome}/Taxonomy/Plots/plsda_site_{microbiome}_"
    output:
        log = "out/rRNA_{microbiome}/Taxonomy/Plots/summary.log"
    script:
        "scripts/Microbiome_taxo_analysis.py"


rule Stats:
    """
    Aim: Plots
    """
    message:
        """--- Export Stats {wildcards.microbiome} ---"""
    conda:
        ENVIRONMENT
    input:
        microbiome = "out/rRNA_{microbiome}/Taxonomy/All_LSU_SSU.tax.summary"
    params:
        stats = "out/rRNA_{microbiome}/Taxonomy/Plots/stats_{microbiome}",
        names = "out/rRNA_{microbiome}/Taxonomy/Plots/stats_names_{microbiome}",
        percents = "out/rRNA_{microbiome}/Taxonomy/Plots/stats_percents_{microbiome}",
    output:
        log = "out/rRNA_{microbiome}/Taxonomy/Plots/stats_summary.log"
    notebook:
        "scripts/Microbiome_taxo_stats.ipynb"
