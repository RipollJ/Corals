import os
import sys

configfile: "Config.yml"

ENVIRONMENT1 = "../envs/DBD.yml"
ENVIRONMENT2 = '../envs/R.yml'
ENVIRONMENT3 = '../envs/pandas.yml'

MICROBIOME = config["microbiome"]
SPECIES = config["species"] # warning if trouble you have to declare species one by one with config["species"][0] and then config["species"][1]...
SAMPLE, = glob_wildcards("out/Sortmerna/"+SPECIES+"/Merged_{sample}.fastq") # species in wildcards not authorized
THREADS = config['threads']['Normal']

###############################################################################

rule all:
    """

    ####################
    # Summary Taxonomy #
    ####################

    Author: Julie Ripoll
    Contact: julie.ripoll87@gmail.com
    Created: 2018
    License: License CC-BY-NC 4.0

    Modified: 2018-10-30

    This snakefile need the use of snakemake version > 3.5 for solving conda environment with the --use-conda option. 
    It needs also conda > 4.5

    Execute as this:
    >snakemake -s Microbiome/06_Summary_tax.smk --use-conda --conda-frontend mamba -p -r -j 1

    """
    input:
        outFile = expand("out/rRNA_{microbiome}/{species}/Taxonomy/log/{sample}_del_total.log",
                         sample=SAMPLE,
                         species=SPECIES,
                         microbiome=MICROBIOME)

###############################################################################

rule ConvertLevel:
    """
    Aim: Format Silva files
    """
    message:
        """--- Prepares Silva files for taxonomy analyses for {wildcards.microbiome} ---"""
    conda:
        ENVIRONMENT3
    input:
        notalign = "Databases/Silva/{microbiome}_notalign.tax",
        ssu = "Databases/Silva/tax_slv_ssu_128.txt",
        lsu = "Databases/Silva/tax_slv_lsu_128.txt"
    output:
        notalign = "Databases/Silva/{microbiome}_notalign_clean_level.tax"
    script:
        "scripts/Silva_prepare_taxo.py"


rule Format:
    """
    Aim: Format output of SortMeRNA
        # blast 8 output format
        # query 	subject 	%id 	alignmentlength	mismatches 	gapopenings 	querystart	queryend	subjectstart	subjectend 	Evalue	bitscore
        # https://edwards.flinders.edu.au/blast-output-8/
    """
    message:
        """--- Prepares output of SortMeRNA files for taxonomy analyses for {wildcards.microbiome} - {wildcards.sample} ---"""
    conda:
        ENVIRONMENT1
    input:
        notalign = "Databases/Silva/{microbiome}_notalign_clean_level.tax",
        samrRNA = "out/rRNA_{microbiome}/{species}/{sample}.sam",
        blastrRNA = "out/rRNA_{microbiome}/{species}/{sample}.blast"
    output:
        rFasta = "out/rRNA_{microbiome}/{species}/Filtered/{sample}.fasta",
        rFatxt = "out/rRNA_{microbiome}/{species}/Filtered/{sample}.fatext",
        rtxt = "out/rRNA_{microbiome}/{species}/Filtered/{sample}.txt"
    shell:
        """awk '/^@/{{ next; }} {{ OFS="\t"; print ">"$3,$10 }}' {input.samrRNA} > {output.rFatxt} """
        "&& "
        """awk '/^@/{{ next; }} {{ OFS="\t"; print ">"$2,$4,$11 }}' {input.blastrRNA} > {output.rtxt} """
        "&& "
        """awk '/^@/{{ next; }} {{ print ">"$3; print $10 }}' {input.samrRNA} > {output.rFasta}"""


rule Tax:
    """
    Aim: Add taxonomy to rRNA output files
    """
    message:
        """--- Add taxonomy for {wildcards.microbiome} - {wildcards.sample} ---"""
    conda:
        ENVIRONMENT2
    log:
        log = "out/rRNA_{microbiome}/{species}/Taxonomy/log/{sample}_tax.log"
    input:
        fatext = "out/rRNA_{microbiome}/{species}/Filtered/{sample}.fatext",
        txt = "out/rRNA_{microbiome}/{species}/Filtered/{sample}.txt",
        outdb = "Databases/Silva/{microbiome}_notalign_clean_level.tax"
    output:
        taxo = "out/rRNA_{microbiome}/{species}/Taxonomy/{sample}_Uniq.tax",
        merge = "out/rRNA_{microbiome}/{species}/Taxonomy/{sample}_duplicates.tax"
    script:
        "scripts/Rscript_microbiome_tax.R"


rule Summary_tax:
    """
    Aim: Creates a summary of taxonomy files using uniq taxon for presence - absence analysis
    """
    message:
        """--- Summary of taxonomy for {wildcards.microbiome} - {wildcards.sample} ---"""
    conda:
        ENVIRONMENT1
    priority: 100
    input:
        uniqf = "out/rRNA_{microbiome}/{species}/Taxonomy/{sample}_Uniq.tax"
    params:
        fold = "out/rRNA_{microbiome}/{species}/Taxonomy/intermediate_files/",
        outtmp = "out/rRNA_{microbiome}/{species}/Taxonomy/intermediate_files/{sample}.tax",
        outint = "out/rRNA_{microbiome}/{species}/Taxonomy/intermediate_files/{sample}.tax.summary"
    output:
        outfinal = "out/rRNA_{microbiome}/{species}/Taxonomy/{sample}.tax.summary",
        log = "out/rRNA_{microbiome}/{species}/Taxonomy/intermediate_files/{sample}_taxsum.log"
    shell:
        """awk '{{ OFS="\\t"; print $1,$3";" }}' {input.uniqf} | sed '1d' | uniq > {params.outtmp} """
        "&& "
        'mothur "#summary.tax(taxonomy={params.outtmp}, outputdir={params.fold})" '
        "&& "
        "1>{output.log} "
        "&& "
        """
        awk '{{ OFS="\\t"; print $1, $2, $3, $4, $5, $5 }}' {params.outint} | sed "1s/taxlevel\\trankID\\ttaxon\\tdaughterlevels\\ttotal/taxlevel\\trankID\\ttaxon\\tdaughterlevels\\ttotal\\t{wildcards.sample}/g" > {output.outfinal} 
        """


rule Summary_tax_delete_total:
    """
    Aim: Creates a final taxonomy file by Microbiota category
    """
    message:
        """--- Delete duplicate column {wildcards.microbiome} - {wildcards.sample} ---"""
    conda:
        ENVIRONMENT1
    input:
        "out/rRNA_{microbiome}/{species}/Taxonomy/{sample}.tax.summary"
    output:
        "out/rRNA_{microbiome}/{species}/Taxonomy/log/{sample}_del_total.log"
    shell:
        """sed -i '1s/\\ttotal\\t{wildcards.sample}\\ttotal/\\ttotal\\t{wildcards.sample}/' {input} """
        "2>{output}"
