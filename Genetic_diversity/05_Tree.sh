#!/bin/bash

#####################################
## Tree files after identification ##
#####################################

# Author: Julie Ripoll
# Contact: julie.ripoll87@gmail.com
# Created: 2018-10
# Last update: 2022-07-11
# License: License CC-BY-NC 4.0

# Note: Execute this script after Identification.sh

###############################################################################
# Software Environment
#conda env create -f ./envs/ClusMus.yml
conda activate ClusMus # "source activate " for conda < 4.5.11

###############################################################################
# Add sequences of reference files for plot tree
mkdir out/Sortmerna/ITS2_Symb/Tree

cat out/Sortmerna/ITS2_Symb/Fasta/All_ITS2_Symb.fasta Databases/Cunning_ITS2_db_CoralsOnly.fasta >out/Sortmerna/ITS2_Symb/Tree/ALL_Symb_and_ref.fasta

###############################################################################
# alignment ITS2 Symb
clustalw -align -infile=out/Sortmerna/ITS2_Symb/Tree/ALL_Symb_and_ref.fasta -type=DNA -outfile=out/Sortmerna/ITS2_Symb/Tree/ALL_Symb_and_ref.align.out.fasta -stats=out/Sortmerna/ITS2_Symb/Tree/ALL_Symb_and_ref_Clustal.log

muscle -in out/Sortmerna/ITS2_Symb/Tree/ALL_Symb_and_ref.fasta -out out/Sortmerna/ITS2_Symb/Tree/ALL_Symb_and_ref_muscle.fasta -maxiters 1

###############################################################################
# add outgroup for tree file
#choose your outgroup and add it
muscle -in out/Sortmerna/ITS2_Symb/Tree/ALL_Symb_and_ref_with_outgroup.fasta -out out/Sortmerna/ITS2_Symb/Tree/Symb_ref_outgroup_muscle.fasta -maxiters 1

###############################################################################
# test best model for tree
##xdg-open https://github.com/ddarriba/jmodeltest2 # install jmodeltest2
java -jar jModelTest.jar -d out/Sortmerna/ITS2_Symb/Tree/Symb_ref_outgroup_muscle.fasta -g 4 -i -f -AICc -BIC -v -tr 24 -o out/Sortmerna/ITS2_Symb/Tree/jmodeltest_ITS2_Symb.out # -g 4 = 4 modèles de bases; -g 1 = 22 modèles comparés

###############################################################################
# fastTree
FastTree -gtr -log Log/logfileOutgroup -seed 04072018 -nt out/Sortmerna/ITS2_Symb/Tree/Symb_ref_outgroup_muscle.fasta >out/Sortmerna/ITS2_Symb/Tree/tree_file_ITS2_Symb_Muscle_outgroup

###############################################################################
# Common sequence
#ITS2 of Symbiodiniaceae here was uniq, a common sequence was used for plot in ITOL and named Sample
#Output File : Symb_ref_outgroup_muscle_uniqSample.fasta

###############################################################################
# ITOL supplemental files for color are provided in the gitlab CORALS project
# This following part is an example of the first step of preparation of these files

# preparing file for ITOL online
#grep '>' Symb_ref_outgroup_muscle_uniqSample.fasta | sed 's/>//g' > Colors.txt

# add information to file (TREE_COLORS \n SEPARATOR TAB \n DATA)
# add clade information and colors and character information
# exemple: 777 cladex #c04104 normal(dashed,bold...) 1
#awk 'substr($0,0,1)=="A"{print $1 "\t" "range" "\t" "#c23551" "\t" "Clade_A" "\t" "1";} substr($0,0,1)=="B"{print $1 "\t" "range" "\t" "#bdb6fb" "\t" "Clade_B" "\t" "1";} substr($0,0,1)=="C"{print $1 "\t" "range" "\t" "#15daa6" "\t" "Clade_C" "\t" "1";} substr($0,0,1)=="D"{print $1 "\t" "range" "\t" "#c4f71f" "\t" "Clade_D" "\t" "1";} substr($0,0,1)=="E"{print $1 "\t" "range" "\t" "#ffff66" "\t" "Clade_E" "\t" "1";} substr($0,0,1)=="F"{print $1 "\t" "range" "\t" "#f36f0f" "\t" "Clade_F" "\t" "1";} substr($0,0,1)=="G"{print $1 "\t" "range" "\t" "#90a533" "\t" "Clade_G" "\t" "1";} substr($0,0,1)=="H"{print $1 "\t" "range" "\t" "#c04104" "\t" "Clade_H" "\t" "1";} substr($0,0,1)=="I"{print $1 "\t" "range" "\t" "#8a3861" "\t" "Clade_I" "\t" "1";} substr($0,0,1)=="S"{print $1 "\t" "range" "\t" "#0000ff" "\t" "Sample" "\t" "1";}' Colors.txt > colors.txt

# go to ITOL online to plot your tree
#https://itol.embl.de/

###############################################################################
# deactivate environment
conda deactivate ClusMus

###############################################################################
# END
