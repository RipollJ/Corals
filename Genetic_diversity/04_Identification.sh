#!/bin/bash

#################################################
## Identification of corals and Symbiodiniaceae ##
#################################################

# Author: Julie Ripoll
# Contact: julie.ripoll87@gmail.com
# Created: 2018-10
# Last update: 2022-07-11
# License: License CC-BY-NC 4.0

# Note: this script needs the installation of databases available through Databases.sh and NCBI.sh

###############################################################################
# Software Environment
#conda create --name ClusMus clustalw=2.1 muscle=3.8 diamond=0.9.14 sortmerna=2.1 FastTree=2.1.9 mothur=1.39 rpy2 r
#conda env export -n ClusMus > ClusMus.yml
conda env create -n ClusMus -f ./envs/ClusMus.yml

conda activate ClusMus # "source activate " for conda < 4.5.11

###############################################################################
# Workdir files = output files from rule Merge_files of Quality_Filtering.py
## Merged = "out/Sortmerna/Merged_{sample}.fastq"
mkdir out/Sortmerna/Actin out/Sortmerna/COI out/Sortmerna/ITS2_coraux out/Sortmerna/ITS2_Symb out/Sortmerna/D-loop

###############################################################################
## ACTIN

# index db sortmerna
indexdb_rna --ref ./Databases/Actin.fasta,./Silva/Databases/Actin.fasta.index -m 4.e+04 -v

# extraction via sortmerna
for file in out/Sortmerna/Merged_*.fastq; do
    filename=$(basename $file | cut -d "." -f 1)
    sortmerna --reads $file --ref ./Databases/Actin.fasta,./Databases/Actin.fasta.index --blast '1' --sam --aligned ./out/Sortmerna/Actin/Actin_$filename --log -v -a 28
done

# conversion sam in fasta
for file in out/Sortmerna/Actin/*.sam; do
    awk '/^@/{ next; } { print $3 "\t" $10 }' $file >out/Sortmerna/Actin/$file.fasta
done

# conversion blast for merge
for file in out/Sortmerna/Actin/*.blast; do
    awk '/^@/{ next; } { print $2 "\t" $4 "\t" $11 }' $file >out/Sortmerna/Actin/$file.txt
done

# manage output
mkdir out/Sortmerna/Actin/Sortmerna_output
mv out/Sortmerna/Actin/*.sam out/Sortmerna/Actin/Sortmerna_output
mv out/Sortmerna/Actin/*.log out/Sortmerna/Actin/Sortmerna_output
mv out/Sortmerna/Actin/*.blast out/Sortmerna/Actin/Sortmerna_output

# paste files by row
for file in ./out/Sortmerna/Actin/Actin_*.blast.txt; do
    filename=$(basename $file | cut -d '.' -f 1)
    paste $file out/Sortmerna/Actin/$filename.sam.fasta >out/Sortmerna/Actin/$filename.merged
done

# filter on E-value and uniq read
for file in ./out/Sortmerna/Actin/*.merged; do
    filename=$(basename $file | cut -d '.' -f 1)
    awk '{ if ($3 < 0.0001) print $0}' $file | uniq >out/Sortmerna/Actin/$filename.filter
done

# nb reads per samples
wc -l out/Sortmerna/Actin/*.filter >out/Sortmerna/Actin/Actin_reads.txt

# prepare file for assembly
for file in ./out/Sortmerna/Actin/*.filter; do
    filename=$(basename $file | cut -d '.' -f 1)
    awk '{ print ">"$4 "\n" $5}' $file >out/Sortmerna/Actin/$filename.fasta
done

# manage output files
mkdir out/Sortmerna/Actin/TmpFile
mv out/Sortmerna/Actin/*.sam.fasta out/Sortmerna/Actin/TmpFile
mv out/Sortmerna/Actin/*.blast.txt out/Sortmerna/Actin/TmpFile

# assembly by megahit to limit reads output
for file in ./out/Sortmerna/Actin/*.fasta; do
    filename=$(basename $file | cut -d '.' -f 1)
    megahit --r $file -o out/Sortmerna/Actin/Megahit_$filename --k-step 10 -m 0.7 -t2
done # mode single read

# Copy all files in one output dir
mkdir out/Sortmerna/Actin/Fasta
dest_dir=out/Sortmerna/Actin/Fasta
for file in out/Sortmerna/Actin/Megahit_*/final.contigs.fa; do
    [[ -f "$file" ]] || continue
    dir="${file%/*}"
    cp "$file" "$dest_dir/${dir#*Megahit_Actin_merged_}.fasta"
done

# Add filename at each read and combine them
awk '/^>/{sub(">","&"FILENAME"_");sub(/\.fasta/,x)}1' out/Sortmerna/Actin/Fasta/*.fasta >out/Sortmerna/Actin/Fasta/All_Actin.fasta

# blastx output for filtering only putative actin prot
## nr with taxonomy
diamond makedb --in Databases/nr.fasta -d nr                                                                                                                                                                                                                                                         # index db
diamond blastx -d nr -q out/Sortmerna/Actin/Fasta/All_Actin.fasta -o out/Sortmerna/Actin/Fasta/Diamond_blastx_nr_Actin --outfmt 6 qseqid qlen qstart qend evalue score ppos gaps staxids qtitle stitle --taxonmap Databases/blastdb/prot.accession2taxid.gz -k 1 --more-sensitive -p 8 >nr_Actin.log # do not forget to change -p value according to your CPUs available

# filter keep only actin blastx
awk 'FS=OFS="\t" {print $10,$11}' out/Sortmerna/Actin/Fasta/Diamond_blastx_nr_Actin | sed 's/\ /_/g' | egrep 'Actin|actin' | egrep 'Acropora|Pocillopora|Stylophora|Seriatopora' | egrep 'cytoplasmic' >out/Sortmerna/Actin/Fasta/prot_actin

awk '{print $1}' out/Sortmerna/Actin/Fasta/prot_actin >out/Sortmerna/Actin/Fasta/listprotactin

sed -i 's/\ /_/g' out/Sortmerna/Actin/Fasta/All_Actin.fasta

perl -ne 'if(/^>(\S+)/){$c=$i{$1}}$c?print:chomp;$i{$_}=1 if @ARGV' out/Sortmerna/Actin/Fasta/listprotactin out/Sortmerna/Actin/Fasta/All_Actin.fasta >out/Sortmerna/Actin/Fasta/Blastx_Actin.fasta

###############################################################################
## COI

# index db
indexdb_rna --ref ./Databases/MIDORI/COI_Anthozoa1.fasta,./Databases/MIDORI/COI_Anthozoa1.fasta.index -m 4e+04 -v

# extraction
for file in out/Sortmerna/*.fastq; do
    filename=$(basename $file | cut -d "." -f 1)
    sortmerna --reads $file --ref ./Databases/MIDORI/COI_Anthozoa1.fasta,./Databases/MIDORI/COI_Anthozoa1.fasta.index --blast '1 cigar qcov qstrand' --sam --aligned ./out/Sortmerna/COI/COI_$filename --log -v -a 20
done

# conversion sam in fasta
for file in out/Sortmerna/COI/*.sam; do
    awk '/^@/{ next; } { print $3 "\t" $10 }' $file >out/Sortmerna/COI/$file.fasta
done

# conversion blast for merge
for file in out/Sortmerna/COI/*.blast; do
    awk '/^@/{ next; } { print $2 "\t" $4 "\t" $11 }' $file >out/Sortmerna/COI/$file.txt
done

# manage output files
mkdir out/Sortmerna/COI/Sortmerna_output
mv out/Sortmerna/COI/*.sam out/Sortmerna/COI/Sortmerna_output
mv out/Sortmerna/COI/*.log out/Sortmerna/COI/Sortmerna_output
mv out/Sortmerna/COI/*.blast out/Sortmerna/COI/Sortmerna_output

# paste
for file in ./out/Sortmerna/COI/COI*.blast.txt; do
    filename=$(basename $file | cut -d '.' -f 1)
    paste $file out/Sortmerna/COI/$filename.sam.fasta >out/Sortmerna/COI/$filename.merged
done

# filter
for file in ./out/Sortmerna/COI/*.merged; do
    filename=$(basename $file | cut -d '.' -f 1)
    awk '{ if ($3 < 0.0001) print $0}' $file | uniq >out/Sortmerna/COI/$filename.filter
done

# format
for file in ./out/Sortmerna/COI/*.filter; do
    filename=$(basename $file | cut -d '.' -f 1)
    awk '{ print ">"$4 "\n" $5}' $file >out/Sortmerna/COI/$filename.fasta
done

# manage files
mkdir out/Sortmerna/COI/TmpFile
mv out/Sortmerna/COI/*.sam.fasta out/Sortmerna/COI/TmpFile
mv out/Sortmerna/COI/*.blast.txt out/Sortmerna/COI/TmpFile

# assembly megahit
for file in ./out/Sortmerna/COI/*.fasta; do
    filename=$(basename $file | cut -d '.' -f 1)
    megahit --r $file -o out/Sortmerna/COI/Megahit_$filename --k-step 10 -m 0.7 -t2
done # mode single read

# Copy all files in one output dir
mkdir out/Sortmerna/COI/Fasta
dest_dir=out/Sortmerna/COI/Fasta
for file in out/Sortmerna/COI/Megahit_*/final.contigs.fa; do
    [[ -f "$file" ]] || continue
    dir="${file%/*}"
    cp "$file" "$dest_dir/${dir#*Megahit_COI_merged_}.fasta"
done

# Add filename at each read and combine them
awk '/^>/{sub(">","&"FILENAME"_");sub(/\.fasta/,x)}1' out/Sortmerna/COI/Fasta/*.fasta >out/Sortmerna/COI/Fasta/All_COI.fasta

###############################################################################
## ITS2 coraux

# index db
indexdb_rna --ref ./Databases/ITS2_corals/ITS2_AcrPoc.fasta,./Databases/ITS2_corals/ITS2_AcrPoc.fasta.index -m 4.e+04 -v

# extraction via sortmerna
for file in ./out/Sortmerna/*.fastq; do
    filename=$(basename $file | cut -d "." -f 1)
    sortmerna --reads $file --ref ./Databases/ITS2_corals/ITS2_AcrPoc.fasta,./Databases/ITS2_corals/ITS2_AcrPoc.fasta.index --blast '1 cigar qcov qstrand' --sam --aligned ./out/Sortmerna/ITS2_coraux/ITS2_$filename --log -v -a 20
done

# conversion sam in fasta
for file in out/Sortmerna/ITS2_coraux/*.sam; do
    awk '/^@/{ next; } { print $3 "\t" $10 }' $file >out/Sortmerna/ITS2_coraux/$file.fasta
done

# conversion blast for merge
for file in out/Sortmerna/ITS2_coraux/*.blast; do
    awk '/^@/{ next; } { print $2 "\t" $4 "\t" $11 }' $file >out/Sortmerna/ITS2_coraux/$file.txt
done

# manage output files
mkdir out/Sortmerna/ITS2_coraux/Sortmerna_output
mv out/Sortmerna/ITS2_coraux/*.sam out/Sortmerna/ITS2_coraux/Sortmerna_output
mv out/Sortmerna/ITS2_coraux/*.log out/Sortmerna/ITS2_coraux/Sortmerna_output
mv out/Sortmerna/ITS2_coraux/*.blast out/Sortmerna/ITS2_coraux/Sortmerna_output

# merge files
for file in ./out/Sortmerna/ITS2_coraux/ITS2_*.blast.txt; do
    filename=$(basename $file | cut -d '.' -f 1)
    paste $file out/Sortmerna/ITS2_coraux/$filename.sam.fasta >out/Sortmerna/ITS2_coraux/$filename.merged
done

# filter on E-value and uniq reads
for file in ./out/Sortmerna/ITS2_coraux/*.merged; do
    filename=$(basename $file | cut -d '.' -f 1)
    awk '{ if ($3 < 0.0001) print $0}' $file | uniq >out/Sortmerna/ITS2_coraux/$filename.filter
done

# format for assembly
for file in ./out/Sortmerna/ITS2_coraux/*.filter; do
    filename=$(basename $file | cut -d '.' -f 1)
    awk '{ print ">"$4 "\n" $5}' $file >out/Sortmerna/ITS2_coraux/$filename.fasta
done

# manage output files
mkdir out/Sortmerna/ITS2_coraux/TmpFile
mv out/Sortmerna/ITS2_coraux/*.sam.fasta out/Sortmerna/ITS2_coraux/TmpFile
mv out/Sortmerna/ITS2_coraux/*.blast.txt out/Sortmerna/ITS2_coraux/TmpFile

# assembly megahit
for file in ./out/Sortmerna/ITS2_coraux/*.fasta; do
    filename=$(basename $file | cut -d '.' -f 1)
    megahit --r $file -o out/Sortmerna/ITS2_coraux/Megahit_$filename --k-step 10 -m 0.7 -t2
done # mode single read

# Copy all files in one output dir
mkdir out/Sortmerna/ITS2_coraux/Fasta
dest_dir=out/Sortmerna/ITS2_coraux/Fasta
for file in out/Sortmerna/ITS2_coraux/Megahit_*/final.contigs.fa; do
    [[ -f "$file" ]] || continue
    dir="${file%/*}"
    cp "$file" "$dest_dir/${dir#*Megahit_ITS2_merged_}.fasta"
done

# Add filename at each read and combine them
awk '/^>/{sub(">","&"FILENAME"_");sub(/\.fasta/,x)}1' out/Sortmerna/ITS2_coraux/Fasta/*.fasta >out/Sortmerna/ITS2_coraux/Fasta/All_ITS2_coraux.fasta

###############################################################################
## ITS2 Symbiodinium

# index db
indexdb_rna --ref ./Databases/ITS2_Symb/Cunning_ITS2_db_CoralsOnly.fasta,./Databases/ITS2_Symb/Cunning_ITS2.fasta.index -m 4.e+04 -v

# extraction
for file in ./out/Sortmerna/*.fastq; do
    filename=$(basename $file | cut -d "." -f 1)
    sortmerna --reads $file --ref ./Databases/ITS2_Symb/Cunning_ITS2_db_CoralsOnly.fasta,./Databases/ITS2_Symb/Cunning_ITS2.fasta.index --blast '1' --sam --aligned ./out/Sortmerna/ITS2_Symb/Symb_$filename --log -v -a 8
done

# conversion sam fasta
for file in ./out/Sortmerna/ITS2_Symb/*.sam; do
    awk '/^@/{ next; } { print $3 "\t" $10 }' $file >out/Sortmerna/ITS2_Symb/$file.fasta
done

# conversion blast merge
for file in ./out/Sortmerna/ITS2_Symb/*.blast; do
    awk '/^@/{ next; } { print $2 "\t" $4 "\t" $11 }' $file >out/Sortmerna/ITS2_Symb/$file.txt
done

# manage files
mkdir out/Sortmerna/ITS2_Symb/Sortmerna_output
mv out/Sortmerna/ITS2_Symb/*.sam out/Sortmerna/ITS2_Symb/Sortmerna_output
mv out/Sortmerna/ITS2_Symb/*.log out/Sortmerna/ITS2_Symb/Sortmerna_output
mv out/Sortmerna/ITS2_Symb/*.blast out/Sortmerna/ITS2_Symb/Sortmerna_output

# paste
for file in ./out/Sortmerna/ITS2_Symb/Symb_*.blast.txt; do
    filename=$(basename $file | cut -d '.' -f 1)
    paste $file $filename.sam.fasta >out/Sortmerna/ITS2_Symb/$filename.merged
done

# filter
for file in ./out/Sortmerna/ITS2_Symb/*.merged; do
    filename=$(basename $file | cut -d '.' -f 1)
    awk '{ if ($3 < 0.0001) print $0}' $file | uniq >out/Sortmerna/ITS2_Symb/$filename.filter
done

# format
for file in ./out/Sortmerna/ITS2_Symb/*.filter; do
    filename=$(basename $file | cut -d '.' -f 1)
    awk '{ print ">"$4 "\n" $5}' $file >out/Sortmerna/ITS2_Symb/$filename.fasta
done

# manage files
mkdir out/Sortmerna/ITS2_Symb/TmpFile
mv out/Sortmerna/ITS2_Symb/*.sam.fasta out/Sortmerna/ITS2_Symb/TmpFile
mv out/Sortmerna/ITS2_Symb/*.blast.txt out/Sortmerna/ITS2_Symb/TmpFile

# assembly megahit
for file in ./out/Sortmerna/ITS2_Symb/*.fasta; do
    filename=$(basename $file | cut -d '.' -f 1)
    megahit --r $file -o out/Sortmerna/ITS2_Symb/Megahit_$filename --k-step 10 -m 0.7 -t2
done # mode single read

# Copy all files in one output dir
mkdir out/Sortmerna/ITS2_Symb/Fasta
dest_dir=out/Sortmerna/ITS2_Symb/Fasta
for file in out/Sortmerna/ITS2_Symb/Megahit_*/final.contigs.fa; do
    [[ -f "$file" ]] || continue
    dir="${file%/*}"
    cp "$file" "$dest_dir/${dir#*Megahit_Symb_merged_}.fasta"
done

# Add filename at each read and combine them
awk '/^>/{sub(">","&"FILENAME"_");sub(/\.fasta/,x)}1' out/Sortmerna/ITS2_Symb/Fasta/*.fasta >out/Sortmerna/ITS2_Symb/Fasta/All_ITS2_Symb.fasta

###############################################################################
## D-loop

# index db
indexdb_rna --ref ./Databases/Ali_CARIOCA.fasta,./Databases/Ali_CARIOCA.fasta.index -m 4.e+04 -v

# extraction
for file in ./out/Sortmerna/*.fastq; do
    filename=$(basename $file | cut -d "." -f 1)
    sortmerna --reads $file --ref ./Databases/Ali_CARIOCA.fasta,./Databases/Ali_CARIOCA.fasta.index --blast '1' --sam --aligned ./out/Sortmerna/D-loop/D-loop_$filename --log -v -a 8
done

# conversion sam fasta
for file in ./out/Sortmerna/D-loop/*.sam; do
    awk '/^@/{ next; } { print $3 "\t" $10 }' $file >out/Sortmerna/D-loop/$file.fasta
done

# conversion blast merge
for file in ./out/Sortmerna/D-loop/*.blast; do
    awk '/^@/{ next; } { print $2 "\t" $4 "\t" $11 }' $file >out/Sortmerna/D-loop/$file.txt
done

# manage files
mkdir out/Sortmerna/D-loop/Sortmerna_output
mv out/Sortmerna/D-loop/*.sam out/Sortmerna/D-loop/Sortmerna_output
mv out/Sortmerna/D-loop/*.log out/Sortmerna/D-loop/Sortmerna_output
mv out/Sortmerna/D-loop/*.blast out/Sortmerna/D-loop/Sortmerna_output

# paste
for file in ./out/Sortmerna/D-loop/Symb_*.blast.txt; do
    filename=$(basename $file | cut -d '.' -f 1)
    paste $file $filename.sam.fasta >out/Sortmerna/D-loop/$filename.merged
done

# filter
for file in ./out/Sortmerna/D-loop/*.merged; do
    filename=$(basename $file | cut -d '.' -f 1)
    awk '{ if ($3 < 0.0001) print $0}' $file | uniq >out/Sortmerna/D-loop/$filename.filter
done

# format
for file in ./out/Sortmerna/D-loop/*.filter; do
    filename=$(basename $file | cut -d '.' -f 1)
    awk '{ print ">"$4 "\n" $5}' $file >out/Sortmerna/D-loop/$filename.fasta
done

# manage files
mkdir out/Sortmerna/D-loop/TmpFile
mv out/Sortmerna/D-loop/*.sam.fasta out/Sortmerna/D-loop/TmpFile
mv out/Sortmerna/D-loop/*.blast.txt out/Sortmerna/D-loop/TmpFile

# assembly megahit
for file in ./out/Sortmerna/D-loop/*.fasta; do
    filename=$(basename $file | cut -d '.' -f 1)
    megahit --r $file -o out/Sortmerna/D-loop/Megahit_$filename --k-step 10 -m 0.7 -t2
done # mode single read

# Copy all files in one output dir
mkdir out/Sortmerna/D-loop/Fasta
dest_dir=out/Sortmerna/D-loop/Fasta
for file in out/Sortmerna/D-loop/Megahit_*/final.contigs.fa; do
    [[ -f "$file" ]] || continue
    dir="${file%/*}"
    cp "$file" "$dest_dir/${dir#*Megahit_D-loop_merged_}.fasta"
done

# Add filename at each read and combine them
awk '/^>/{sub(">","&"FILENAME"_");sub(/\.fasta/,x)}1' out/Sortmerna/D-loop/Fasta/*.fasta >out/Sortmerna/D-loop/Fasta/All_D-loop.fasta

###############################################################################
# deactivate environment
conda deactivate ClusMus

###############################################################################
# END
