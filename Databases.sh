#!/bin/bash

###########################################
## Databases download for CORALS Project ##
###########################################

# Author: Julie Ripoll
# Contact: julie.ripoll87@gmail.com
# Created: 2018-10
# Last update: 2022-08-09
# License: License CC-BY-NC 4.0

###############
# Note: the first part for Silva required your attention. Not executable as is, follow each step.
# Copy all files of the github and follow the process


###############
# Software Environment
#conda create --name DBD arb-bio=6.0.6 mothur=1.36.1 sortmerna=2.1
#conda env export -n DBD > DBD.yml
conda env update -n DBD -f ./envs/DBD.yml # create the environment from the yaml file provided in ./envs
conda activate DBD # "source activate " for conda < 4.5.11


###############
# Silva database download LSU and SSU
mkdir Databases/Silva Databases/MIDORI Databases/ITS2_corals Databases/ITS2_symb

wget -o Log/logSSU -N -P Databases/Silva 'https://www.arb-silva.de/fileadmin/arb_web_db/release_128/ARB_files/SSURef_NR99_128_SILVA_07_09_16_opt.arb.gz'
gunzip Databases/Silva/SSURef_NR99_128_SILVA_07_09_16_opt.arb.gz

wget -o Log/logLSU -N -P Databases/Silva 'https://www.arb-silva.de/fileadmin/arb_web_db/release_128/ARB_files/LSURef_NR99_128_SILVA_07_09_16_opt.arb.gz'
gunzip Databases/Silva/LSURef_NR99_128_SILVA_07_09_16_opt.arb.gz

# download taxonomies
# https://mothur.org/blog/2017/SILVA-v128-reference-files/
wget -P Databases/Silva/ https://www.arb-silva.de/fileadmin/silva_databases/release_128/Exports/taxonomy/tax_slv_ssu_128.txt
wget -P Databases/Silva/ https://www.arb-silva.de/fileadmin/silva_databases/release_128/Exports/taxonomy/tax_slv_lsu_128.txt

# https://mothur.org/wiki/silva_reference_files/#release-128
wget -P Databases/Silva/ https://mothur.s3.us-east-2.amazonaws.com/wiki/silva.nr_v128.tgz
gunzip Databases/Silva/silva.nr_v128.tgz
tar xvf -C Databases/Silva Databases/Silva/silva.nr_v128.tar

# Check at md5sum file for all DL before continue


###############
# Others databases download and formatting

# ITS2 from Wuerzberg University (all species)
wget -o Log/logITSWb -P Databases/ITS2_corals 'https://github.com/BioInf-Wuerzburg/ITS2database_update_2015/blob/master/data/2015/eukaryota.all.fasta.bz2' # download
bzip2 -dk Databases/ITS2_corals/eukaryota.all.fasta.bz2 # dezip
awk '{ print $1 "\n" $2 "_" $3 "_" $4 "_" $5 "_" $6}' Databases/ITS2_corals/eukaryota.all.fasta | sed 's/____//g ; /^$/d ; s/$/\t/' > tmp.tmp
awk '/^>/ {printf (NR==1 ? "" : RS) $0; next} { printf "%s", $0} END { printf RS}' tmp.tmp | egrep 'Acropora|Pocillopora' | sed ':begin;s/\t/\n/2;t begin' | sed '/^$/d' > Databases/ITS2_corals/ITS2_AcrPoc.fasta
rm tmp.tmp
grep '>' Databases/ITS2_corals/ITS2_AcrPoc.fasta > Databases/ITS2_corals/ITS2_AcrPoc.tax # tax file

# ITS2 Symbiodinium from Cunning 2016
wget -o Log/logITSCunning -P Databases/ITS2_Symb 'https://github.com/jrcunning/STJ2012/blob/master/data/ITS2db_trimmed.fasta' # db filtered
grep '>' Databases/ITS2_Symb/Cunning_ITS2_db_CoralsOnly.fasta > Databases/ITS2_Symb/Cunning.tax # tax

# COI from MIDORI website
wget -o Log/logCOI -P Databases/MIDORI 'http://www.reference-midori.info/download.php' # download unique RDP COI
sed 's/$/\t/' Databases/MIDORI/MIDORI_UNIQUE_1.1_COI_RDP.fasta/MIDORI_UNIQUE_1.1_COI_RDP.fasta | awk '/^>/ {printf (NR==1 ? "" : RS) $0; next} { printf "%s", $0} END { printf RS}' | grep 'Anthozoa' | sed -e ':begin;s/\t//3;t begin' | sed -e ':begin;s/\t/\n/2;t begin' | sed -e '/^$/d' >  Databases/MIDORI/COI_Anthozoa1.fasta
grep '>'  Databases/MIDORI/COI_Anthozoa1.fasta >  Databases/MIDORI/COI_filtered.tax # filter Anthozoa tax

# Our specific databases available on gitlab, download it for identification.sh
# Actin 19 sequences from NCBI, using key-words: actin cytoplasmique AND cnidaria
# CARIOCA D-loop


###############
# index all databases
cd ../..

# index databases separately and all together according to the future uses
indexdb_rna --ref ./Databases/Silva/LSU_archaea_notalign.fasta,./Databases/Silva/LSU_archaea_notalign.fasta.index -m 4e+04 -v

indexdb_rna --ref ./Databases/Silva/LSU_bacteria_notalign.fasta,./Databases/Silva/LSU_bacteria_notalign.fasta.index -m 4e+04 -v

indexdb_rna --ref ./Databases/Silva/LSU_eukaryota_notalign.fasta,./Databases/Silva/LSU_eukaryota_notalign.fasta.index -m 4e+04 -v

indexdb_rna --ref ./Databases/Silva/SSU_archaea_notalign.fasta,./Databases/Silva/SSU_archaea_notalign.fasta.index -m 4e+04 -v

indexdb_rna --ref ./Databases/Silva/SSU_bacteria_notalign.fasta,./Databases/Silva/SSU_bacteria_notalign.fasta.index -m 4e+04 -v

indexdb_rna --ref ./Databases/Silva/SSU_eukaryota_notalign.fasta,./Databases/Silva/SSU_eukaryota_notalign.fasta.index -m 4e+04 -v

indexdb_rna --ref ./Databases/Silva/LSU_archaea_notalign.fasta,./Databases/Silva/LSU_archaea_notalign.fasta.index:./Databases/Silva/LSU_bacteria_notalign.fasta,./Databases/Silva/LSU_bacteria_notalign.fasta.index:./Databases/Silva/LSU_eukaryota_notalign.fasta,./Databases/Silva/LSU_eukaryota_notalign.fasta.index:./Databases/Silva/SSU_archaea_notalign.fasta,./Databases/Silva/SSU_archaea_notalign.fasta.index:./Databases/Silva/SSU_bacteria_notalign.fasta,./Databases/Silva/SSU_bacteria_notalign.fasta.index:./Databases/Silva/SSU_eukaryota_notalign.fasta,./Databases/Silva/SSU_eukaryota_notalign.fasta.index -m 4e+04 -v

indexdb_rna --ref ./Databases/ITS2_corals/ITS2_AcrPoc.fasta,./Databases/ITS2_corals/ITS2_AcrPoc.fasta.index -m 4.e+04 -v

indexdb_rna --ref ./Databases/ITS2_Symb/Cunning_ITS2_db_CoralsOnly.fasta,./Databases/ITS2_Symb/Cunning_ITS2_db_CoralsOnly.fasta.index -m 4.e+04 -v

indexdb_rna --ref ./Databases/MIDORI/COI_Anthozoa1.fasta,./Databases/MIDORI/COI_Anthozoa1.fasta.index -m 4e+04 -v

indexdb_rna --ref ./Databases/Actin.fasta,./Databases/Actin.fasta.index -m 4e+04 -v


###############
conda deactivate DBD # or "source deactivate " for conda < 4.5.11
